#include <iostream>
#include "coloring.hpp"

int main(int argc, char** argv) {
  Arak::Coloring c;
  std::cin >> c;
  if (!std::cin.good()) {
    std::cerr << "Error reading coloring from standard input." << std::endl;
    return 1;
  }
  c.writeXfig(std::cout);
  return 0;
}
