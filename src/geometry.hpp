// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _GEOMETRY_HPP
#define _GEOMETRY_HPP

#include <algorithm>
#include "global.hpp"

/* A crucial decision when implementing algorithms that perform
 * geometric reasoning is what type of numeric representation to use.
 * Standard floating point has errors that often lead to
 * inconsistencies and bugs.  The CGAL library offers many choices,
 * but the crucial decisions seem to be whether the following are
 * supported:
 *
 *   - exact constructions (e.g., construct a point on this line)
 *   - exact predicates (e.g., is this point on that line?)
 * 
 * The code base has been written so that most portions rely on
 * neither of these.  Thus, we can use floating point representations
 * without (evident) problems.  The notable exceptions are
 *
 *   - sanity checking code makes extensive use of exact predicates, 
 *     and some isolated sanity checking code uses requires 
 *     constructions
 *   - using a triangulation to check for edge crossings
 *     (coloring.cpp) requires exact predicates
 *
 * Exact constructions may be used for debugging purposes, but they
 * lead to very slow programs.  By default, we use neither.  If an
 * inexplicable bug arises, the first action should be to turn on
 * exact geometric predicates.
 */

// #define EXACT_GEOM_PRED // Use exact predicates (~20% slower)
// #define EXACT_GEOM_CXN  // Use exact constructions (very slow)

/*
 * This file serves as an interface to the CGAL geometry library.
 */

#include <CGAL/basic.h>
#include <CGAL/Cartesian.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Gmpq.h>
#include <CGAL/Homogeneous.h>
#include <CGAL/Simple_homogeneous.h>
#include <CGAL/Filtered_kernel.h>
#include <CGAL/Iso_rectangle_2_Segment_2_intersection.h>
#include <CGAL/Segment_2_Segment_2_intersection.h>
#include <CGAL/Triangle_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Bbox_2.h>

namespace Arak {
  namespace Geometry {

    // This is a rational number represented by two arbitrary size
    // integers.  Computations are exact, but extremely slow.
    typedef CGAL::Cartesian<CGAL::Gmpq>          ExactCxnKernel;

    // The kernel below is inexact in its predicates and
    // constructions.
    typedef CGAL::Simple_cartesian<double>       InexactKernel;

    // This is a wrapper around a double representation that guarantees
    // exact predicates, but not exact constructions.  Exact predicates
    // are used in many parts of the code.
    typedef CGAL::Filtered_kernel<InexactKernel> ExactPredKernel;

#ifdef EXACT_GEOM_CXN
#define EXACT_GEOM_PRED
    typedef ExactCxnKernel                       Kernel;
#elif defined(EXACT_GEOM_PRED)
    typedef ExactPredKernel                      Kernel;
#else
    typedef InexactKernel                        Kernel;
#endif
    typedef Kernel::Point_2                      Point;
    typedef Kernel::Line_2                       Line;
    typedef Kernel::Segment_2                    Segment;
    typedef Kernel::Direction_2                  Direction;
    typedef Kernel::Vector_2                     Vector;
    typedef Kernel::Ray_2                        Ray;
    typedef Kernel::Iso_rectangle_2              Rectangle;
    typedef Kernel::Triangle_2                   Triangle;
    typedef CGAL::Polygon_2<Kernel>                    Polygon;

    /**
     * Determines if two line segments cross, i.e., if they intersect at a
     * point that is not a shared vertex.
     */
    inline bool crosses(const Segment& s, const Point& a, const Point& b) {
      if ((s.source() == a) xor (s.target() == b)) 
        return false;
      else if ((s.source() == b) xor (s.target() == a)) 
        return false;
      else {
        // First do a quick bounding box check?
        Segment r(a, b);
        return CGAL::do_intersect(s, r);
      }
    }

    /**
     * This is a helper function that computes the smallest rectangle that
     * encloses the supplied points.
     */
    inline Rectangle boundingRectangle(const Point& u, 
                                       const Point& v, 
                                       const Point& w) {
      const Kernel::FT& xmin = 
        std::min(std::min(u.x(), v.x()), w.x());
      const Kernel::FT& xmax = 
        std::max(std::max(u.x(), v.x()), w.x());
      const Kernel::FT& ymin = 
        std::min(std::min(u.y(), v.y()), w.y());
      const Kernel::FT& ymax = 
        std::max(std::max(u.y(), v.y()), w.y());
      return Rectangle(xmin, ymin, xmax, ymax);
    }
    
    /**
     * This is a helper function that computes the smallest rectangle that
     * encloses the supplied points.
     */
    inline Rectangle boundingRectangle(const Point& t,
                                       const Point& u, 
                                       const Point& v, 
                                       const Point& w) {
      const Kernel::FT& xmin = 
        std::min(std::min(std::min(t.x(), u.x()), v.x()), w.x());
      const Kernel::FT& xmax = 
        std::max(std::max(std::max(t.x(), u.x()), v.x()), w.x());
      const Kernel::FT& ymin = 
        std::min(std::min(std::min(t.y(), u.y()), v.y()), w.y());
      const Kernel::FT& ymax = 
        std::max(std::max(std::max(t.y(), u.y()), v.y()), w.y());
      return Rectangle(xmin, ymin, xmax, ymax);
    }

    /**
     * Computes the logarithm of the sine of an angle specified by three 
     * points.
     * 
     * The Side-Angle-Side (SAS) formula for a triangle's area gives
     * \f[ 
     * A = \frac{1}{2} x y \sin Z 
     * \f]
     * where \f$A\f$ is the area of the triangle, \f$x\f$ and \f$y\f$
     * are the lengths of two sides, and \f$Z\f$ is the angle between 
     * these two sides.  From this we get
     * \f[ 
     * \log \sin Z = \log (2A) - \log (xy)
     * \f]
     *
     * @param a the first point
     * @param b the second point (distinct from a)
     * @param c the third point (distinct from a and b)
     * @return the logarithm of the sine of the angle at vertex b of 
     *         the triangle abc
     */ 
    inline double logSineB(const Point& a, 
                           const Point& b, 
                           const Point& c) {
      Kernel::FT area = CGAL::abs(CGAL::area(a, b, c));
      Kernel::FT ab_len_sq = CGAL::squared_distance(a, b);
      Kernel::FT bc_len_sq = CGAL::squared_distance(b, c);
      return (ln(2.0 * CGAL::to_double(area))
              - ln(CGAL::to_double(ab_len_sq * bc_len_sq)) / 2.0);
    }

    /**
     * Projects a point orthogonally onto the closure of a rectangle.
     * This is useful to postprocess inexact constructions so that the
     * resulting point is in the closure of the rectangle.
     *
     * @param p a point
     * @param r a rectangle
     * @return  the point closest to p in the closure of r
     */
    inline Geometry::Point project(const Geometry::Point& p,
                                   const Geometry::Rectangle& r) {
      Geometry::Kernel::FT x = p.x();
      Geometry::Kernel::FT y = p.y();
      x = std::max<Geometry::Kernel::FT>(x, r.xmin());
      x = std::min<Geometry::Kernel::FT>(x, r.xmax());
      y = std::max<Geometry::Kernel::FT>(y, r.ymin());
      y = std::min<Geometry::Kernel::FT>(y, r.ymax());
      return Geometry::Point(x, y);
    }
    
    /**
     * Creates a regular \f$n\f$-gon with the supplied number of
     * vertices and radius.
     */
    void n_gon(int n, double radius, Polygon& poly);

  }
}

#endif
