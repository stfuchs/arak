// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _SONAR_OBS_HPP
#define _SONAR_OBS_HPP

#include <iostream>
#include "properties.hpp"
#include "coloring.hpp"
#include "query_point.hpp"
#include "arak.hpp"
#include "grid.hpp"

namespace Arak {

  /**
   * Impact objects represent those aspects of a sonar impact that
   * are relevant for likelihood computations.
   */
  struct SonarImpact {
    
    /**
     * The angle between the face and the segment joining the sensor
     * location with its projection on the face.  If the projection
     * lies on the face then this angle is \f$\pi / 2\f$, or 90
     * degrees.  Otherwise, it is the angle to the closest endpoint
     * of the visible portion of the face.
     */
    double proj_angle;
    
    /**
     * The distance from the sensor to the closest point on the
     * visible portion of the face.
     */
    double min_range;
    
    /**
     * The angle subtended by the visible portion of the face,
     * expressed in positive radians (< 2 * pi).  This is zero if this
     * impact is a corner impact.
     */
    double visible_angle;
    
    /**
     * The direction to the first point visible on the face.
     */
    Geometry::Direction dmin;
    
    /**
     * The direction to the last point visible on the face.
     */
    Geometry::Direction dmax;

    /**
     * The point visible on the face with minimum angle.  This
     * member is not used for likelihood computations---only
     * debugging.
     */
    Geometry::Point pmin;
    
    /**
     * The point visible on the face with maximum angle.  This
     * member is not used for likelihood computations---only
     * debugging.
     */
    Geometry::Point pmax;
    
    /**
     * The point on the visible portion of the face that is closest to
     * the sonar sensor.
     */
    Geometry::Point projection;

    /**
     * Default constructor.
     */
    SonarImpact() : proj_angle(0.0), 
		    min_range(0.0), 
		    visible_angle(0.0),
		    pmin(CGAL::ORIGIN), 
		    pmax(CGAL::ORIGIN),
		    projection(CGAL::ORIGIN) { }
    
    /**
     * Constructor for a face impact.
     *
     * @param source the location of the sensor
     * @param dmin   the direction to the start of the visible portion 
     *               of the face
     * @param dmax   the direction to the end of the visible portion of 
     *               the face
     * @param pmin   the start of the visible portion of the face
     * @param pmax   the end of the visible portion of the face
     */
    SonarImpact(const Geometry::Point& source, 
		const Geometry::Direction& dmin,
		const Geometry::Direction& dmax,
		const Geometry::Point& pmin,
		const Geometry::Point& pmax) : 
      dmin(dmin), dmax(dmax), pmin(pmin), pmax(pmax) { 
      // Compute the point on the impact face closest to the source.
      Geometry::Line face_line(pmin, pmax);
      projection = face_line.projection(source);
      // Project the projection into the segment.
      if (CGAL::collinear_are_ordered_along_line(projection, pmin, pmax)) 
	projection = pmin;
      else if (CGAL::collinear_are_ordered_along_line(pmin, pmax, projection))
	projection = pmax;
      // Compute the impact information.
      Geometry::Direction fd = face_line.direction();
      Geometry::Direction psd = Geometry::Line(projection, source).direction();
      this->proj_angle = (atan2(CGAL::to_double(psd.dy()), 
				CGAL::to_double(psd.dx())) - 
			  atan2(CGAL::to_double(fd.dy()), 
				CGAL::to_double(fd.dx())));
      this->min_range = 
	sqrt(CGAL::to_double(CGAL::squared_distance(source, projection)));
      if (dmin != dmax) {
	// Compute the visible angle.  First convert the directions to
	// angles.
	double amin = atan2(CGAL::to_double(dmin.dy()), 
			    CGAL::to_double(dmin.dx()));
	double amax = atan2(CGAL::to_double(dmax.dy()), 
			    CGAL::to_double(dmax.dx()));
	// Now compute the smaller of the two angles between them.
	double adiff1 = amax - amin;
	if (adiff1 < 0.0) adiff1 += 2 * M_PI;
	if (adiff1 >= 2 * M_PI) adiff1 -= 2 * M_PI;
	double adiff2 = amin - amax;
	if (adiff2 < 0.0) adiff2 += 2 * M_PI;
	if (adiff2 >= 2 * M_PI) adiff2 -= 2 * M_PI;
	this->visible_angle = std::min(fabs(adiff1), fabs(adiff2));
      } else
	this->visible_angle = 0.0;
    }
      
    /**
     * Constructor for a corner impact.
     *
     * @param source the location of the sensor
     * @param pmin   the start of the visible portion of the first face
     * @param dir    the direction to the corner
     * @param corner the corner that joins the two faces
     * @param pmax   the end of the visible portion of the second face
     */
    SonarImpact(const Geometry::Point& source, 
		const Geometry::Point& pmin,
		const Geometry::Direction& dir,
		const Geometry::Point& corner,
		const Geometry::Point& pmax) : 
      dmin(dir), dmax(dir), pmin(corner), pmax(corner), projection(corner) { 
      this->proj_angle = 0.0; // ?
      this->min_range = 
	sqrt(CGAL::to_double(CGAL::squared_distance(source, corner)));
      this->visible_angle = 0.0;
    }

    /**
     * Returns true if this impact is a corner impact, and false if it
     * is a face impact.
     */
    bool isCorner() const { return (visible_angle == 0.0); }

    /**
     * Places a total ordering on sonar impacts.  Impacts are first
     * ordered by depth, and if two impacts have the same depth,
     * corners precede faces.
     */
    bool operator<(const SonarImpact& other) const {
      if (min_range < other.min_range)
	return true;
      else if (min_range == other.min_range) {
	return (isCorner() && !other.isCorner());
      } else
	return false;
    } 
  }; // End of class: SonarImpact

  /**
   * Uses conic ray tracing to compute the sonar impacts associated
   * with a given coloring and sonar cone.
   *
   * @param coloring the coloring representing the map
   * @param vertex   the location of the sonar sensor
   * @param min      the minimum direction of the sonar cone
   * @param max      the maximum direction of the sonar cone
   * @param maxRange the maximum range, i.e. radius of the sonar cone
   * @param impacts  a vector that is emptied and populated with
   *                 sonar impacts sorted by minimum depth
   * @param maxImpactRange if this pointer is not NULL, then it is
   *                       set to the maximum range visible across the
   *                       entire sonar cone (which may be positive infinity)
   */
  void computeSonarImpacts(const Coloring& coloring,
			   const Geometry::Point& vertex,
			   const Geometry::Direction& min,
			   const Geometry::Direction& max,
			   const Geometry::Kernel::FT maxRange,
			   std::vector<SonarImpact>& impacts,
			   double* maxImpactRange = NULL);

  /**
   * Computes the closest sonar impact associated with a given
   * coloring and sonar cone.
   *
   * @param coloring the coloring representing the map
   * @param vertex   the location of the sonar sensor
   * @param min      the minimum direction of the sonar cone
   * @param max      the maximum direction of the sonar cone
   * @param maxRange the maximum range, i.e. radius of the sonar cone
   * @param impact   updated with the sonar impact information (if any)
   * @return true iff there was an impact in the sonar cone
   */
  bool computeFirstSonarImpact(const Coloring& coloring,
			       const Geometry::Point& vertex,
			       const Geometry::Direction& min,
			       const Geometry::Direction& max,
			       const Geometry::Kernel::FT maxRange,
			       SonarImpact& impact);

  /** 
   * An posterior Arak process conditioned on localized sonar
   * measurements with Gaussian noise.
   */
  class ArakPosteriorSonarObs : public ArakProcess, 
				public Coloring::Listener {

  protected:

    class SonarObs;
    typedef PointerHandle<SonarObs> SonarObsHandle;

    /**
     * A grid where each cell stores handles to all observations
     * whose cones intersect the cell's boundary.
     */
    typedef Grid<SonarObsHandle> SonarObsIndex;

    /**
     * A sonar observation.
     */
    class SonarObs : public QueryPointListener {

    public:
      
      /**
       * The location from which the sonar measurement was taken.
       */
      Geometry::Point source;

      /**
       * The minimum direction of the sonar cone.
       */
      Geometry::Direction min;

      /**
       * The maximum direction of the sonar cone.
       */
      Geometry::Direction max;

      /**
       * The sonar range measurement.
       */
      double range;

      /**
       * The current color of the source.
       */
      Color sourceColor;

      /**
       * The current log probability of this observation.
       */
      double lp;

      /**
       * A triangular outer approximation to the region to which this
       * observation's likelihood is sensitive.
       */
      Geometry::Triangle relevantRegion;

      /**
       * An entry of an observation in the index.
       */
      typedef SonarObsIndex::Cell::Entry CellEntry;

      /**
       * A list of cell entries.
       */
      typedef std::list<CellEntry> CellEntryList;

      /**
       * A list of cell entries in which this observation is stored.
       */
      CellEntryList entries;

      /**
       * A list of unused cell entries, used to minimize memory
       * allocation overhead.
       */
      CellEntryList unused;

      /**
       * The sequence number of the last update to this observation.
       */
      unsigned long int updateId;

      /**
       * Default constructor.
       */
      SonarObs(const Geometry::Point& source, 
	       const double angle,
	       const double range,
	       const double aperture,
	       const double maxRange);

      /**
       * Destructor.
       */
      virtual ~SonarObs() {};

      /**
       * Reacts to information that the query point has been recolored.
       *
       * @param color the query point's new color
       */
      virtual void recolor(Color color) {
	sourceColor = color;
      }

    }; // End of class: ArakPosteriorSonarObs::SonarObs

    /**
     * The prior process.
     */
    const ArakProcess& prior;

    /**
     * An array of the observations.
     */
    std::vector<SonarObs*> obs;

    /**
     * An index of the observations.
     */
    SonarObsIndex* obsIndex;

    /**
     * The update sequence number.
     */
    unsigned long int updateId;

    /**
     * The width of the sonar cones (in radians).
     */
    double aperture;

    /**
     * This is the sonar measurement obtained when the sensor does not
     * impact a surface.
     */
    double maxRange;

    /**
     * The probability of obtaining a max-range measurement given the
     * reading is not generated by a sonar contact.
     */
    double prob_max_range;

    /**
     * The weight of a uniform model for outlier measurements.
     */
    double unif_outlier_weight;

    /**
     * The parameter of an exponential model for outlier measurements.
     */
    double exp_outlier_coef;

    /**
     * The constant coefficient (or bias) of the logistic return
     * probability model for face features.
     */
    double face_const_coef;

    /**
     * The coefficient of the projection angle term in the logistic
     * return probability model for face features.
     */
    double face_proj_angle_coef;

    /**
     * The coefficient of the minimum range term in the logistic
     * return probability model for face features.
     */
    double face_min_range_coef;

    /**
     * The coefficient of the visible angle term in the
     * logistic return probability model for face features.
     */
    double face_vis_angle_coef;

    /**
     * The constant coefficient (or bias) of the logistic return
     * probability model for corner features.
     */
    double corner_const_coef;

    /**
     * The coefficient of the range term in the logistic return
     * probability model for corner features.
     */
    double corner_range_coef;

    /**
     * The standard deviation in sonar face range measurements,
     * expressed as relative error.  For example, a value of 0.01
     * would signify that the standard deviation of range measurements
     * to a surface at range \f$d\f$ is \f$0.01 \times d\f$.
     */
    double faceRelErrStDev;

    /**
     * The standard deviation in sonar corner range measurements,
     * expressed as relative error.  For example, a value of 0.01
     * would signify that the standard deviation of range measurements
     * to a corner at range \f$d\f$ is \f$0.01 \times d\f$.
     */
    double cornerRelErrStDev;

    /**
     * The current potential arising from observations.
     */
    double lp;
    
    /**
     * Updates the log probability of the supplied observation under
     * the current coloring and process parameters.  The change in its
     * log probability is returned.
     */
    double updateObsLP(SonarObs& o, bool init = false) const;

  public:

    /**
     * Stream constructor.  The observations are read from a file of
     * numbers with the following format:
     *
     * \verbatim
     * n
     * source-1-x source-1-y angle-1 range-1 
     * source-2-x source-2-y angle-2 range-2 
     * ...
     * source-n-x source-n-y angle-n range-n 
     * \endverbatim
     *
     * @param coloring The coloring whose probability is evaluated.  
     *                 This is fixed for each process object so that 
     *                 likelihood changes due to coloring updates can 
     *                 be processed efficiently.
     * @param props    the properties that define the posterior
     */
    ArakPosteriorSonarObs(const ArakProcess& prior, 
			  const Arak::Util::PropertyMap& props);

    /**
     * Destructor.
     */
    virtual ~ArakPosteriorSonarObs();

    /**
     * Returns a const reference to a rectangle that encloses the
     * observed data.
     */
    const Geometry::Rectangle& boundary() const { 
      return obsIndex->boundary(); 
    }

    /**
     * Returns the scale of this Arak process.
     */
    virtual double scale() const { return prior.scale(); }

    /**
     * Computes the (log) measure associated with the Arak process for
     * the coloring.  (This is actually multiplied by an elementary
     * Poisson measure over the vertex locations to yield the coloring
     * measure.)
     *
     * @return  the measure of the coloring 
     */
    virtual double logMeasure() const;

    /**
     * Computes the potential associated with this posterior Arak
     * process.
     * 
     * @return  the current potential
     * @see ArakProcess
     */
    virtual double potential() const;

    /**
     * Renders a graphical representation of this Arak posterior using
     * the supplied widget.  The Gaussian observations are rendered as
     * disks of varying gray level.
     *
     * @param widget the widget on which to visualize the process
     */
    virtual void visualize(CGAL::Qt_widget& widget) const;

    /**
     * This method recalculates the likelihood of all observations
     * that are out of date and are sensitive to a given triangle.
     *
     * @param a the first vertex of the triangle
     * @param b the second vertex of the triangle
     * @param c the third vertex of the triangle
     */
    virtual void update(const Geometry::Point& a,
			const Geometry::Point& b,
			const Geometry::Point& c);
      
    /**
     * This method is invoked to inform this process that the
     * triangle with the supplied vertices has been recolored.
     *
     * @param a the first vertex of the triangle
     * @param b the second vertex of the triangle
     * @param c the third vertex of the triangle
     */
    virtual void recolored(const Geometry::Point& a,
			   const Geometry::Point& b,
			   const Geometry::Point& c);
      
    /**
     * This method is invoked to inform this process that the
     * quadrilateral with the supplied vertices has been recolored.
     * The vertices are supplied in either clockwise or
     * counter-clockwise order.  Note that this quadrilateral is
     * simple, but not necessarily convex.
     *
     * @param a the first vertex of the quadrilateral
     * @param b the second vertex of the quadrilateral
     * @param c the third vertex of the quadrilateral
     * @param d the fourth vertex of the quadrilateral
     */
    virtual void recolored(const Geometry::Point& a,
			   const Geometry::Point& b,
			   const Geometry::Point& c,
			   const Geometry::Point& d);
    
    /**
     * Computes the probability of obtaining a sonar return given the
     * location and orientation of the sensor and the position of the
     * impact face.  The returned probability is given by the
     * following logistic model:
     * \f[
     *   p = \frac{1}{1 + \exp \left\{ a + b \log \sin \theta + 
     *                                 cr + d\phi \right\}}
     * \f]
     * where \f$\theta\f$ is the impact's projection angle, \f$r\f$ is
     * the impact's minimum range, \f$\phi\f$ is the impact's visible
     * angle, \f$a\f$, \f$b\f$, \f$c\f$ and \f$d\f$ are model
     * parameters.
     *
     * @param impact the impact
     * @return      the probability a sonar return is generated from 
     *              the supplied impact (given the model parameters)
     */
    inline double returnProb(const SonarImpact& impact) const {
      if (impact.isCorner()) 
	return 1.0 / (1.0 + 
		      exp(-(corner_const_coef + 
			    (corner_range_coef * impact.min_range))));
      else
	return 1.0 / (1.0 + 
		      exp(-(face_const_coef + 
			    (face_proj_angle_coef * log(sin(impact.proj_angle))) +
			    (face_min_range_coef * impact.min_range) +
			    (face_vis_angle_coef * impact.visible_angle))));
    }

    /**
     * Returns the likelihood of obtaining an observed sonar range
     * given a true sonar impact.  This model is a Gaussian model with 
     * multiplicative noise.
     *
     * @param impact the impact
     * @param obs    the observed sonar range
     * @return       the likelihood of the observation given the impact
     */
    inline double distLikelihood(const SonarImpact& impact,
				 const double obs) const {
      double stdev = impact.min_range * 
	(impact.isCorner() ? cornerRelErrStDev : faceRelErrStDev);
      double var = stdev * stdev;
      double dist = (obs - impact.min_range);
      // TODO: this should be a truncated Gaussian
      return exp(- (dist * dist) / (2.0 * var)) / (stdev * sqrt(2.0 * M_PI));
    } 

    /**
     * Computes the likelihood of a sonar range reading given a set of
     * true sonar impacts.
     */
    template <typename ImpactIterator>
    inline double likelihood(double range,
			     ImpactIterator begin,
			     ImpactIterator end) const {
      bool is_max_range = (range >= maxRange);
      double no_prev_returns_prob = 1.0;
      double likelihood = 0.0;
      // Step through the sonar contacts in sorted order, adding in
      // the likelihood of the observation given the contact.
      while (begin != end) {
	const SonarImpact& impact = *begin;
	double return_prob = returnProb(impact);
	if (!is_max_range)
	  likelihood += return_prob * no_prev_returns_prob *
	    distLikelihood(impact, range);
	no_prev_returns_prob *= (1.0 - return_prob);
	++begin;
      }
      // Add in the likelihood that comes from the background model,
      // which is an outlier density with a spike at the max-range
      // value.
      if (is_max_range)
	likelihood += no_prev_returns_prob * prob_max_range;
      else
	likelihood += no_prev_returns_prob * (1.0 - prob_max_range)
	  * (// uniform outlier model
	     (unif_outlier_weight / maxRange)
      	     +
	     // exponential outlier model
	     (1.0 - unif_outlier_weight) * 
	     exp_outlier_coef * exp(-exp_outlier_coef * (maxRange - range)));
      // Return the likelihood.
      return likelihood;
    }

  }; // End of class: ArakPosteriorSonarObs 

  /**
   * Renders a graphical representation of an Arak posterior using the
   * supplied widget.  The sonar measurments are rendered as line segments.
   *
   * @param widget the widget on which to visualize the process
   * @param p      the process to visualize
   * @return       the widget after the rendering is finished
   */
  CGAL::Qt_widget& operator<<(CGAL::Qt_widget& widget, 
			      ArakPosteriorSonarObs& p);

}

#endif
