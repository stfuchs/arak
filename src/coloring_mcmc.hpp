// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _COLORING_MCMC_HPP
#define _COLORING_MCMC_HPP

#include "properties.hpp"
#include "random.hpp"
#include "coloring.hpp"
#include "arak.hpp"
#include "mcmc.hpp"

namespace Arak {

  /**
   * A change to a coloring that is reversible.
   */
  class ColoringMove {

  public:

    /**
     * Updates the supplied coloring according to this move.  It is an
     * error to call this method if the move is not valid for the
     * supplied coloring.
     *
     * @param c a coloring to be updated
     */
    virtual void execute(Coloring& c) = 0;
  
    /**
     * Updates the supplied Coloring so that the effects of performing
     * this move are undone.  The semantics of this method ensure that
     * applying #execute(Coloring&) and then #undo(Coloring&) to the
     * same coloring leaves the coloring invariant.  It is an error to
     * call this method if this undo is not valid for the supplied
     * coloring.
     * 
     * @param c a coloring to be updated
     */
    virtual void undo(Coloring& c) = 0;
  
    /**
     * Destructor.
     */
    virtual ~ColoringMove() {}
  
  };

  /**
   * A null move.
   */
  class NullColoringMove : public ColoringMove {

  public:

    NullColoringMove() { }
    void execute(Coloring& c) { }
    void undo(Coloring& c) { }
    ~NullColoringMove() {}

    /**
     * A static instance of a null coloring move.
     */
    static const NullColoringMove instance;

  };

  /**
   * The proposal distribution of the Markov chain.  This distribution
   * determines the convergence speed of the Markov chain, but does not
   * affect the stationary distribution (provided it is Harris
   * recurrent).
   *
   * This implementation is analogous to an iterator interface.  The
   * #sample method "advances the iterator" by generating a new move
   * which can be accessed by the #move() method.
   */
  class ColoringProposal {

  public:

    /**
     * Destructor.
     */
    virtual ~ColoringProposal() {}

    /**
     * Samples the proposal distribution to obtain a new move.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     * @param reversible a flag which indicates whether the sampled 
     *                   move be reversible (default: true)
     */
    virtual void sample(const Coloring& state,
			Arak::Util::Random& random,
			bool reversible = true) = 0;

    /**
     * Returns a reference to the move most recently sampled from this
     * proposal distribution.  This move is guaranteed to be stable only
     * until #sample(const Coloring&, Arak::Util::Random&) is called again.
     *
     * @return the most recently sampled move
     */
    virtual ColoringMove& move() = 0;

    /**
     * Returns the log likelihood of the most recently sampled proposal.
     *
     * @return the log likelihood of the most recently sampled proposal
     */
    virtual double ll(const Coloring& c) = 0;
  
    /**
     * Returns the log likelihood of sampling the reverse of the most
     * recently sampled proposal.
     *
     * @return the log likelihood of sampling the reverse of the most
     *         recently sampled proposal
     */
    virtual double rll(const Coloring& c) = 0;

    /**
     * This method is invoked by the Markov chain to inform the
     * proposal of the result of its most recently proposed move.
     */
    virtual void result(bool accepted) = 0;
  };

  /**
   *
   */
  struct ArakMarkovChainTraits {
    typedef ArakProcess DistributionType;
    typedef Coloring StateType;
    typedef ColoringMove MoveType;
    typedef ColoringProposal ProposalType;
  };

  /** 
   * A Metropolis-Hastings Markov chain for obtaining samples from an
   * Arak process.
   */
  typedef MarkovChain<ArakMarkovChainTraits> ArakMarkovChain;

  /** 
   * An annealed Metropolis-Hastings Markov chain for obtaining
   * samples from an Arak process.
   */
  typedef AnnealedMarkovChain<ArakMarkovChainTraits> AnnealedArakMarkovChain;

  /** 
   * A stochastic hill climbing Markov chain for optimizing an Arak
   * process.
   */
  typedef StochasticHillClimber<ArakMarkovChainTraits> HillClimbingArakMarkovChain;
}

#endif
