// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _ARAK_HPP
#define _ARAK_HPP

#include "properties.hpp"
#include "coloring.hpp"

namespace CGAL {
  class Qt_widget;
}

namespace Arak {

  /** 
   * An Arak process.  This is a probability distribution over binary
   * colorings of the plane whose discontinuities are line segments.  
   *
   * It consists of two parts.  The first part is a fundamental measure
   *
   * \f[
   *   \lambda(d\gamma) = p^{n_i + n_b/2} \prod_{e \in E} |e|^{-1}
   *                      \prod_{v \in V(\gamma)} \sin(\phi_v) \nu(d\gamma)
   * \f]
   *
   * where \f$\gamma\f$ is a graph, \f$n_i\f$ is the number of vertices
   * in the interior, \f$n_b\f$ is the number of vertices on the
   * boundary, \f$|e|\f$ is the length of edge \f$e\f$, \f$\phi_v\f$ is
   * the smaller angle between the edges incident to vertex \f$v\f$ (or
   * if \f$v\f$ is on the boundary, the smaller angle between the only
   * edge incident to \f$v\f$ and the tangent to the boundary), and
   * \f$\nu(d\gamma)\f$ is the Poisson measure of unit intensity
   * associated with the vertices of the graph \f$\gamma\f$.
   * 
   * The second part is a potential \f$F(\chi)\f$ over the coloring
   * such that the probability measure is given by
   *
   * \f[
   *   Q(\chi) \propto \exp \{ F(\chi) \} \lambda(d\gamma(\chi))
   * \f]
   *
   * The Arak prior sets this potential to be
   *
   * \f[
   *   F(\chi) = 2p \sum_{e \in E} |e|
   * \f]
   *
   * but posteriors typically include in this potential terms that
   * depend upon the observed data.  So long as the potential is
   * additively decomposable the Arak process has the spatial Markov
   * property.
   */
  class ArakProcess {

  protected:

    /**
     * The coloring whose probability is evaluated.  This is fixed for
     * each process object so that likelihood changes due to coloring
     * updates can be processed efficiently.
     */
    const Coloring& c;

  public:

    /**
     * Default constructor.
     *
     * @param c        The coloring whose probability is evaluated.  
     *                 This is fixed for each process object so that 
     *                 likelihood changes due to coloring updates can 
     *                 be processed efficiently.
     */
    ArakProcess(const Coloring& c) : c(c) { }

    /**
     * Destructor.
     */
    virtual ~ArakProcess() { }

    /**
     * Returns a const reference to the coloring whose probability is
     * assessed by this process.
     */
    const Coloring& getColoring() const { return c; }

    /**
     * Returns the scale of this Arak process.
     */
    virtual double scale() const = 0;

    /**
     * Computes the (log) measure associated with the Arak process for
     * the coloring.  (This is actually multiplied by an elementary
     * Poisson measure over the vertex locations to yield the coloring
     * measure.)
     *
     * @return  the measure of the coloring 
     */
    virtual double logMeasure() const = 0;

    /**
     * Computes the potential associated with the Arak process.  This
     * implementation evaluates to the prior potential.  Posterior
     * Arak processes add to this prior potential terms that depend
     * upon the observed data.  If the posterior potential is
     * additively decomposable then the posterior Arak process has the
     * spatial Markov property.
     * 
     * @return  the potential of the coloring
     * @see ArakProcess
     */
    virtual double potential() const = 0;

    /**
     * Returns the log likelihood of the supplied coloring.
     *
     * @param   temp the temperature (if the likelihood is to be annealed)
     * @return  the log likelihood of the coloring
     */
    double logLikelihood(double temp = 1.0) const {
      return logMeasure() - potential() / temp;
    }
    
    /**
     * Returns the log likelihood of the supplied coloring.
     *
     * @param   c the coloring whose likelihood is evaluated; this must
     *            be the same coloring supplied to the constructor
     * @param   temp the temperature (if the likelihood is to be annealed)
     * @return  the log likelihood of the coloring
     */
    double logLikelihood(Coloring& c, double temp = 1.0) const {
      assert(c == this->c);
      return logLikelihood(temp);
    }

    /**
     * Renders a graphical representation of an Arak process using the
     * supplied widget.  This base implementation does nothing, but
     * derived instances that represent posteriors can specialize this
     * method to visualize their observations.
     *
     * @param widget the widget on which to visualize the process
     */
    virtual void visualize(CGAL::Qt_widget& widget) const { }
  };

  /**
   * Renders a graphical representation of an Arak process using the
   * supplied widget.
   *
   * @param widget the widget on which to visualize the process
   * @param p      the process to visualize
   * @return       the widget after the rendering is finished
   */
  inline CGAL::Qt_widget& 
  operator<<(CGAL::Qt_widget& widget, const ArakProcess& p) {
    p.visualize(widget);
    return widget;
  }

  /** 
   * A standard Arak prior process.  The Arak prior potential is
   *
   * \f[
   *   F(\chi) = 2p \sum_{e \in E} |e|
   * \f]
   *
   * This potential is additively decomposable so the Arak process has
   * the spatial Markov property.
   */
  class ArakPrior : public ArakProcess, public Coloring::Listener {

  protected:

    /**
     * A scale parameter for the Arak process.  The expected number of
     * vertices in a region of area \f$ a \f$ is \f$ 4 \pi (ap)^2 \f$
     * and the expected number of edges is \f$ 4ap + 4 \pi (ap)^2 \f$.
     */
    double p;

    /**
     * The (cached) sum of log lengths of the edges in the coloring.
     * This is equal to
     * 
     * \f[
     *   \sum_{e \in E(\gamma)} \log |e|
     * \f]
     *
     * where \f$E(\gamma)\f$ is the set of (non-false) edges in the
     * coloring.  This quantity is referenced by the base measure, so
     * a cached copy is kept here for efficiency.
     */
    double sumLogLengths;
  
    /**
     * The (cached) sum of log sines of the angles in the coloring.
     * This is equal to
     * 
     * \f[
     *   \sum_{v \in V(\gamma)} \log \sin(\phi_v)
     * \f]
     *
     * where \f$V(\gamma)\f$ is the set of (non-false) vertices in the
     * coloring and \f$\phi_v\f$ is either the smaller angle between
     * the two edges incident to \f$v\f$ (if \f$v\f$ is an interior
     * vertex) or the smaller angle between the only edge incident to
     * \f$v\f$ and the boundary tangent (if \f$v\f$ is a boundary
     * vertex).  This quantity is referenced by the base measure, so a
     * cached copy is kept here for efficiency.
     */
    double sumLogSines;

    /**
     * The (cached) sum of lengths of the edges in the coloring.  This
     * is equal to
     * 
     * \f[
     *   \sum_{e \in E(\gamma)} |e|
     * \f]
     *
     * where \f$E(\gamma)\f$ is the set of (non-false) edges in the
     * coloring.  This quantity is referenced by the potential, so a
     * cached copy is kept here for efficiency.
     */
    double sumLengths;
  
    /**
     * Initializes this Arak process object.
     */
    void initialize();

  public:

    /**
     * Default constructor.
     *
     * @param c        The coloring whose probability is evaluated.  
     *                 This is fixed for each process object so that 
     *                 likelihood changes due to coloring updates can 
     *                 be processed efficiently.
     * @param p        a scale parameter for the Arak process
     */
    ArakPrior(Coloring& c, double p = 1.0) : ArakProcess(c), p(p) { 
      initialize();
    }

    /**
     * Constructor.
     *
     * @param c        The coloring whose probability is evaluated.  
     *                 This is fixed for each process object so that 
     *                 likelihood changes due to coloring updates can 
     *                 be processed efficiently.
     * @param props    a property map that specifies the scale 
     *                 parameter "arak.scale"
     */
    ArakPrior(Coloring& c, const Arak::Util::PropertyMap& props) : 
      ArakProcess(c), p(1.0) { 
      using namespace Arak::Util;
      assert(parse(getp(props, "arak.scale"), p));
      initialize();
    }

    /**
     * Destructor.
     */
    virtual ~ArakPrior() { }

    /**
     * Returns the scale of this Arak process.
     */
    virtual double scale() const { return p; }

    /**
     * Computes the (log) measure associated with the Arak process for
     * the coloring.  (This is actually multiplied by an elementary
     * Poisson measure over the vertex locations to yield the coloring
     * measure.)
     *
     * @return  the measure of the coloring 
     * @see ArakProcess
     */
    virtual double logMeasure() const;

    /**
     * Computes the potential associated with the Arak process.  This
     * implementation evaluates to the prior potential.  Posterior
     * Arak processes add to this prior potential terms that depend
     * upon the observed data.  If the posterior potential is
     * additively decomposable then the posterior Arak process has the
     * spatial Markov property.
     * 
     * @return  the potential of the coloring
     * @see ArakProcess
     */
    virtual double potential() const {
      return 2.0 * p * sumLengths;
    }

    /**
     * This method is invoked to inform this process that the
     * triangle with the supplied vertices has been recolored.
     *
     * @param a the first vertex of the triangle
     * @param b the second vertex of the triangle
     * @param c the third vertex of the triangle
     */
    virtual void recolored(const Geometry::Point& a,
			   const Geometry::Point& b,
			   const Geometry::Point& c) { }
      
    /**
     * This method is invoked to inform this process that the
     * quadrilateral with the supplied vertices has been recolored.
     * The vertices are supplied in either clockwise or
     * counter-clockwise order.  Note that this quadrilateral is
     * simple, but not necessarily convex.
     *
     * @param a the first vertex of the quadrilateral
     * @param b the second vertex of the quadrilateral
     * @param c the third vertex of the quadrilateral
     * @param d the fourth vertex of the quadrilateral
     */
    virtual void recolored(const Geometry::Point& a,
			   const Geometry::Point& b,
			   const Geometry::Point& c,
			   const Geometry::Point& d) { }
    
    /**
     * This method is invoked to inform the listener that a new
     * vertex has been added to the coloring.
     *
     * @param vh a handle to the new vertex
     */
    virtual void vertexHasBeenAdded(Coloring::VertexHandle vh);
    
    /**
     * This method is invoked to inform the listener that an
     * existing vertex is about to be removed from the coloring.
     *
     * @param vh a handle to the vertex that will be removed
     */
    virtual void vertexWillBeRemoved(Coloring::VertexHandle vh);

    /**
     * This method is invoked to inform the listener that a new
     * (interior) edge has been added to the coloring.
     *
     * @param eh a handle to the new edge
     */
    virtual void edgeHasBeenAdded(Coloring::IntEdgeHandle eh);

    /**
     * This method is invoked to inform the listener that an
     * existing (interior) edge will be removed from the coloring.
     *
     * @param eh a handle to the edge that will be removed
     */
    virtual void edgeWillBeRemoved(Coloring::IntEdgeHandle eh);
    
    /**
     * Renders a graphical representation of an Arak process using the
     * supplied widget.  This base implementation does nothing, but
     * derived instances that represent posteriors can specialize this
     * method to visualize their observations.
     *
     * @param widget the widget on which to visualize the process
     */
    virtual void visualize(CGAL::Qt_widget& widget) const { }
  };

}

#endif
