// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _GLOBAL_HPP
#define _GLOBAL_HPP

#include <assert.h>
#include <ios>
#include <iostream>
#include <cmath>
#include <cerrno>
#include <limits>

/** \mainpage 
 *
 * The Arak package includes classes and algorithms related to
 * inference in the Arak process (see Arak::ArakProcess), which is a
 * probability distribution over polygonal colorings of the plane.
 * Such colorings are represented by Arak::Coloring objects.  The
 * process can condition on point observations (e.g., see
 * Arak::ArakPosteriorGaussianObs and
 * Arak::ArakPosteriorBernoulliObs).  Inference is performed via
 * Markov chain Monte Carlo (see Arak::ArakMarkovChain).
 */

// Uses expensive testing code to check for bugs (very slow)
// #define SANITY_CHECK 

namespace Arak {

  /**
   * The possible colors.
   */
  enum Color {
    WHITE = 0,
    BLACK,
    INVALID_COLOR
  };

  /**
   * Computes the opposite of a color.
   */
  static inline Color opposite(Color c) {
    switch (c) {
    case WHITE:
      return BLACK;
    case BLACK:
      return WHITE;
    default:
      assert(false);
    }
  }

  /**
   * An input stream manipulator that reads off everything up to (and
   * including) the next newline.  Example usage:
   * \verbatim
   * int i, j;
   * std::cin << i << skipline << j;
   * \endverbatim
   * 
   * @param in an input stream
   * @returns  the same input stream after all characters up to the 
   *           next newline have been read off
   */
  template<typename charT, typename traits>
  std::basic_istream<charT,traits>&
  skipline(std::basic_istream<charT,traits>& in) {
    charT c;
    while (in.get(c) && (c != '\n'))
      ;
    return in;
  }
  
  /**
   * An input stream manipulator that reads #-style comments off of an
   * input stream (if any exist).  A #-style comment starts with the
   * character '#' and continues to the next newline ('\n').  Even if
   * there are no such comments, this manipulator may read whitespace
   * off of the stream.
   * 
   * Example usage: 
   * \verbatim int i, j;
   * std::cin << i << skipcomments << j; 
   * \endverbatim
   * 
   * @param in an input stream
   * @returns  the same input stream after all leading #-style 
   *           comments have been read off
   */
  template<typename charT, typename traits>
  std::basic_istream<charT,traits>&
  skipcomments(std::basic_istream<charT,traits>& in) {
    while (true) {
      in >> std::ws; // strip leading whitespace
      if (in.peek() == '#')
	in >> skipline;
      else break;
    }
    return in;
  }

  /**
   * Computes the natural logarithm of x.  If x is zero, this returns
   * negative infinity (-std::numeric_limits<double>::infinity()); if
   * x is negative, an error is signalled.
   */
  inline double ln(double x) {
    assert(x >= 0.0);
    if (x == 0.0) 
      return -std::numeric_limits<double>::infinity();
    else 
      return std::log(x);
  }

} // End of namespace: Arak

#endif
