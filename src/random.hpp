// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _RANDOM_HPP
#define _RANDOM_HPP

#include <iostream>
#include <assert.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "geometry.hpp"

namespace Arak {
  namespace Util {

    /**
     * A simple wrapper around the GNU Scientific Library pseudorandom
     * number generator.
     *
     * @see http://www.gnu.org/software/gsl/manual/gsl-ref_17.html
     */
    class Random {

    protected:

      /**
       * The underlying pseudorandom number generator.
       */
      gsl_rng* r;

    public:

      /**
       * Default constructor.  If the environment variables
       * GSL_RNG_TYPE and/or GSL_RNG_SEED are specified, then their
       * values are used to determine the type and initial state of
       * the pseudorandom number generator.  The defaults are to use
       * the Mersenne Twister PRNG with the seed 0.  
       */
      Random() {
	const gsl_rng_type * T;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	r = gsl_rng_alloc (T);
      }

      /**
       * Copy constructor.
       *
       * @param other another PRNG of the same type as this one
       */
      Random(const Random& other) : r(gsl_rng_clone(other.r)) { }

      /**
       * Destructor.
       */
      ~Random() {
	gsl_rng_free(r);
      }

      /**
       * Returns a pseudorandom real number in the range \f$[0, 1)\f$.
       */
      double uniform() { 
	return gsl_rng_uniform(r);
      }

      /**
       * Returns a pseudorandom real number in the range \f$[a, b)\f$.
       */
      double uniform(double a, double b) { 
	assert(a < b);
	double x = gsl_rng_uniform(r);
	return x * a + (1 - x) * b;
      }

      /**
       * Returns a pseudorandom integer in the range \f$[0, n)\f$.
       */
      unsigned long int uniform(unsigned long int n) { 
	return gsl_rng_uniform_int(r, n);
      }

      /**
       * Returns true with probability \f$p\f$, and false with
       * probability \f$1 - p\f$.
       */
      bool bernoulli(double p = 0.5) {
	return (uniform() < p);
      }

      /**
       * Returns a sample from the binomial distribution with success
       * probability \f$p\f$ and number of trials \f$n\f$.  
       */
      unsigned int binomial(double p, unsigned int n) {
	return gsl_ran_binomial(r, p, n);
      }

      /**
       * Samples a point uniformly from a line segment.
       *
       * @param source the source of the line segment
       * @param target the target of the line segment
       * @return  a point drawn u.a.r. from s
       */
      inline Geometry::Point uniform(const Geometry::Point& source,
				     const Geometry::Point& target) {
	Geometry::Vector a(CGAL::ORIGIN, source);
	Geometry::Vector b(CGAL::ORIGIN, target);
	Geometry::Kernel::FT x(uniform());
	Geometry::Vector u = (a * x) + b - (b * x);
	Geometry::Point p(u.x(), u.y());
#ifdef SANITY_CHECK
	assert(CGAL::collinear_are_ordered_along_line(s.source(), p, s.target()));
#endif
	return p;
      }
      
      /**
       * Samples a point uniformly from a line segment.
       *
       * @param s the segment
       * @return  a point drawn u.a.r. from s
       */
      inline Geometry::Point uniform(const Geometry::Segment& s) {
	return uniform(s.source(), s.target());
      }

      /**
       * Samples a point uniformly from a triangle.
       *
       * @param t the triangle
       * @return  a point drawn u.a.r. from t
       * @see Turk G.; Generating Random Points in Triangles; 
       *      Graphics Gems I, p.24
       */
      inline Geometry::Point uniform(const Geometry::Triangle& t) {
	// Generate two uniform random numbers.
	double r1 = uniform();
	double r2 = uniform();
	// Compute the barycentric coordinates of the random point.
	double sqrt_r1 = sqrt(r1);
	double alpha = 1.0 - sqrt_r1;
	double beta = (1.0 - r2) * sqrt_r1;
	double gamma = r2 * sqrt_r1;
	// Translate to Euclidean coordinates.
	return CGAL::ORIGIN 
	  + (Geometry::Vector(CGAL::ORIGIN, t[0]) * alpha)
	  + (Geometry::Vector(CGAL::ORIGIN, t[1]) * beta)
	  + (Geometry::Vector(CGAL::ORIGIN, t[2]) * gamma);
      }

      /**
       * Writes a short description of this PRNG to the supplied
       * stream.
       */
      void write(std::ostream& out) const {
	out << gsl_rng_name(r) 
	    << " (with seed " << gsl_rng_default_seed << ")";
      }
    };

    /**
     * A default source of randomness, initialized using environment
     * variables.
     */
    extern Random default_random;

  } // end namespace: Arak::Util
} // end namespace: Arak

inline std::ostream& operator<<(std::ostream& out, 
				const Arak::Util::Random& r) {
  r.write(out);
  return out;
}

#endif
