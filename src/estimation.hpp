// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _ESTIMATION_HPP
#define _ESTIMATION_HPP

#include <iostream>
#include <iomanip>
#include "query_point.hpp"
#include "coloring_mcmc.hpp"

/**
 * The following header is written at the start of a stream
 * representation of a point set estimator.
 */
#define POINT_SET_ESTIMATE_HEADER "\
#######################################################################\n\
#                                                                      \n\
# This file represents estimates of the colors of points under an      \n\
# Arak process.  The first line below consists of the number of samples\n\
# used to compute the estimates (needed to merge estimates) and then   \n\
# the number of point estimates.  Each line thereafter represents an   \n\
# point color estimate: the first two numbers are the X and Y          \n\
# coordinates of the point, and the third number is the estimated      \n\
# probability that the point is colored black.                         \n\
#                                                                      \n\
#######################################################################\n"

#define GRID_ESTIMATE_HEADER "\
#######################################################################\n\
#                                                                      \n\
# This file represents estimates of the colors of a grid of points     \n\
# under an Arak process.  The first line below consists of the number  \n\
# of samples used to compute the estimates (needed to merge estimates).\n\
# The second line has the number of rows and columns in the grid.  The \n\
# third line gives the boundary of the grid.  Then the estimates are   \n\
# written out as a matrix (one line per row) in reverse-row order (so  \n\
# the first element corresponds to the upper left corner of the grid.  \n\
# Each estimate is the probability the cell's center point is black.   \n\
#                                                                      \n\
#######################################################################\n"

namespace Arak {

  /** 
   * An estimator of the color of a point.
   */
  class PointColorEstimator : public QueryPointListener {

    friend class PointSetColorEstimator;
    friend class GridColorEstimator;

  protected:

    /**
     * The Markov chain generating the samples used by this estimator.
     */
    const ArakMarkovChain& chain;

    /**
     * The location at which the color is being estimated.
     */
    const Geometry::Point point;

    /**
     * The time (in Markov chain samples, not steps) in which the
     * estimator was last updated.
     */
    unsigned long int updated;

    /**
     * This the last color used to update the estimator.
     */
    Color color;

    /**
     * The number of samples in which the point was colored black.
     */
    unsigned long int numBlack;

    /**
     * The number of samples in which the point was colored white.
     */
    unsigned long int numWhite;

    /**
     * Updates this estimator to reflect that the color of the point
     * was updated.
     *
     * @param color   the new color
     * @param time    the time at which the change occured
     */
    void update(Color color, unsigned long int time) {
      assert(updated <= time);
      // This is the number of samples that the point stayed its
      // previous color.
      unsigned long int duration = time - updated;
      if (this->color == BLACK)
	numBlack += duration;
      else if (this->color == WHITE)
	numWhite += duration;
      this->color = color;
      updated = time;
    }

  public:
    
    /**
     * Constructor.
     *
     * @param chain the Markov chain generating the samples
     *              used by this estimator
     * @param point the point at which the color is being estimated
     * @param color the initial color of the point
     */
    PointColorEstimator(const ArakMarkovChain& chain,
			const Geometry::Point& point,
			Color color) : 
      QueryPointListener(), chain(chain), point(point), 
      updated(chain.getNumSamples()), color(color), 
      numBlack(0), numWhite(0) { }

    /**
     * Destructor.
     */
    virtual ~PointColorEstimator() { }

    /**
     * Reacts to information that the query point has been recolored.
     * The estimator is updated.
     *
     * @param color the new color of the query point
     */
    virtual void recolor(Color color) {
      update(color, chain.getNumSamples());
    }

    /**
     * Returns the estimated probability the point is colored black.
     */
    double estimate() const { 
      unsigned long int time = chain.getNumSamples();
      unsigned long int curNumBlack = 
	numBlack + ((color == BLACK) ? time - updated : 0);
      unsigned long int curNumWhite = 
	numWhite + ((color == BLACK) ? 0 : time - updated);
      if ((curNumBlack == 0) && (curNumWhite == 0))
	return 0.5;
      else
	return (double(curNumBlack) / 
		(double(curNumBlack) + double(curNumWhite)));
    }

  };

  /**
   * A set of point color estimators at arbitrary locations.  (For a
   * grid of estimates, use GridColorEstimator instead.)
   */
  class PointSetColorEstimator : public QueryPointListenerFactory {

  protected:

    /**
     * The Markov chain generating the samples used in estimation.
     */
    const ArakMarkovChain& chain;

    /**
     * A list of the point color estimators.
     */
    std::list<PointColorEstimator*> estimators;

    /**
     * An index of the query points at whose locations the color is
     * estimated.
     */
    QueryPointIndex* index;

  public:

    /**
     * Constructor for a grid of point color estimators.  This
     * constructor uses the properties
     * "arak.mcmc.estimation.avg_grid.rows" and
     * "arak.mcmc.estimation.avg_grid.cols" to define a grid of points
     * at which the color is estimated.
     *
     * @param chain the Markov chain generating the samples 
     *              used for estimation
     * @param props a properties map 
     */
    PointSetColorEstimator(ArakMarkovChain& chain,
			   const Arak::Util::PropertyMap& props);

    /**
     * Destructor.
     */
    virtual ~PointSetColorEstimator() {
      while (!estimators.empty()) {
	PointColorEstimator* estimator = estimators.front();
	estimators.pop_front();
	delete estimator;
      }
      // TODO: stop listening to coloring
      delete index;
    }

    /**
     * Creates a new listener for the supplied point.
     */
    virtual PointColorEstimator* create(const QueryPoint& p) {
      PointColorEstimator* estimator = 
	new PointColorEstimator(chain, p, p.color());
      estimators.push_front(estimator);
      return estimator;
    }

    /**
     * Renders a graphical representation of this point set color
     * estimator using the supplied widget.  The point color estimates
     * are rendered as squares of varying gray level.
     *
     * \todo Use a Qt image rendering method
     *
     * @param widget the widget on which to visualize the estimates
     */
    virtual void visualize(CGAL::Qt_widget& widget) const;

    /**
     * Writes an ASCII representation of the current estimates to the
     * supplied stream.  Each line is "x y p" where (x, y) is the
     * coordinate of the point and p is the probability the point is
     * black.
     *
     * @param out    the stream on which to write the estimates
     */
    template<typename charT, typename traits>
    void write(std::basic_ostream<charT,traits>& out) const {
      // Write some comment information about the format of the file.
      out << POINT_SET_ESTIMATE_HEADER;
      // Write out the number of samples and the number of points.
      out << chain.getNumSamples() << " " 
	  << estimators.size() << std::endl;
      // Now write out each point color estimate.
      out << std::scientific << std::setw(16) << std::setprecision(12);
      typedef std::list<PointColorEstimator*>::const_iterator Iterator;
      for (Iterator it = estimators.begin(); it != estimators.end(); it++) {
	const PointColorEstimator* estimator = *it;
	out << estimator->point << " " << estimator->estimate() << std::endl;
      }
    }
  }; // End of class: PointSetColorEstimator

  /**
   * Renders a graphical representation of the point set color
   * estimator using the supplied widget.  The point color estimates
   * are rendered as disks of varying gray level.
   *
   * @param widget the widget on which to visualize the estimates
   * @param p      the estimates to visualize
   * @return       the widget after the rendering is finished
   */
  inline CGAL::Qt_widget& operator<<(CGAL::Qt_widget& widget, 
				     PointSetColorEstimator& p) {
    p.visualize(widget);
    return widget;
  }

  /**
   * Writes an ASCII representation of the current estimates to the
   * supplied stream.  
   *
   * @param out    the stream on which to write the estimates
   * @param p      the estimates to write
   * @return       the stream after the output is finished
   */
  template<typename charT, typename traits>
  std::basic_ostream<charT,traits>& 
  operator<<(std::basic_ostream<charT,traits>& out,
	     const PointSetColorEstimator& p) {
    p.write(out);
    return out;
  }

  /**
   * A grid of point color estimators.
   */
  class GridColorEstimator : public Coloring::Listener {

  protected:

    /**
     * The Markov chain generating the samples used in estimation.
     */
    const ArakMarkovChain& chain;

    /**
     * A list of the point color estimators.
     */
    std::list<PointColorEstimator*> estimators;

    /**
     *
     */
    typedef Grid<PointColorEstimator*> EstimatorGrid;

    /**
     * An index of the query points at whose locations the color is
     * estimated.
     */
    EstimatorGrid* grid;

  public:

    /**
     * Constructor for a grid of point color estimators.  This
     * constructor uses the properties
     * "arak.mcmc.estimation.avg_grid.rows" and
     * "arak.mcmc.estimation.avg_grid.cols" to define a grid of points
     * at which the color is estimated.
     *
     * @param chain the Markov chain generating the samples 
     *              used for estimation
     * @param props a properties map 
     */
    GridColorEstimator(ArakMarkovChain& chain, 
		       const Arak::Util::PropertyMap& props);

    /**
     * Destructor.
     */
    virtual ~GridColorEstimator() {
      while (!estimators.empty()) {
	PointColorEstimator* estimator = estimators.front();
	estimators.pop_front();
	delete estimator;
      }
      delete grid;
    }

    /**
     * This method is invoked to inform the listener that the
     * triangle with the supplied vertices has been recolored.
     *
     * @param a the first vertex of the triangle
     * @param b the second vertex of the triangle
     * @param c the third vertex of the triangle
     */
    virtual void recolored(const Geometry::Point& a,
			   const Geometry::Point& b,
			   const Geometry::Point& c);
    
    /**
     * This method is invoked to inform the listener that the
     * quadrilateral with the supplied vertices has been recolored.
     * The vertices are supplied in either clockwise or
     * counter-clockwise order.  Note that this quadrilateral is
     * simple, but not necessarily convex.
     *
     * @param a the first vertex of the quadrilateral
     * @param b the second vertex of the quadrilateral
     * @param c the third vertex of the quadrilateral
     * @param d the fourth vertex of the quadrilateral
     */
    virtual void recolored(const Geometry::Point& a,
			   const Geometry::Point& b,
			   const Geometry::Point& c,
			   const Geometry::Point& d) {
      recolored(a, b, c);
      recolored(a, d, c);
    }
    
    /**
     * Renders a graphical representation of this grid color estimator
     * using the supplied widget.  The point color estimates are
     * rendered as squares of varying gray level.
     *
     * \todo Use a Qt image rendering method
     *
     * @param widget the widget on which to visualize the estimates
     */
    virtual void visualize(CGAL::Qt_widget& widget) const;

    /**
     * Writes an ASCII representation of the current estimates to the
     * supplied stream.  The first line gives the number of samples
     * used in estimation; the second gives the number of rows and
     * columns of the grid; the third line is the boundary of the
     * grid.  Then the estimates are written out as a matrix (one line
     * per row) in reverse-row order (so the first element corresponds
     * to the upper left corner of the grid).  Each estimate is the
     * probability the cell's center point is black.
     *
     * @param out    the stream on which to write the estimates
     */
    template<typename charT, typename traits>
    void write(std::basic_ostream<charT,traits>& out) const {
      // Write some comment information about the format of the file.
      out << GRID_ESTIMATE_HEADER;
      // Write out the number of samples and the number of points.
      out << chain.getNumSamples() << std::endl;
      // Write out the number of rows and columns.
      out << grid->numRows() << " " << grid->numCols() << std::endl;
      // Write out the boundary of the grid.
      out << grid->boundary() << std::endl;
      // Now write out each point color estimate.
      out << std::scientific << std::setw(16) << std::setprecision(12);
      for (int i = grid->numRows() - 1; i >= 0; i--) {
	for (int j = 0; j < grid->numCols(); j++) {
	  const EstimatorGrid::Cell& cell = grid->getCell(i, j);
	  const PointColorEstimator* pce = *(cell.getItemList().begin());
	  out << pce->estimate() << " ";
	}
	out << std::endl;
      }
    }
  }; // End of class: GridColorEstimator

  /**
   * Renders a graphical representation of the grid color
   * estimator using the supplied widget.  The point color estimates
   * are rendered as disks of varying gray level.
   *
   * @param widget the widget on which to visualize the estimates
   * @param p      the estimates to visualize
   * @return       the widget after the rendering is finished
   */
  inline CGAL::Qt_widget& operator<<(CGAL::Qt_widget& widget, 
				     GridColorEstimator& p) {
    p.visualize(widget);
    return widget;
  }

  /**
   * Writes an ASCII representation of the current estimates to the
   * supplied stream.  
   *
   * @param out    the stream on which to write the estimates
   * @param p      the estimates to write
   * @return       the stream after the output is finished
   */
  template<typename charT, typename traits>
  std::basic_ostream<charT,traits>& 
  operator<<(std::basic_ostream<charT,traits>& out,
	     const GridColorEstimator& p) {
    p.write(out);
    return out;
  }

}

#endif
