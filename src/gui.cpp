// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

// #include "planar_map.hpp"
#include "gui.hpp"

#include <CGAL/Bbox_2.h>
#include <CGAL/IO/Qt_widget.h>
#include <CGAL/IO/Qt_widget_standard_toolbar.h>
#include <qpixmap.h>
#include <qimage.h>

using namespace Arak;

//#include "src/gui.moc"
#include "moc_gui.cxx"

QApplication* Arak::InitGui(int argc,
                            char** argv, 
                            int width,
                            int height, 
                            Coloring& c, 
                            ArakProcess& p,
                            GridColorEstimator& e,
                            QMutex& mutex,
                            bool toolbar,
                            double refreshRateHz) {
  ColoringQTLayer* layer = new ColoringQTLayer(c, p, e, mutex);
  QApplication* app = new QApplication(argc, argv);
  QMainWindow* win = new QMainWindow();
  app->setMainWidget(win);
  win->resize(width, height);
  win->show();

  ColoringQTWidget* w = new ColoringQTWidget(*win, *layer, refreshRateHz);
  w->attach(layer);
  win->setCentralWidget(w);
  CGAL::Bbox_2 bbox = c.boundary().bbox();
  w->set_window(bbox.xmin(),
                bbox.xmax(),
                bbox.ymin(),
                bbox.ymax());
  w->setFilled(true);
  w->resize(width, height);
  w->show();

  if (toolbar) {
    CGAL::Qt_widget_standard_toolbar *stoolbar;
    stoolbar = 
      new CGAL::Qt_widget_standard_toolbar(w, win, "Standard Toolbar");
  }

  layer->draw();

  return app;
}

ColoringQTLayer::ColoringQTLayer(Coloring& c, 
                                 ArakProcess& p, 
                                 GridColorEstimator& e,
                                 QMutex& mutex) 
  : c(c), p(p), e(e), mutex(mutex), curType(COLORING) {
}

void ColoringQTLayer::draw() {
  mutex.lock();
  widget->lock();

  // Draw the white background.
  *widget << CGAL::BackgroundColor(CGAL::WHITE);

  switch (curType) {
  case PROC_AND_BD:
    p.visualize(*widget);
    c.visualize(*widget, true, true, false, false);
    break;
  case COLORING:
    c.visualize(*widget, false, false, true, false);
    break;
  case PT_COL_EST_AND_BD:
    e.visualize(*widget);
    c.visualize(*widget, true, true, false, false);
    break;
  case QUERY_PTS_AND_BD:
    c.visualize(*widget, true, true, false, true);
    break;
  default:
    assert(false);
  }
  
  widget->unlock();
  mutex.unlock();
}

ColoringQTWidget::ColoringQTWidget(QMainWindow& win, 
                                   ColoringQTLayer& layer,
                                   double refreshRateHz)
  : CGAL::Qt_widget(&win) {
  this->layer = &layer;
  this->timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), SLOT(timeout()));
  timer->start(int(1000.0 / refreshRateHz));
}

void ColoringQTWidget::timeout() {
  layer->draw();
}

void ColoringQTWidget::mousePressEvent(QMouseEvent *e) {
  Qt_widget::mousePressEvent(e);
  if(e->button() == Qt::RightButton) {
    if (layer->curType == PROC_AND_BD) {
      layer->curType = COLORING;
    } else if (layer->curType == COLORING) {
      layer->curType = PT_COL_EST_AND_BD;
    } else if (layer->curType == PT_COL_EST_AND_BD) {
      layer->curType = QUERY_PTS_AND_BD;
      // layer->curType = PROC_AND_BD;
    } else if (layer->curType == QUERY_PTS_AND_BD) {
      layer->curType = PROC_AND_BD;
    }
  }
  /*
  {
    Point_2 p = Point_2(x_real(e->x()), y_real(e->y()));
    if(fsqtl->fs->map.number_of_halfedges() == 0)
    {
      fsqtl->fs->Set_free_face(p);
      fsqtl->fs->Compute_frontiers();

      if(fsqtl->fs->final_path.size() > 0)
      {
        fsqtl->istate = fsqtl->fs->final_path.begin();
        fsqtl->fs->Make_polygons_from_info_state(fsqtl->polygons,
                                                 fsqtl->labels,
                                                 *fsqtl->istate);
        fsqtl->save_img = true;
      }
    }
    else
    {
      if(fsqtl->fs->final_path.size() > 0)
      {
        fsqtl->istate++;
        if(fsqtl->istate == fsqtl->fs->final_path.end())
          fsqtl->istate = fsqtl->fs->final_path.begin();
        fsqtl->fs->Make_polygons_from_info_state(fsqtl->polygons,
                                                 fsqtl->labels,
                                                 *fsqtl->istate);
        fsqtl->save_img = true;
      }
    }
  }
  else if(e->button() == Qt::RightButton)
  {
    Point_2 p = Point_2(x_real(e->x()), y_real(e->y()));
    if(!this->setting_theta)
    {
      fsqtl->fs->robot = p;
      fsqtl->fs->Set_free_face(fsqtl->fs->robot);
      this->setting_theta = true;
    }
    else
    {
      Vertex_iteratorList vlist;
      GapEdgeList glist;
      fsqtl->fs->gap_edges.clear();
      fsqtl->fs->Compute_gap_edges(&vlist,glist,fsqtl->fs->robot);
      Direction_2 dir(Ray_2(fsqtl->fs->robot,p));
      Direction_2 min_dir = fsqtl->fs->min_phi_tx(dir);
      Direction_2 max_dir = fsqtl->fs->max_phi_tx(dir);
      fsqtl->fs->Add_FOV_boundaries(fsqtl->fs->gap_edges,glist,
                                    fsqtl->fs->robot,min_dir,max_dir);
      this->setting_theta = false;
    }
  }
  else if(e->button() == Qt::MidButton)
  {
    if(fsqtl->draw_boundary)
      fsqtl->draw_boundary = false;
    else
      fsqtl->draw_boundary = true;
  }
  redraw();
  if(fsqtl->save_img)
  {
    //QPixmap qp = QPixmap::grabWindow(this->winId(),0,50,-1,this->height());
    QPixmap qp = QPixmap::grabWindow(this->winId());
    QImage qi = qp.convertToImage();
    char fname[32];
    sprintf(fname,"plan-phi-img-%03d.png",fsqtl->img_idx++);
    std::cout << "Saving screenshot:" << fname << ":" << std::endl;
    if(!qi.save(fname, "PNG"))
      std::cout << "QImage::save() failed" << std::endl;
    fsqtl->save_img = false;
  }
  */
}


