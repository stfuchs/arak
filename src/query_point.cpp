// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#include "query_point.hpp"
#include <CGAL/Orthogonal_k_neighbor_search.h>

using namespace Arak;

QueryPointIndex::QueryPointIndex(const Geometry::Rectangle& r, 
				 int rows, int cols) {
  // Allocate the color variables and listener lists
  int n = rows * cols;
  color = new Color[n];
  listenerLists = new QueryPointListenerList[n];
  // Create the query points.
  qpoints.reserve(n);
  typedef Geometry::Kernel::FT FT;
  FT gridx = (r.xmax() - r.xmin()) / FT(rows + 1);
  FT gridy = (r.ymax() - r.ymin()) / FT(cols + 1);
  FT x = r.xmin() + gridx; 
  int k = 0;
  for (int i = 0; i < rows; i++) {
    FT y = r.ymin() + gridy; 
    for (int j = 0; j < cols; j++) {
      color[k] = INVALID_COLOR;
      qpoints.push_back(QueryPoint(Geometry::Point(x, y),
				   &(listenerLists[k]), 
				   &(color[k])));
      k++;
      y += gridy; 
    }
    x += gridx; 
  }
  // Initialize the KD tree.
  tree = new KDTree(qpoints.begin(), qpoints.end());
}

QueryPointIndex::QueryPointIndex(const QueryPointIndex& q1, 
				 const QueryPointIndex& q2) {
  int m = q1.size();
  int n = q2.size();
  // Allocate the color variables and listener lists
  color = new Color[m + n];
  listenerLists = new QueryPointListenerList[m + n];
  for (int i = 0; i < m; i++) {
    color[i] = q1.color[i];
    listenerLists[i] = q1.listenerLists[i];
  }
  for (int i = 0; i < n; i++) {
    color[i + m] = q2.color[i];
    listenerLists[i + m] = q2.listenerLists[i];
  }
  // Create the query points.
  qpoints.reserve(m + n);
  for (int i = 0; i < m; i++) 
    qpoints.push_back(QueryPoint(q1.point(i),
				 &(listenerLists[i]), 
				 &(color[i])));
  for (int i = 0; i < n; i++) 
    qpoints.push_back(QueryPoint(q2.point(i),
				 &(listenerLists[i + m]), 
				 &(color[i + m])));
  // Initialize the KD tree.
  tree = new KDTree(qpoints.begin(), qpoints.end());
}

QueryPointIndex::~QueryPointIndex() { 
  delete [] color;
  delete [] listenerLists;
  delete tree;
}

void QueryPointIndex::addListeners(QueryPointListenerFactory& factory) {
  for (int i = 0; i < int(qpoints.size()); i++) 
    listenerLists[i].push_back(factory.create(qpoints[i]));
}

const QueryPoint& QueryPointIndex::closest(const Geometry::Point& p) const { 
  typedef CGAL::Orthogonal_k_neighbor_search<SearchTraits> NeighborSearch;
  SearchTraits::Point_d q = p;
  NeighborSearch ns(const_cast<KDTree&>(*tree), q);
  return ns.begin()->first;
}

void QueryPointIndex::recolor(const Geometry::Point& a,
			      const Geometry::Point& b,
			      const Geometry::Point& c) {
  const Geometry::Point* xmin = &a;
  const Geometry::Point* xmax = &a;
  const Geometry::Point* ymin = &a;
  const Geometry::Point* ymax = &a;
  if (xmin->x() > b.x()) xmin = &b;
  if (xmin->x() > c.x()) xmin = &c;
  if (xmax->x() < b.x()) xmax = &b;
  if (xmax->x() < c.x()) xmax = &c;
  if (ymin->y() > b.y()) ymin = &b;
  if (ymin->y() > c.y()) ymin = &c;
  if (ymax->y() < b.y()) ymax = &b;
  if (ymax->y() < c.y()) ymax = &c;
  Geometry::Rectangle r(*xmin, *xmax, *ymin, *ymax);
#ifdef SANITY_CHECK
  assert(!r.has_on_unbounded_side(a));
  assert(!r.has_on_unbounded_side(b));
  assert(!r.has_on_unbounded_side(c));
#endif
  Geometry::Triangle t(a, b, c);
  RecolorTriangleFunction f(t);
  apply(r, f);
}

void QueryPointIndex::recolor(const Geometry::Point& a,
			      const Geometry::Point& b,
			      const Geometry::Point& c,
			      const Geometry::Point& d) {
#ifdef SANITY_CHECK
  assert(!do_intersect(Geometry::Segment(a, b), 
		       Geometry::Segment(c, d)));
#endif
  const Geometry::Point* xmin = &a;
  const Geometry::Point* xmax = &a;
  const Geometry::Point* ymin = &a;
  const Geometry::Point* ymax = &a;
  if (xmin->x() > b.x()) xmin = &b;
  if (xmin->x() > c.x()) xmin = &c;
  if (xmin->x() > d.x()) xmin = &d;
  if (xmax->x() < b.x()) xmax = &b;
  if (xmax->x() < c.x()) xmax = &c;
  if (xmax->x() < d.x()) xmax = &d;
  if (ymin->y() > b.y()) ymin = &b;
  if (ymin->y() > c.y()) ymin = &c;
  if (ymin->y() > d.y()) ymin = &d;
  if (ymax->y() < b.y()) ymax = &b;
  if (ymax->y() < c.y()) ymax = &c;
  if (ymax->y() < d.y()) ymax = &d;
  Geometry::Rectangle r(*xmin, *xmax, *ymin, *ymax);
#ifdef SANITY_CHECK
  assert(!r.has_on_unbounded_side(a));
  assert(!r.has_on_unbounded_side(b));
  assert(!r.has_on_unbounded_side(c));
  assert(!r.has_on_unbounded_side(d));
#endif
  Geometry::Triangle t1(a, b, c);
  Geometry::Triangle t2(a, c, d);
#ifdef SANITY_CHECK
  assert(!t1.is_degenerate());
  assert(!t2.is_degenerate());
#endif
  RecolorQuadrilateralFunction f(t1, t2);
  apply(r, f);
}

