// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _PARSECL_H
#define _PARSECL_H

#include <list>
#include <vector>
#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <typeinfo>

namespace Arak {

  namespace Util {

    /** 
     * This class parses the command line arguments of a C/C++
     * program.
     */
    class CommandLine {

    public:

      /**
       * An option that is either supplied or not.
       */
      class Option {

	friend class CommandLine;

      protected:

	/**
	 * The short form of the option.  This is usually preceded by
	 * a single dash and consists of a single letter, as in "-h".
	 */
	const char* sflag;

	/**
	 * The lflag form of the option.  This is usually preceded by
	 * a double dash, as in "--help".
	 */
	const char* lflag;

	/**
	 * A description of the option.  This should be a string with
	 * no formatting (e.g., no newlines).
	 */
	const char* desc;

	/**
	 * This flag is true iff the option was supplied on the
	 * command line.
	 */
	bool _supplied;

	/**
	 * This flag is true iff the option is required.
	 */
	const bool required;

	/**
	 * Returns true if the supplied flag matches this option.  A
	 * flag matches if it is a prefix of the option's short or
	 * long flag names.
	 * 
	 * @param flag the command line flag
	 * @return     true if the flag matches this option
	 */
	bool matches(const char* flag) const {
	  int len = strlen(flag);
	  return ((strncmp(flag, sflag, len) == 0) ||
		  (strncmp(flag, lflag, len) == 0));
	}

	/**
	 * This is a placeholder for functionality in a derived class.
	 * It should not be called.
	 * 
	 * @param str a string representation of this option's value
	 * @return     true if the read was successful
	 */
	virtual bool read(const char* str) {
	  return false;
	}

	/**
	 * Prints the usage of this option to the supplied output
	 * stream.
	 */
	virtual void printUsage(std::ostream& out) const {
	  out << "  " 
	      << sflag << "|" 
	      << lflag << " : " 
	      << desc;
	}

      public:

	/**
	 * Constructor.
	 *
	 * @param sflag    The short form of the option.  This is 
	 *                 usually preceded by a single dash and 
	 *                 consists of a single letter, as in "-h".
	 * @param lflag    The long form of the option.  This is 
	 *                 usually preceded by a double dash, as in 
	 *                 "--help".
	 * @param desc     A description of the option.  This should 
	 *                 be a string with no formatting (e.g., no 
	 *                 newlines).
	 * @param required true if this option is required
	 */
	Option(const char* sflag, const char* lflag, const char* desc,
	       bool required = false) 
	  : sflag(sflag), lflag(lflag), desc(desc),
	    _supplied(false), required(false) { }

	/**
	 * Destructor.
	 */
	virtual ~Option() { }	

	/**
	 * Returns true (after parsing) if this option was supplied on
	 * the command line.
	 */
	virtual bool supplied() const { return _supplied; }

	/**
	 * Returns true if this option takes an argument.
	 */
	virtual bool hasArg() { return false; }
	
      };

      /**
       * A parameter that has a default value and may be overridden on
       * the command line.
       */
      template <class T>
      class Parameter : public Option {

	friend class CommandLine;

      private:

	/**
	 * The value of this parameter.
	 */
	T val;

	/**
	 * Prints the usage of this parameter to the supplied output
	 * stream.
	 */
	virtual void printUsage(std::ostream& out) const {
	  out << "  " 
	      << sflag << "|" 
	      << lflag << " <" 
	      << typeid(val).name() << "> : " 
	      << desc;
	}

      protected:

	/**
	 * Reads the value of this parameter from the supplied string.
	 * 
	 * @param str a string representation of this parameter's value
	 * @return     true if the read was successful
	 */
	virtual bool read(const char* str) {
	  std::string string(str);
	  std::istringstream in(string);
	  in >> val;
	  return !in.bad();
	}

      public:

	/**
	 * Constructor.
	 *
	 *
	 * @param sflag    The short form of the option.  This is 
	 *                 usually preceded by a single dash and 
	 *                 consists of a single letter, as in "-h".
	 * @param lflag    The long form of the option.  This is 
	 *                 usually preceded by a double dash, as in 
	 *                 "--help".
	 * @param desc     A description of the option.  This should 
	 *                 be a string with no formatting (e.g., no 
	 *                 newlines).
	 * @param def      The default value of the parameter.
	 * @param required true if this option is required
	 */
	Parameter(const char* sflag, 
		  const char* lflag, 
		  const char* desc,
		  const T& def,
		  bool required = false) 
	  : Option(sflag, lflag, desc, required), val(def) { }

	/**
	 * Destructor.
	 */
	virtual ~Parameter() { }	

	/**
	 * Returns the value of this parameter.
	 */
	const T& value() const { return val; }

	/**
	 * Returns true if this option takes an argument.
	 */
	virtual bool hasArg() { return true; }
	
      }; // end of class: CommandLine::Parameter

      /**
       * A parameter that has can be specified multiple times on
       * the command line.
       */
      template <class T>
      class MultiParameter : public Option {

	friend class CommandLine;

      private:

	/**
	 * The value of this parameter.
	 */
	std::vector<T> vals;

	/**
	 * Prints the usage of this parameter to the supplied output
	 * stream.
	 */
	virtual void printUsage(std::ostream& out) const {
	  T t;
	  out << "  " 
	      << sflag << "|" 
	      << lflag << " <" 
	      << typeid(t).name() << "> : " 
	      << desc;
	}

      protected:

	/**
	 * Reads the value of this parameter from the supplied string.
	 * 
	 * @param str a string representation of this parameter's value
	 * @return     true if the read was successful
	 */
	virtual bool read(const char* str) {
	  std::string string(str);
	  std::istringstream in(string);
	  T val;
	  in >> val;
	  vals.push_back(val);
	  return !in.bad();
	}

      public:

	/**
	 * Constructor.
	 *
	 * @param sflag    The short form of the option.  This is 
	 *                 usually preceded by a single dash and 
	 *                 consists of a single letter, as in "-h".
	 * @param lflag    The long form of the option.  This is 
	 *                 usually preceded by a double dash, as in 
	 *                 "--help".
	 * @param desc     A description of the option.  This should 
	 *                 be a string with no formatting (e.g., no 
	 *                 newlines).
	 */
	MultiParameter(const char* sflag, 
		       const char* lflag, 
		       const char* desc) 
	  : Option(sflag, lflag, desc, false), vals() { }

	/**
	 * Destructor.
	 */
	virtual ~MultiParameter() { }	

	/**
	 * Returns the value of this parameter.
	 */
	const std::vector<T>& values() const { return vals; }

	/**
	 * Returns true if this option takes an argument.
	 */
	virtual bool hasArg() { return true; }
	
      };

    protected:
      
      /**
       * The name of the executable.
       */
      const char* name;

      /**
       * A list of the options available.  This class manages the
       * memory used by these objects.
       */
      std::list<Option*> options;

    public:

      /**
       * Constructor.
       */
      CommandLine() : name(""), options() { }

      /**
       * Destructor.
       */
      ~CommandLine() { }
      
      /**
       * Parses the command line arguments.
       *
       * @param argc the number of command line arguments
       * @param argv an array of the command line arguments
       * @param err  the stream to which any parsing errors are written
       * @return true if the parsing encountered an error
       */
      bool parse(int argc, char** argv, std::ostream& err) {
	typedef std::list<Option*> OptionList;
	name = argv[0];
	for (int i = 1; i < argc; i++) {
	  const char* flag = argv[i];
	  // Search for a unique match to the flag.
	  Option* match = NULL;
	  for (OptionList::const_iterator it = options.begin();
	       it != options.end(); it++) {
	    Option* opt = *it;
	    if (opt->matches(flag)) {
	      if (match != NULL) {
		err << "error: ambiguous flag: " << flag << std::endl;
		return false;
	      }
	      match = opt;
	    }
	  }
	  // Make sure there is a match.
	  if (match == NULL) {
	    err << "error: unrecognized flag: " << flag << std::endl;
	    return false;
	  }
	  // Parse the flag.
	  match->_supplied = true;
	  if (match->hasArg()) {
	    i++;
	    const char* arg = argv[i];
	    if (!(match->read(arg))) {
	      err << "error: cannot parse value " << arg 
		  << " for flag " << flag << std::endl;
	      return false;
	    }
	  }
	}
	// Check that all required options have been supplied.
	for (OptionList::const_iterator it = options.begin();
	     it != options.end(); it++) {
	  Option* opt = *it;
	  if (opt->required && !(opt->_supplied)) {
	    err << "error: required flag " << opt->sflag
		<< " not supplied" << std::endl;
	    return false;
	  }
	}
	return true;
      }

      /**
       * Prints the usage information to the supplied stream.
       *
       * @param out the stream to write the usage information to
       */
      void printUsage(std::ostream& out) const {
	out << name << std::endl;
	typedef std::list<Option*> OptionList;
	for (OptionList::const_iterator it = options.begin();
	     it != options.end(); it++) {
	  Option* opt = *it;
	  opt->printUsage(out);
	  out << std::endl;
	}
      }

      /**
       * Adds a new option to the options.
       *
       * @param opt The new option
       */
      void add(Option& opt) {
	options.push_front(&opt);
      }

    }; // end of class: CommmandLine

  } // end of namespace: Arak::Util

} // end of namespace: Arak

#endif
