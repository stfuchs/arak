// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _POINT_OBS_HPP
#define _POINT_OBS_HPP

#include <iostream>
#include "properties.hpp"
#include "query_point.hpp"
#include "arak.hpp"

namespace Arak {

  /**
   * An object that listens to the color of a point and updates a
   * potential to reflect its current cost.
   */
  class QueryPointPotential : public QueryPointListener {

    /**
     * The potential value that is updated.
     */
    double& potential;

    /**
     * The current color of the point.
     */
    Color color;

    /**
     * The amount by which the potential should be increased if the
     * point is black.
     */
    const double blackVal;

    /**
     * The amount by which the potential should be increased if the
     * point is white.
     */
    const double whiteVal;

  public:

    /**
     * Constructor.  
     *
     * @param potential the potential that is updated 
     * @param blackVal  the amount the potential is increased when 
     *                  the point is black
     * @param whiteVal  the amount the potential is increased when 
     *                  the point is white
     */
    QueryPointPotential(double &potential,
			double blackVal,
			double whiteVal) : 
      potential(potential), 
      color(INVALID_COLOR), 
      blackVal(blackVal), 
      whiteVal(whiteVal) { }

    /**
     * Reacts to information that the query point has been recolored.
     * If the color has changed, the potential is updated accordingly.
     *
     * @param color the query point's new color
     */
    virtual void recolor(Color color) {
      if (color == this->color) return;
      // Subtract off the old potential.
      switch (this->color) {
      case BLACK:
	potential -= blackVal; 
	break;
      case WHITE:
	potential -= whiteVal; 
	break;
      default:
	break;
      }
      // Add in the new potential.
      switch (color) {
      case BLACK:
	potential += blackVal; 
	break;
      case WHITE:
	potential += whiteVal; 
	break;
      default:
	break;
      }
      // Update the current color.
      this->color = color;
    }
  };

  /** 
   * An posterior Arak process conditioned on color measurements with
   * Gaussian noise.  The posterior potential is given by
   *
   * \f[
   *   F(\chi) = F_{\textit{prior}}(\chi) + 
   *    \sum_{i = 1}^n (\mu_{\chi(t_i)} - y_i)^2 / 2\sigma_{\chi(t_i)}^2
   * \f]
   *
   * where \f$t_i\f$ is the location of a test point, \f$\chi(t_i)\f$ is
   * its true color, \f$y_i\f$ is the observed value, and \f$\mu_c\f$
   * and \f$\sigma_c^2\f$ are the conditional mean and variance of the
   * observation given the true color is \f$c\f$.
   *
   * This potential is additively decomposable so the posterior Arak
   * process has the spatial Markov property.
   */
  class ArakPosteriorGaussianObs : public ArakProcess {

  protected:

    /**
     * The prior process.
     */
    const ArakProcess& prior;

    /**
     * A vector of points for which observations are available.
     */
    std::vector<Geometry::Point> points;

    /**
     * A vector of the observations for each point.  The indices used in
     * #points index into this vector.
     */
    std::vector<double> obs;

    /**
     * A vector of potentials, one per observation.
     */
    std::vector<QueryPointPotential*> potentials;

    /**
     * The mean color observation for a white point.
     */
    double whiteMean;
  
    /**
     * The mean color observation for a black point.
     */
    double blackMean;

    /**
     * The variance of observations of white points.
     */
    double whiteVar;

    /**
     * The variance of observations of black points.
     */
    double blackVar;

    /**
     * The current potential arising from observations.
     */
    double lp;

  public:

    /**
     * Stream constructor.  The parameters of the posterior are read
     * from a file with the following format:
     *
     * \verbatim
     * n
     * point-1-x point-1-y point-1-obs
     * point-2-x point-2-y point-2-obs
     * ...
     * point-n-x point-n-y point-n-obs
     * \endverbatim
     *
     * @param c  The coloring whose probability is evaluated.  
     *           This is fixed for each process object so that 
     *           likelihood changes due to coloring updates can 
     *           be processed efficiently.
     * @param props the properties that define the posterior
     */
    ArakPosteriorGaussianObs(const ArakProcess& prior,
			     const Arak::Util::PropertyMap& props);

    /**
     * Destructor.
     */
    virtual ~ArakPosteriorGaussianObs();

    /**
     * Returns the scale of this Arak process.
     */
    virtual double scale() const { return prior.scale(); }

    /**
     * Computes the (log) measure associated with the Arak process for
     * the coloring.  (This is actually multiplied by an elementary
     * Poisson measure over the vertex locations to yield the coloring
     * measure.)
     *
     * @return  the measure of the coloring 
     */
    virtual double logMeasure() const;

    /**
     * Computes the potential associated with this posterior Arak
     * process.
     * 
     * @return  the current potential
     * @see ArakProcess
     */
    virtual double potential() const;

    /**
     * Renders a graphical representation of this Arak posterior using
     * the supplied widget.  The Gaussian observations are rendered as
     * disks of varying gray level.
     *
     * @param widget the widget on which to visualize the process
     */
    virtual void visualize(CGAL::Qt_widget& widget) const;
  };

  /**
   * Renders a graphical representation of an Arak posterior using the
   * supplied widget.  The Gaussian observations are rendered as disks
   * of varying gray level.
   *
   * @param widget the widget on which to visualize the process
   * @param p      the process to visualize
   * @return       the widget after the rendering is finished
   */
  CGAL::Qt_widget& operator<<(CGAL::Qt_widget& widget, 
			      ArakPosteriorGaussianObs& p);

  /** 
   * An posterior Arak process conditioned on color measurements with
   * Bernoulli noise.  The posterior potential is given by
   *
   * \f[
   *   F(\chi) = F_{\textit{prior}}(\chi) + 
   *    \sum_{i = 1}^n (\mu_{\chi(t_i)} - y_i)^2 / 2\sigma_{\chi(t_i)}^2
   * \f]
   *
   * where \f$t_i\f$ is the location of a test point, \f$\chi(t_i)\f$ is
   * its true color, \f$y_i\f$ is the observed value, and \f$\mu_c\f$
   * and \f$\sigma_c^2\f$ are the conditional mean and variance of the
   * observation given the true color is \f$c\f$.
   *
   * This potential is additively decomposable so the posterior Arak
   * process has the spatial Markov property.
   */
  class ArakPosteriorBernoulliObs : public ArakProcess {

  protected:

    /**
     * The prior process.
     */
    const ArakProcess& prior;

    /**
     * A vector of points for which observations are available.
     */
    std::vector<Geometry::Point> points;

    /**
     * A vector of potentials, one per observation.
     */
    std::vector<QueryPointPotential*> potentials;

    /**
     * A vector of the observations for each point.  The indices used in
     * #points index into this vector.
     */
    std::vector<bool> obs;

    /**
     * The conditional probability a black point will be observed as
     * black.
     */
    double blackProb;
  
    /**
     * The conditional probability a white point will be observed as
     * white.
     */
    double whiteProb;

    /**
     * The current potential arising from observations.
     */
    double lp;

  public:

    /**
     * Stream constructor.  The parameters of the posterior are read
     * from a file with the following format:
     *
     * \verbatim
     * n
     * point-1-x point-1-y point-1-obs
     * point-2-x point-2-y point-2-obs
     * ...
     * point-n-x point-n-y point-n-obs
     * \endverbatim
     *
     * @param c  The coloring whose probability is evaluated.  
     *           This is fixed for each process object so that 
     *           likelihood changes due to coloring updates can 
     *           be processed efficiently.
     * @param props the property map defining the posterior
     */
    ArakPosteriorBernoulliObs(const ArakProcess& prior,
			      const Arak::Util::PropertyMap& props);

    /**
     * Destructor.
     */
    virtual ~ArakPosteriorBernoulliObs();

    /**
     * Returns the scale of this Arak process.
     */
    virtual double scale() const { return prior.scale(); }

    /**
     * Computes the (log) measure associated with the Arak process for
     * the coloring.  (This is actually multiplied by an elementary
     * Poisson measure over the vertex locations to yield the coloring
     * measure.)
     *
     * @return  the measure of the coloring 
     */
    virtual double logMeasure() const;

    /**
     * Computes the potential associated with this posterior Arak
     * process.
     * 
     * @return  the current potential 
     * @see ArakProcess
     */
    virtual double potential() const;

    /**
     * Renders a graphical representation of this Arak posterior using
     * the supplied widget.  The Bernoulli observations are rendered as
     * disks of varying gray level.
     *
     * @param widget the widget on which to visualize the process
     */
    virtual void visualize(CGAL::Qt_widget& widget) const;
  };

  /**
   * Renders a graphical representation of an Arak posterior using the
   * supplied widget.  The Bernoulli observations are rendered as disks
   * of varying gray level.
   *
   * @param widget the widget on which to visualize the process
   * @param p      the process to visualize
   * @return       the widget after the rendering is finished
   */
  CGAL::Qt_widget& operator<<(CGAL::Qt_widget& widget, 
			      ArakPosteriorBernoulliObs& p);

}

#endif
