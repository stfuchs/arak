// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _COLORING_HPP
#define _COLORING_HPP

#include <assert.h>
#include <vector>
#include <list>
#include <iostream>
#include <CGAL/Triangulation_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include "global.hpp"
#include "random.hpp"
#include "handle.hpp"
#include "store.hpp"
#include "properties.hpp"
#include "geometry.hpp"
#include "grid.hpp"

// One or the other of the following must be uncommented
#define GRID_EDGE_INDEX
// #define TRIANGULATION_EDGE_INDEX // This is much (4x) slower

#if defined(TRIANGULATION_EDGE_INDEX) && !defined(EXACT_GEOM_PRED)
#error "The triangulation index requires exact geometric predicates."
#endif

namespace CGAL {
  class Qt_widget;
}

namespace Arak {

  class QueryPointIndex;

  /** 
   * Represents a binary polygonal coloring of a planar region whose
   * boundary is an axis aligned rectangle.  The colored region is
   * called the <i>window</i>.  (Extending this implementation to the
   * case of an arbitrary simple polygonal window would be easy.)  The
   * coloring is polygonal, which means that color discontinuities occur
   * only along line segments that form polylinear boundaries between
   * subregions of different colors.
   *
   * This class represents binary polygonal colorings using a graph
   * consisting of interior and boundary vertices and edges.  Each
   * interior edge is a line segment representing a color discontinuity;
   * interior edges may not cross.  Each interior vertex is the point at
   * the intersection of two interior edges.  Each boundary vertex is
   * the intersection of an interior edge and the boundary of the
   * window.  Boundary edges are line segments along the boundary that
   * join two boundary vertices; these are a representational
   * convenience, and do not represent color discontinuities.
   *
   * There are two types of color discontinuities made from interior
   * edges: <i>polygons</i> consist entirely of interior edges joining
   * interior vertices, and <i>polylines</i> start at one boundary
   * vertex and terminate at another.  Both of these discontinuities are
   * called <i>paths</i>.  A path is represented by a doubly-linked list
   * of alternating vertices and edges.  In the case of a polygon, this
   * list is circular and is rooted at an arbitrary vertex; in the case
   * of a polyline, it is rooted at one of the boundary vertices and
   * terminates at the other.  
   *
   * This data structure uses two types of indexes to speed
   * computations.  The first is an index that splits the colored
   * region into a grid of cells that record all edges that intersect
   * the cell; this is used to efficiently check for crossing edges
   * and for ray tracing operations.  The second is an index of query
   * points in the region's interior that record their current color.
   * From (even one of) these points the color of any arbitrary point
   * can be computed by counting the number of interior edges that
   * cross a test segment from the test point to the nearest query
   * point.
   */
  class Coloring {

  public:

    class Vertex;
    class InteriorEdge;
    class BoundaryEdge;

    typedef PointerHandle<Vertex> VertexHandle;

    typedef PointerHandle<InteriorEdge> IntEdgeHandle;

    typedef PointerHandle<BoundaryEdge> BdEdgeHandle;

  public:

    /**
     * This is the base class for objects that annotate the faces of a
     * triangulation representing a coloring.
     */
    struct FaceColorInfo {
      /**
       * The color of the face.
       */
      Color color;
    };

    /**
     * These traits describe the types used to form a triangulation
     * representation of a coloring.  FaceInfoType must be derived
     * from FaceColorInfo.
     */
    template <typename FaceInfoType = FaceColorInfo,
              typename VertexBaseType = CGAL::Triangulation_vertex_base_2<Geometry::Kernel> >
    struct TriangulationTraits {
      /**
       * The face base with extra information.
       */
      typedef typename 
      CGAL::Triangulation_face_base_with_info_2<FaceInfoType,
                                                Geometry::Kernel>
      FaceBaseWithInfo;

      /**
       * The constrained triangulation face base.
       */
      typedef typename 
      CGAL::Constrained_triangulation_face_base_2<Geometry::Kernel,
                                                  FaceBaseWithInfo>
      TFb;

      /**
       * The triangulation data structure type.  This is parameterized
       * by our vertex and face base types.
       */
      typedef typename 
      CGAL::Triangulation_data_structure_2<VertexBaseType, TFb> Tds;

      /**
       * This tag is supplied to the constrained triangulation type to
       * inform it that constrained edges will not intersect (except
       * possibly at their endpoints).
       */
      typedef CGAL::No_intersection_tag itag;

      /**
       * The triangulation type.  We use a constrained Delaunay
       * triangulation that is parameterized by the geometric kernel,
       * the triangulation data structure type, and a tag indicating
       * that constrained edges will not intersect (except at their
       * endpoints).
       */
      typedef typename
      CGAL::Constrained_Delaunay_triangulation_2<Geometry::Kernel, Tds, itag> 
      Triangulation;
    };

#ifdef TRIANGULATION_EDGE_INDEX

  protected:

    /**
     * The vertex base type used in the triangulation index.  This
     * annotates each vertex of the triangulation with the
     * corresponding vertex of the coloring.
     */
    template <class Vb = CGAL::Triangulation_vertex_base_2<Geometry::Kernel> >
    class TVb : public Vb {
      typedef Vb                              Base;
    public:
      typedef typename Vb::Vertex_handle      Vertex_handle;
      typedef typename Vb::Face_handle        Face_handle;
      typedef typename Vb::Point              Point;
      typedef std::pair<Face_handle,int>      Edge;
      
      template < typename TDS2 >
      struct Rebind_TDS {
        typedef typename Vb::template Rebind_TDS<TDS2>::Other    Vb2;
        typedef TVb<Vb2>                 Other;
      };
      
      /**
       * The associated vertex of the coloring.
       */
      VertexHandle vertex;
    
      /**
       * Constructors.
       */
      TVb() : Base(), vertex(NULL) {}
      TVb(const Point & p) : Base(p), vertex(NULL) {}
      TVb(const Point & p, Face_handle f) : Base(f,p), vertex(NULL) {}
      TVb(Face_handle f) : Base(f), vertex(NULL) {}
    };

    /**
     * The triangulation index type.  We use a constrained Delaunay
     * triangulation that is parameterized by the geometric kernel,
     * the triangulation data structure type, and a tag indicating
     * that constrained edges will not intersect (except at their
     * endpoints).
     */
    typedef TriangulationTraits<FaceColorInfo, TVb<> >::Triangulation 
    TriangulationIndex;

    /**
     * A handle on a face of the triangulation index.
     */
    typedef TriangulationIndex::Face_handle TFaceHandle;
    
    /**
     * A handle on a vertex of the triangulation index.
     */
    typedef TriangulationIndex::Vertex_handle TVertexHandle;

    /**
     * A handle on a vertex of the triangulation index.
     */
    typedef TriangulationIndex::Edge TEdge;

    /**
     * This is a circulator that travels the faces intersected by a
     * line.
     */
    typedef TriangulationIndex::Line_face_circulator TFaceCirculator;

    /**
     * This is a forwards iterator over the faces of a triangulation
     * that are intersected by a given line segment or ray.
     */
    class Tracer {

    protected:

      /**
       * The source of the line segment.
       */
      Geometry::Point source;

      /**
       * The target of the line segment, or a point on the ray
       * distinct from #source.
       */
      Geometry::Point target;

      /**
       * The triangulation whose faces are enumerated.
       */
      const Coloring::TriangulationIndex* tri;

      /**
       * An underlying line face circulator over the triangulation
       * #tri that is used to implement the segment tracer.
       */
      Coloring::TFaceCirculator circ;

      /**
       * The most recently crossed constrained edge.
       */
      Coloring::TEdge edge;

      /**
       * If true, then the circulator must be incremented to move towards
       * the target.  Otherwise, it must be decremented.
       */
      bool forwards;

      /**
       * If true, then this iterator is valid.
       */
      bool valid;

      /**
       * This is true if the object to be traced is a ray, and false
       * if it is a segment.
       */
      bool isRay;

    public:

      /**
       * Begin constructor.
       *
       * @param source the source of the segment or ray
       * @param target the target of the segment (or a point on the 
       *               ray distinct from source)
       * @param tri    the triangulation 
       * @param isRay  true if the tracer should traverse the ray
       * @param hint   a face of the triangulation that is near
       *               the source
       */
      Tracer(const Geometry::Point& source,
             const Geometry::Point& target,
             const Coloring::TriangulationIndex& tri,
             bool isRay = false,
             const Coloring::TFaceHandle hint = 
             Coloring::TFaceHandle());
      
      /**
       * End constructor.
       */
      Tracer()
        : source(CGAL::ORIGIN), 
          target(CGAL::ORIGIN), 
          tri(NULL), circ(),
          edge(Coloring::TFaceHandle(), 0), 
          forwards(true), valid(false), isRay(false) { }
      
      /**
       * Comparison operators.
       */
      inline bool operator==(const Tracer& other) const
      { return ((valid == other.valid) &&
                (!valid || (valid && (circ == other.circ)))); }
      inline bool operator!=(const Tracer& other) const 
      { return !(*this == other); }
      
      /**
       * Increment operators.
       */
      Tracer operator++(int) {
        Tracer tmp(*this);
        ++(*this);
        return tmp;
      }
      Tracer& operator++();

      /**
       * Accessors.
       */
      const TEdge* operator->() const {return &edge;}
      const TEdge& operator*() const { return edge;}

    }; // End of class: Coloring::Tracer

#endif // #ifdef TRIANGULATION_EDGE_INDEX

  public:

    /**
     * A vertex joins interior and/or boundary edges.
     */
    class Vertex : public Geometry::Point {

      friend class InteriorEdge;
      friend class BoundaryEdge;
      friend class Coloring;
    
    public:

      /**
       * The type of the vertex (INTERIOR, BOUNDARY, or CORNER).
       */
      enum Type {
        INTERIOR, /*!< A polygon vertex that is inside 
                   *   the colored region.     */
        BOUNDARY, /*!< A polygon vertex on the boundary 
                   *   of the colored region. */
        CORNER,   /*!< A vertex of the boundary of the 
                   *   colored region.         */
        INVALID,  /*!< A vertex that is not currently *
                    in use.                  */
      };

    protected:

#ifdef TRIANGULATION_EDGE_INDEX
      /**
       * A handle on the vertex's representation in the triangulation.
       */
      TVertexHandle tvh;
#endif

      /**
       * The type of this vertex.  Note that a vertex of type
       * Coloring::Vertex::CORNER is not a true vertex of the coloring.
       */
      Type t;

      /**
       * A handle on the previous interior edge incident to this
       * vertex (for some ordering of the path's vertices).  If this
       * is a CORNER vertex, then this pointer is NULL.
       */
      IntEdgeHandle prevIntEdge;

      /**
       * A pointer to the next interior edge incident to this vertex
       * (for some ordering of the path's vertices).  If this is a
       * CORNER vertex, then this pointer is NULL.
       */
      IntEdgeHandle nextIntEdge;

      /**
       * If this vertex is of type BOUNDARY, then this points to the
       * previous boundary edge incident to this vertex (for some
       * ordering of the boundary edges).  Otherwise, this is NULL.
       */
      BdEdgeHandle prevBdEdge;

      /**
       * If this vertex is of type BOUNDARY, then this points to the
       * next boundary edge incident to this vertex (for some ordering
       * of the boundary edges).  Otherwise, this is NULL.
       */
      BdEdgeHandle nextBdEdge;

      /**
       * The index of this vertex in the random access vectors.  If
       * this vertex is of type INTERIOR, then this its index in
       * #interiorVertices; if this vertex is of type BOUNDARY, then
       * this is its index in #boundaryVertices; and if this vertex is
       * of type CORNER, then this is its index in #cornerVertices.
       * If this vertex is unused (of type INVALID), then this is -1.
       */
      int index;

    public:

      /** 
       * Default constructor.
       */
      Vertex() {
        t = INVALID;
        index = -1;
      }
    
      /**
       * Returns the type of this vertex.
       *
       * @return the type of this vertex
       */
      inline Type type() const { return t; }

      /**
       * Returns a point representation of the vertex.
       */
      const Geometry::Point& point() const { return *this; }

      /**
       * Returns true if this vertex has a previous interior edge.  If
       * it does not, then it is a boundary vertex at the start of a
       * path and calling #getPrevIntEdge() will yield an error.
       */
      inline bool hasPrevIntEdge() const { 
        assert(type() != CORNER);
        return (prevIntEdge.valid()); 
      }

      /**
       * Returns a reference to the previous edge incident to this
       * vertex (for some ordering of the path's vertices).  This vertex
       * must not be of type CORNER, and it must have a previous
       * interior edge.
       */
      inline IntEdgeHandle getPrevIntEdge() { 
        assert(type() != CORNER);
        return prevIntEdge; 
      }
      inline const IntEdgeHandle getPrevIntEdge() const { 
        assert(type() != CORNER);
        return prevIntEdge; 
      }

      /**
       * Returns true if this vertex has a next interior edge.  If it
       * does not, then it is a boundary vertex at the end of a path and
       * calling #getNextIntEdge() will yield an error.
       */
      inline bool hasNextIntEdge() const {
        assert(type() != CORNER);
        return (nextIntEdge.valid()); 
      }

      /**
       * Returns a reference to the next interior edge incident to this
       * vertex (for some ordering of the path's vertices).  This vertex
       * must not be of type CORNER, and it must have a next interior
       * edge.
       */
      inline IntEdgeHandle getNextIntEdge() {
        assert(type() != CORNER);
        return nextIntEdge; 
      }
      inline const IntEdgeHandle getNextIntEdge() const {
        assert(type() != CORNER);
        return nextIntEdge; 
      }

      /**
       * Returns a reference to the previous boundary edge incident to
       * this vertex (for some ordering of the boundary vertices).  This
       * vertex must be of type BOUNDARY or CORNER.
       */
      inline BdEdgeHandle getPrevBdEdge() {
        assert((type() == CORNER) || (type() == BOUNDARY)); 
        return prevBdEdge; 
      }
      inline const BdEdgeHandle getPrevBdEdge() const {
        assert((type() == CORNER) || (type() == BOUNDARY)); 
        return prevBdEdge; 
      }

      /**
       * Returns a reference to the next boundary edge incident to
       * this vertex (for some ordering of the boundary vertices).
       * This vertex must be of type BOUNDARY or CORNER.
       */
      inline BdEdgeHandle getNextBdEdge() { 
        assert((type() == CORNER) || (type() == BOUNDARY)); 
        return nextBdEdge; 
      }
      inline const BdEdgeHandle getNextBdEdge() const { 
        assert((type() == CORNER) || (type() == BOUNDARY)); 
        return nextBdEdge; 
      }

      /**
       * Returns a reference to the vertex before this vertex in the
       * ordering of the polygon's vertices.
       */
      inline VertexHandle getPrevVertex() {
        return getPrevIntEdge()->getPrevVertex();
      }
      inline const VertexHandle getPrevVertex() const {
        return getPrevIntEdge()->getPrevVertex();
      }

      /**
       * Returns a reference to the vertex after this vertex in the
       * ordering of the polygon's vertices.
       */
      inline VertexHandle getNextVertex() {
        return getNextIntEdge()->getNextVertex();
      }
      inline const VertexHandle getNextVertex() const {
        return getNextIntEdge()->getNextVertex();
      }

      /**
       * Returns a reference to the previous corner vertex preceding
       * this boundary vertex.  This method signals an error if this
       * vertex is not a boundary vertex.
       */
      inline VertexHandle getPrevCornerVertex() {
        return getPrevBdEdge()->getPrevCornerVertex();
      }
      inline const VertexHandle getPrevCornerVertex() const {
        return getPrevBdEdge()->getPrevCornerVertex();
      }

      /**
       * Returns a reference to the next corner vertex following this
       * boundary vertex.  This method signals an error if this vertex
       * is not a boundary vertex.
       */
      inline VertexHandle getNextCornerVertex() {
        return getNextBdEdge()->getNextCornerVertex();
      }
      inline const VertexHandle getNextCornerVertex() const {
        return getNextBdEdge()->getNextCornerVertex();
      }

      /**
       * If this vertex is an interior vertex, this computes the log of
       * the sine of the smaller angle between the two incident edges;
       * if this vertex is a boundary vertex, this computes the log of
       * the sine of the smaller angle between the true incident edge
       * and the boundary tangent.  It is an error to invoke this method
       * on a vertex that is not of type BOUNDARY or INTERIOR.
       *
       * @return the log of the sine of the angle associated with the
       *         vertex
       */
      double logSine();

    }; // End of class: Coloring::Vertex

    /**
     * An edge joins two vertices.  If the edge does not lie on the
     * boundary of the colored region, then it is "interior", and its
     * associated line segment represents a color discontinuity; if it
     * lies on the boundary, then it does not.  Two interior edges may
     * not "cross", in that if they intersect, they must intersect at a
     * point that is a vertex of both segments.
     */
    class Edge : public Geometry::Segment {

      friend class Vertex;
      friend class Coloring;

    protected:

      /**
       * A pointer to the previous vertex incident to this edge (for
       * some ordering of the path's vertices).
       */
      VertexHandle prevVertex;

      /**
       * A pointer to the next vertex incident to this edge (for some
       * ordering of the path's vertices).
       */
      VertexHandle nextVertex;

      /**
       * The index of this edge in the random access vectors.  If this
       * edge is of type INTERIOR, then this its index in
       * #interiorEdges; if this edge is of type BOUNDARY, then this is
       * its index in #boundaryEdges.  If this vertex is of type INVALID
       * then this is -1.
       */
      int index;

    public:

      /** 
       * Default constructor.
       */
      Edge() {
        index = -1;
      }

      /**
       * Returns a segment representation of the edge.
       */
      const Geometry::Segment& segment() const { return *this; }

      /**
       * Returns a reference to the previous vertex incident to this
       * edge (for some ordering of the path's vertices).
       */
      inline VertexHandle getPrevVertex() { return prevVertex; }
      inline const VertexHandle getPrevVertex() const { return prevVertex; }

      /**
       * Returns a reference to the next vertex incident to this
       * edge (for some ordering of the path's vertices).
       */
      inline VertexHandle getNextVertex() { return nextVertex; }
      inline const VertexHandle getNextVertex() const { return nextVertex; }

      /**
       * Returns a point sampled uniformly at random from the edge.  If
       * exact geometric constructions are not supported, then this
       * point may not lie exactly on this segment.  Note that for short
       * edges, it is possible that one of the endpoints is returned.
       *
       * @param random a source of pseudorandomness
       * @return a random point in the closure of the edge
       */
      Geometry::Point 
      randomPoint(Arak::Util::Random& random = Arak::Util::default_random) const;

      /**
       * Returns the length of this edge.
       *
       * @return the length of this edge
       */
      double length() const { 
        using namespace Arak::Geometry;
        return sqrt(CGAL::to_double(squared_length())); 
      }

      /**
       * Two edges are equal if they are identically the same object.
       */
      bool operator==(const Edge& other) const {
        return (this == &other);
      }

      /**
       * Two edges are unequal if they are not identically the same
       * object.
       */
      bool operator!=(const Edge& other) const {
        return !this->operator!=(other);
      }

    }; // End of class: Coloring::Edge

    /**
     * A grid where each cell stores pointers to all interior edges
     * whose segments intersect the cell's boundary.
     */
    typedef Grid<IntEdgeHandle> InteriorEdgeIndex;

    /**
     * An edge whose interior is inside the colored region; its
     * associated line segment represents a color discontinuity.  Two
     * interior edges may not "cross", in that if they intersect, they
     * must intersect at a point that is a vertex of both segments.
     */
    class InteriorEdge : public Edge {

      friend class Vertex;
      friend class Coloring;

    public:

      /**
       * An entry of an interior edge in the interior edge index.
       */
      typedef InteriorEdgeIndex::Cell::Entry CellEntry;

      /**
       * A list of cell entries.
       */
      typedef std::list<CellEntry> CellEntryList;

    protected:

      /**
       * A list of cell entries in which this edge is stored.
       */
      CellEntryList entries;

      /**
       * A list of unused cell entries; this is used to minimize the
       * overhead of memory allocation.
       */
      CellEntryList unused;

    public:

      /** 
       * Default constructor.
       */
      InteriorEdge() : Edge(), entries() { }

      /**
       * Default destructor.
       */
      ~InteriorEdge() { }

      /**
       * Returns true if this edge crosses the segment with the supplied
       * endpoints.  Two segments cross iff they intersect at a point
       * that is not a shared endpoint.
       *
       * @param a the source of the segment
       * @param b the target of the segment
       * @return  true if this edge crosses the segment from a to b
       */
      bool crosses(const Geometry::Point& a, 
                   const Geometry::Point& b) const;

      /**
       * Returns a const reference to a list of this edge's entries in
       * the interior edge index.
       */
      const CellEntryList& cellEntries() const { return entries; }

    }; // End of class: Coloring::InteriorEdge

    /**
     * An edge that lies in the boundary of the colored region; such
     * edges do not represent color discontinuities.
     */
    class BoundaryEdge : public Edge {

      friend class Vertex;
      friend class Coloring;

    protected:

      /**
       * The first corner vertex encountered by a backwards traversal
       * from this boundary edge.  This is cached for efficiency.
       */
      VertexHandle prevCorner;

      /**
       * The first corner vertex encountered by a forwards traversal
       * from this boundary edge.  This is cached for efficiency.
       */
      VertexHandle nextCorner;

    public:

      /** 
       * Default constructor.
       */
      BoundaryEdge() : Edge(), prevCorner(NULL), nextCorner(NULL) { }

      /**
       * Returns a reference to the previous corner vertex preceding
       * this boundary edge.
       */
      inline VertexHandle getPrevCornerVertex() { return prevCorner; }
      inline const VertexHandle getPrevCornerVertex() const { return prevCorner; }
      /**
       * Returns a reference to the next corner vertex following this
       * boundary edge.
       */
      inline VertexHandle getNextCornerVertex() { return nextCorner; }
      inline const VertexHandle getNextCornerVertex() const { return nextCorner; }

      /**
       * Returns a point sampled uniformly at random from the edge.  If
       * exact geometric constructions are not supported, then this
       * point may not lie exactly on this segment.  Note that for short
       * edges, it is possible that one of the endpoints is returned.
       *
       * @param random a source of pseudorandomness
       * @return a random point in the closure of the edge
       */
      Geometry::Point 
      randomPoint(Arak::Util::Random& random = Arak::Util::default_random) const;

    }; // End of class: Coloring::BoundaryEdge

  public:

    /**
     * This interface is implemented by objects that must be notified
     * when the coloring is updated.  When an update is requested,
     * notifications occur in the following order: 
     *
     *   1. Listeners are first notified of edges that will be 
     *      removed as a result of the update.
     *   2. Listeners are then notified of vertices that will be 
     *      removed as a result of the update.
     *   3. The update is performed.
     *   4. Listeners are next notified of vertices that have been
     *      created by the update.
     *   5. Listeners are next notified of edges that have been
     *      created by the update.
     *   6. Listeners are finally notified of the region that has been
     *      recolored (either a triangle or a quadrilateral).
     *
     * This ordering ensures that the listeners are notified while the
     * coloring is in a consistent state (and so its methods can be 
     * safely invoked) and that they can refer to the specific 
     * structural elements that are changed by updates.
     */
    class Listener {

    public:

      /**
       * This method is invoked to inform the listener that a new
       * vertex has been added to the coloring.
       *
       * @param vh a handle to the new vertex
       */
      virtual void vertexHasBeenAdded(Coloring::VertexHandle vh) {};

      /**
       * This method is invoked to inform the listener that an
       * existing vertex is about to be removed from the coloring.
       *
       * @param vh a handle to the vertex that will be removed
       */
      virtual void vertexWillBeRemoved(Coloring::VertexHandle vh) {};

      /**
       * This method is invoked to inform the listener that a new
       * (interior) edge has been added to the coloring.
       *
       * @param eh a handle to the new edge
       */
      virtual void edgeHasBeenAdded(Coloring::IntEdgeHandle eh) {};

      /**
       * This method is invoked to inform the listener that an
       * existing (interior) edge will be removed from the coloring.
       *
       * @param eh a handle to the edge that will be removed
       */
      virtual void edgeWillBeRemoved(Coloring::IntEdgeHandle eh) {};

      /**
       * This method is invoked to inform the listener that the
       * triangle with the supplied vertices has been recolored.
       *
       * @param a the first vertex of the triangle
       * @param b the second vertex of the triangle
       * @param c the third vertex of the triangle
       */
      virtual void recolored(const Geometry::Point& a,
                             const Geometry::Point& b,
                             const Geometry::Point& c) {};
      
      /**
       * This method is invoked to inform the listener that the
       * quadrilateral with the supplied vertices has been recolored.
       * The vertices are supplied in either clockwise or
       * counter-clockwise order.  Note that this quadrilateral is
       * simple, but not necessarily convex.
       *
       * @param a the first vertex of the quadrilateral
       * @param b the second vertex of the quadrilateral
       * @param c the third vertex of the quadrilateral
       * @param d the fourth vertex of the quadrilateral
       */
      virtual void recolored(const Geometry::Point& a,
                             const Geometry::Point& b,
                             const Geometry::Point& c,
                             const Geometry::Point& d) {};

    }; // End of class: Coloring::Listener

  protected:

    /**
     * An axis-aligned rectangle representing the boundary of the
     * colored region.  (This could be generalized to an arbitrary
     * convex region of the plane, but uniform sampling would be more
     * expensive.  A triangulation could be used to sample more
     * complex regions efficiently.)
     */
    Geometry::Rectangle bd;

    /**
     * A grid of cells that record all crossing interior edges.
     */
    InteriorEdgeIndex* intEdgeIndex;

#ifdef TRIANGULATION_EDGE_INDEX
    /**
     * A constrained Delaunay triangulation of the coloring's
     * vertices.  The interior and boundary edges are constraints.
     * This data structure is used to implement segment/ray tracing
     * operations efficiently.
     */
    TriangulationIndex tri;
#endif
    
    /**
     * An index of points in the interior of the coloring whose colors
     * are maintained under updates.
     */
    mutable QueryPointIndex* queryPoints;

    /**
     * A store of (potential) vertices to be used in the coloring.
     */
    Store<Vertex> vertexStore;
  
    /**
     * A vector of pointers to vertices in the interior of the region.
     * This array permits random access to the interior vertices.
     */
    std::vector<VertexHandle> interiorVertices;
  
    /**
     * An vector of pointers to vertices on the boundary of the region.
     * This array permits random access to the boundary vertices.
     */
    std::vector<VertexHandle> boundaryVertices;
  
    /**
     * A vector of pointers to vertices at the corner of the region.
     * This array permits random access to the corner vertices.
     */
    std::vector<VertexHandle> cornerVertices;
  
    /**
     * A store of (potential) interior edges to be used in the
     * coloring.
     */
    Store<InteriorEdge> intEdgeStore;
  
    /**
     * A vector of pointers to edges in the interior of the region.  An
     * edge is in the interior unless both of its vertices are not in
     * the interior. This array permits random access to the interior
     * edges.
     */
    std::vector<IntEdgeHandle> interiorEdges;
  
    /**
     * A store of (potential) boundary edges to be used in the
     * coloring.
     */
    Store<BoundaryEdge> bdEdgeStore;
  
    /**
     * An vector of pointers to edges on the boundary of the colored
     * region.  This array permits random access to these edges.
     */
    std::vector<BdEdgeHandle> boundaryEdges;

    /**
     * A list of the objects that are listening to this coloring.
     */
    mutable std::list<Listener*> listeners;

    /**
     * This value is a constraint on the (squared) minimum edge
     * length.  It can be set to zero to allow all edge lengths.
     */
    double minEdgeLengthSq;

    /**
     * Returns a reference to a fresh vertex.
     *
     * @param  type the type of the new vertex
     * @param  p    the location of the vertex
     * @return      a reference to a new Vertex object 
     */
    VertexHandle newVertex(Vertex::Type type, const Geometry::Point& p);

    /**
     * Frees a vertex for future use.
     *
     * @param v a reference to the Vertex to be freed
     */
    void freeVertex(VertexHandle& v);
  
    /**
     * Disconnects a boundary vertex from its incident boundary edges.
     * These boundary edges are replaced by a single boundary edge.
     *
     * @param v a handle to the boundary vertex to be disconnected
     */
    void disconnectFromBd(VertexHandle v);
  
    /**
     * Returns a reference to a fresh interior edge that leads from
     * prev to next.  prev and next are both modified so that the new
     * edge joins them.
     *
     * @param prev the previous vertex 
     * @param next the next vertex 
     * @return     the fresh interior edge
     */
    IntEdgeHandle newInteriorEdge(VertexHandle prev, VertexHandle next);

    /**
     * Returns a reference to a fresh boundary edge that leads from
     * prev to next.  prev and next are both modified so that the new
     * edge joins them.
     *
     * @param prev       the previous boundary vertex 
     * @param next       the next boundary vertex 
     * @param prevCorner the previous corner vertex
     * @param nextCorner the next corner vertex
     * @return     the fresh boundary edge
     */
    BdEdgeHandle newBoundaryEdge(VertexHandle prev, 
                                 VertexHandle next,
                                 VertexHandle prevCorner, 
                                 VertexHandle nextCorner);

    /**
     * Frees an interior edge for future use.
     *
     * @param e a reference to the edge to be freed
     */
    void freeEdge(IntEdgeHandle& e);

    /**
     * Frees a boundary edge for future use.
     *
     * @param e a reference to the edge to be freed
     */
    void freeEdge(BdEdgeHandle& e);

    /**
     * This helper method performs all necessary updates (including
     * informing listeners) when the triangle with the supplied
     * vertices has been recolored.
     *
     * @param a the first vertex of the triangle
     * @param b the second vertex of the triangle
     * @param c the third vertex of the triangle
     */
    void recolored(const Geometry::Point& a,
                   const Geometry::Point& b,
                   const Geometry::Point& c);
    
    /**
     * This helper method performs all necessary updates (including
     * informing listeners) when the quadrilateral with the supplied
     * vertices has been recolored.  The vertices are supplied in
     * either clockwise or counter-clockwise order.  Note that this
     * quadrilateral is simple, but not necessarily convex.
     *
     * @param a the first vertex of the quadrilateral
     * @param b the second vertex of the quadrilateral
     * @param c the third vertex of the quadrilateral
     * @param d the fourth vertex of the quadrilateral
     */
    void recolored(const Geometry::Point& a,
                   const Geometry::Point& b,
                   const Geometry::Point& c,
                   const Geometry::Point& d);

    /**
     * Notifies all listeners that the vertex with the supplied handle
     * will be removed from the coloring.
     */
    inline void notifyVertexWillBeRemoved(VertexHandle vh) {
      for (std::list<Listener*>::iterator it = listeners.begin(); 
           it != listeners.end(); it++) 
        (*it)->vertexWillBeRemoved(vh);
    }
    
    /**
     * Notifies all listeners that the vertex with the supplied handle
     * has been added to the coloring.
     */
    inline void notifyVertexHasBeenAdded(VertexHandle vh) {
      for (std::list<Listener*>::iterator it = listeners.begin(); 
           it != listeners.end(); it++) 
        (*it)->vertexHasBeenAdded(vh);
    }

    /**
     * Notifies all listeners that the edge with the supplied handle
     * will be removed from the coloring.
     */
    inline void notifyEdgeWillBeRemoved(IntEdgeHandle eh) {
      for (std::list<Listener*>::iterator it = listeners.begin(); 
           it != listeners.end(); it++) 
        (*it)->edgeWillBeRemoved(eh);
    }
    
    /**
     * Notifies all listeners that the edge with the supplied handle
     * has been added to the coloring.
     */
    inline void notifyEdgeHasBeenAdded(IntEdgeHandle eh) {
      for (std::list<Listener*>::iterator it = listeners.begin(); 
           it != listeners.end(); it++) 
        (*it)->edgeHasBeenAdded(eh);
    }

    /**
     * Initializes this coloring.  The caller supplies the boundary of
     * the colored region and the dimensions of a grid index over the
     * edges in the coloring; this is used to check for crossing edges
     * and to compute the coloring of an arbitrary point in the
     * region.
     *
     * @param boundary   an axis-aligned rectangle representing the
     *                   boundary of the colored region.
     * @param gridRows   the number of rows in the cell grid
     * @param gridCols   the number of columns in the cell grid
     * @param minEdgeLength (optional) a constraint on edge lengths
     */
    void initialize(Geometry::Rectangle &boundary, 
                    int gridRows = 10,
                    int gridCols = 10,
                    double minEdgeLength = 0.0);

    /**
     * Deallocates all internal structures in this coloring.
     */
    void clear();

  public:
  
    /**
     * Default constructor.
     */
    Coloring();

    /**
     * Constructor.  The caller supplies the boundary of the colored
     * region.  The caller can optionally specify the dimensions of a
     * grid index over the edges in the coloring; this is used to
     * check for crossing edges and to compute the coloring of an
     * arbitrary point in the region.
     *
     * @param boundary   an axis-aligned rectangle representing the
     *                   boundary of the colored region.
     * @param gridRows   the number of rows in the cell grid
     * @param gridCols   the number of columns in the cell grid
     * @param minEdgeLength a constraint on the minimum edge length
     */
    Coloring(Geometry::Rectangle &boundary, 
             int gridRows = 10,
             int gridCols = 10,
             double minEdgeLength = 0.0);
  
    /**
     * Constructor.  The caller supplies the boundary of the colored
     * region.  The caller can optionally specify the dimensions of a
     * grid index over the edges in the coloring; this is used to
     * check for crossing edges and to compute the coloring of an
     * arbitrary point in the region.
     *
     * @param props a property map
     */
    Coloring(const Util::PropertyMap& props);

    /**
     * Destructor.
     */
    ~Coloring();

    /**
     * Tests the integrity of this Coloring object.  An error is raised
     * if this test fails.
     */
    void test() const;

    /**
     * Returns the boundary of this coloring.
     *
     * @return the boundary of this coloring
     */
    const Geometry::Rectangle& boundary() const { return bd; }

    /**
     * Returns a const reference to the interior edge index.
     */
    const InteriorEdgeIndex& getInteriorEdgeIndex() const {
      return *intEdgeIndex;
    }

    /**
     * Returns the number of vertices of the supplied type.
     *
     * @param type a type of vertex
     * @return the number of vertices of the supplied type
     */
    inline int numVertices(Vertex::Type type) const {
      switch (type) {
      case Vertex::INTERIOR:
        return interiorVertices.size();
      case Vertex::BOUNDARY:
        return boundaryVertices.size();
      case Vertex::CORNER:
        return cornerVertices.size();
      default:
        assert(false);
      }
    }

    /**
     * Returns a const reference to the vertex with the supplied type
     * and index.
     *
     * @param  type   a type of vertex
     * @param  index  an index between 0 and numVertices(type) - 1
     * @return        a reference to the vertex
     */
    inline const VertexHandle getVertex(Vertex::Type type, int index) const {
      switch (type) {
      case Vertex::INTERIOR:
        return interiorVertices[index];
      case Vertex::BOUNDARY:
        return boundaryVertices[index];
      case Vertex::CORNER:
        return cornerVertices[index];
      default:
        assert(false);
      }
    }

    /**
     * Returns a mutable reference to the vertex with the supplied type
     * and index.
     *
     * @param  type   a type of vertex
     * @param  index  an index between 0 and numVertices(type) - 1
     * @return        a mutable reference to the vertex
     */
    inline VertexHandle getVertex(Vertex::Type type, int index) {
      switch (type) {
      case Vertex::INTERIOR:
        return interiorVertices[index];
      case Vertex::BOUNDARY:
        return boundaryVertices[index];
      case Vertex::CORNER:
        return cornerVertices[index];
      default:
        assert(false);
      }
    }

    /**
     * Returns the number of interior edges in the coloring.
     *
     * @return the number of interior edges
     */
    inline int numInteriorEdges() const {
      return interiorEdges.size();
    }

    /**
     * Returns the number of boundary edges in the coloring.
     *
     * @return the number of boundary edges
     */
    inline int numBoundaryEdges() const {
      return boundaryEdges.size();
    }

    /**
     * Returns a const reference to the interior edge with the
     * supplied index.
     *
     * @param  index  an index between 0 and #numInteriorEdges() - 1
     * @return        a const reference to the edge
     */
    inline const IntEdgeHandle getInteriorEdge(int index) const {
      return interiorEdges[index];
    }
    inline IntEdgeHandle getInteriorEdge(int index) {
      return interiorEdges[index];
    }

    /**
     * Returns a const reference to the boundary edge with the
     * supplied index.
     *
     * @param  index  an index between 0 and #numBoundaryEdges() - 1
     * @return        a const reference to the edge
     */
    inline const BdEdgeHandle getBoundaryEdge(int index) const {
      return boundaryEdges[index];
    }
    inline BdEdgeHandle getBoundaryEdge(int index) {
      return boundaryEdges[index];
    }

    /**
     * A tag that is used to dispatch a method that checks whether an
     * update would leave the coloring in a valid state.
     */
    struct TEST_IF_VALID_TAG {};

    /**
     * A tag that is used to dispatch a method that first checks
     * whether an update would leave the coloring in a valid state,
     * and if it would, the update is executed.
     */
    struct DO_IF_VALID_TAG {};

    /**
     * A tag that is used to dispatch a method that executes an update
     * without first checking to see if it would leave the coloring in
     * a valid state.  Methods of this sort are provided for
     * efficiency's sake, but they are very unsafe; they can leave the
     * coloring in an inconsistent state that would make further
     * operations impossible.  Before invoking an update with this
     * tag, first invoke the update with the #TEST_IF_VALID_TAG tag to
     * ensure this will not happen.
     */
    struct DO_WITHOUT_TEST_TAG {};

    /**
     * \defgroup ColoringUpdates Methods that update the coloring
     *
     * These methods are the external interface for updating a
     * coloring object.  Each method has three versions: one which
     * tests to see if the update would leave the coloring in a valid
     * state; a second version which executes the update only if it
     * would leave the coloring in a valid state; and a third version
     * that tries to execute the update without first testing to see
     * if the result would be valid.  These three versions are
     * dispatched with the tags #TEST_IF_VALID_TAG, #DO_IF_VALID_TAG,
     * and #DO_WITHOUT_VALID_TAG.
     */

    //@{
    /**
     * Removes a vertex by joining its two adjacent vertices.  This
     * operation is supported only when the vertex is part of a path
     * with four or more vertices, and when the new edge would not cross
     * any other edges in the coloring.  If either of these conditions
     * does not hold, then this method leaves the coloring unchanged and
     * returns false.
     *
     * \ingroup ColoringUpdates
     * @param v the vertex to be deleted; this vertex object becomes
     *          invalid if the operation is successful
     * @return  true if the deletion was executed, and false otherwise
     */
    bool deleteVertex(VertexHandle v, TEST_IF_VALID_TAG) const;
    bool deleteVertex(VertexHandle& v, DO_IF_VALID_TAG);
    bool deleteVertex(VertexHandle& v, DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Splits an interior edge into two that are joined by a vertex at
     * the supplied point.  This operation is supported only when the
     * new edges would not cross any other edges in the coloring.  If
     * this is not true then this method leaves the coloring unchanged
     * and returns false.
     *
     * \ingroup ColoringUpdates
     * @param e the edge to be split; this edge object becomes invalid 
     *          if the operation is successful
     * @param p the location of the new vertex
     * @return  true if the split was executed, and false otherwise
     */
    bool splitEdge(IntEdgeHandle e, 
                   const Geometry::Point& p, 
                   TEST_IF_VALID_TAG) const;
    bool splitEdge(IntEdgeHandle& e, 
                   const Geometry::Point& p, 
                   DO_IF_VALID_TAG);
    bool splitEdge(IntEdgeHandle& e, 
                   const Geometry::Point& p, 
                   DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Constructs a fresh triangular path in the interior of the
     * colored region.  All points inside the triangle are recolored.
     * This method does nothing if the edges of the triangle would
     * cross existing edges in the coloring.
     *
     * \ingroup ColoringUpdates
     * @param x the first point of the triangle
     * @param y the second point of the triangle
     * @param z the third point of the triangle
     * @return a handle on a vertex of the new triangle, or an 
     *         invalid vertex handle if the triangle was not created
     */
    bool newInteriorTriangle(const Geometry::Point& x, 
                             const Geometry::Point& y, 
                             const Geometry::Point& z, 
                             TEST_IF_VALID_TAG) const;
    VertexHandle newInteriorTriangle(const Geometry::Point& x, 
                                     const Geometry::Point& y, 
                                     const Geometry::Point& z, 
                                     DO_IF_VALID_TAG);
    VertexHandle newInteriorTriangle(const Geometry::Point& x, 
                                     const Geometry::Point& y, 
                                     const Geometry::Point& z, 
                                     DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Constructs a fresh triangular path along the boundary of the
     * colored region.  All points inside the triangle are recolored.
     * This method does nothing if the edges of the triangle would
     * cross existing edges in the coloring.
     *
     * \ingroup ColoringUpdates
     * @param e a boundary edge
     * @param u a point on e
     * @param v another point on f (distinct from u)
     * @param w a point in the interior of the colored region
     * @return a handle to the root vertex of the new boundary 
     *         triangle, or an invalid vertex handle if the triangle
     *         was not created
     */
    bool newBoundaryTriangle(BdEdgeHandle e, 
                             const Geometry::Point& u, 
                             const Geometry::Point& v, 
                             const Geometry::Point& w,
                             TEST_IF_VALID_TAG) const;
    VertexHandle newBoundaryTriangle(BdEdgeHandle e, 
                                     const Geometry::Point& u, 
                                     const Geometry::Point& v, 
                                     const Geometry::Point& w,
                                     DO_IF_VALID_TAG);
    VertexHandle newBoundaryTriangle(BdEdgeHandle e, 
                                     const Geometry::Point& u, 
                                     const Geometry::Point& v, 
                                     const Geometry::Point& w,
                                     DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Recolors a triangle in the corner of the colored region by
     * constructing single interior edge that joins boundary vertices on
     * adjacent faces of the boundary.  If the interior edge of the
     * triangle would cross existing edges in the coloring, then 
     * this method does nothing.
     *
     * \ingroup ColoringUpdates
     * @param corner the corner that is one vertex of the triangle
     * @param u      the second point of the triangle; this must lie on 
     *               the previous boundary edge incident to the corner
     * @param v      the third point of the triangle; this must lie on 
     *               the next boundary edge incident to the corner
     * @return a handle to the root vertex of the new boundary 
     *         triangle, or an invalid vertex handle if the triangle
     *         was not created
     */
    bool newCornerTriangle(VertexHandle corner, 
                           const Geometry::Point& u, 
                           const Geometry::Point& v, 
                           TEST_IF_VALID_TAG) const;
    VertexHandle newCornerTriangle(VertexHandle corner, 
                                   const Geometry::Point& u, 
                                   const Geometry::Point& v, 
                                   DO_IF_VALID_TAG);
    VertexHandle newCornerTriangle(VertexHandle corner, 
                                   const Geometry::Point& u, 
                                   const Geometry::Point& v, 
                                   DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Removes an interior triangle from the coloring.  All points in
     * the triangle are recolored.
     *
     * \ingroup ColoringUpdates
     * @param vh a handle to a vertex of the triangle; this is 
     *           updated to be invalid
     * @return true if the deletion was executed
     */
    bool deleteIntTriangle(VertexHandle vh, TEST_IF_VALID_TAG) const;
    bool deleteIntTriangle(VertexHandle& vh, DO_IF_VALID_TAG);
    bool deleteIntTriangle(VertexHandle& vh, DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Removes a boundary triangle from the coloring.  All points in
     * the triangle are recolored.
     *
     * \ingroup ColoringUpdates
     * @param vh a handle to a vertex of the triangle; this is 
     *           updated to be invalid
     * @return true if the deletion was executed
     */
    bool deleteBdTriangle(VertexHandle vh, TEST_IF_VALID_TAG) const;
    bool deleteBdTriangle(VertexHandle& vh, DO_IF_VALID_TAG);
    bool deleteBdTriangle(VertexHandle& vh, DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Removes a corner triangle from the coloring.  The supplied
     * vertex handle must point to a vertex of a triangle in a corner
     * of the window.  All points in the triangle are recolored.
     *
     * \ingroup ColoringUpdates
     * @param vh a handle to a vertex of the triangle; this is 
     *           updated to be invalid
     * @return true if the deletion was executed
     */
    bool deleteCornerTriangle(VertexHandle vh, TEST_IF_VALID_TAG) const;
    bool deleteCornerTriangle(VertexHandle& vh, DO_IF_VALID_TAG);
    bool deleteCornerTriangle(VertexHandle& vh, DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Moves an interior vertex to the supplied location.  This
     * operation is supported only when the movement would not cause the
     * vertex's edges to cross any other edges in the coloring.  If this
     * condition does not hold, then this method leaves the coloring
     * unchanged and returns false.
     *
     * \ingroup ColoringUpdates
     * @param v the interior vertex to be moved
     * @param p the new location for the vertex
     * @return  true if the movement was executed, and false otherwise
     */
    bool moveIntVertex(VertexHandle v, 
                       const Geometry::Point& p, 
                       TEST_IF_VALID_TAG) const;
    bool moveIntVertex(VertexHandle& v, 
                       const Geometry::Point& p, 
                       DO_IF_VALID_TAG);
    bool moveIntVertex(VertexHandle& v, 
                       const Geometry::Point& p, 
                       DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Moves a boundary vertex to the supplied location on the same face
     * of the boundary.  This operation is supported only when the
     * movement would not cause the vertex's edges to cross any other
     * edges in the coloring.  If this condition does not hold, then
     * this method leaves the coloring unchanged and returns false.
     *
     * \ingroup ColoringUpdates
     * @param v the boundary vertex to be moved
     * @param p the new location for the vertex on the same boundary face
     * @return  true if the movement was executed, and false otherwise
     */
    bool moveVertexAlongBd(VertexHandle v, 
                           const Geometry::Point& p, 
                           TEST_IF_VALID_TAG) const;
    bool moveVertexAlongBd(VertexHandle& v, 
                           const Geometry::Point& p, 
                           DO_IF_VALID_TAG);
    bool moveVertexAlongBd(VertexHandle& v, 
                           const Geometry::Point& p, 
                           DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Moves a boundary vertex past a corner vertex to an adjacent face
     * of the boundary.  This operation is supported only when the
     * movement update yields a valid coloring.  If it would cause
     * crossing edges or if the boundary vertex is adjacent to a
     * boundary vertex on the adjacent face, then this method leaves
     * the coloring unchanged and returns false.
     *
     * \ingroup ColoringUpdates
     * @param e an edge joining a corner vertex to a boundary vertex
     * @param p the new location for the vertex on the other boundary face
     *          adjacent to the corner 
     * @return  true if the movement was executed, and false otherwise
     */
    bool moveBdVertexPastCorner(BdEdgeHandle e, 
                                const Geometry::Point& p,
                                TEST_IF_VALID_TAG) const;
    bool moveBdVertexPastCorner(BdEdgeHandle& e, 
                                const Geometry::Point& p,
                                DO_IF_VALID_TAG);
    bool moveBdVertexPastCorner(BdEdgeHandle& e, 
                                const Geometry::Point& p,
                                DO_WITHOUT_TEST_TAG);
    //@}

    //@{
    /**
     * Recolors a quadrilateral region of the colorings.  The two
     * supplied interior edges \f$a \rightarrow b\f$ and \f$c
     * \rightarrow d\f$ are exchanged for two new edges: \f$\{a,
     * c\}\f$ and \f$\{b, d\}\f$ (if ac is true) or \f$\{a, d\}\f$ and
     * \f$\{b, c\}\f$ (if ac is false).  The supplied edge objects are
     * updated in place to represent these new edges.  If this
     * operation would not yield a valid coloring, then this method
     * leaves the coloring unchanged and returns false.
     *
     * \ingroup ColoringUpdates
     * @param  ab the \f$a \rightarrow b\f$ edge
     * @param  cd the \f$c \rightarrow d\f$ edge
     * @param  ac if this flag is true, the current edges are exchanged 
     *            for \f$\{a, c\}\f$ and \f$\{b, d\}\f$; otherwise they
     *            are exchanged for \f$\{a, d\}\f$ and \f$\{b, c\}\f$.
     * @return true if the recoloring was executed, and false otherwise
     */
    bool recolorQuadrilateral(IntEdgeHandle ab, 
                              IntEdgeHandle cd, 
                              bool ac, 
                              TEST_IF_VALID_TAG) const;
    bool recolorQuadrilateral(IntEdgeHandle& ab, 
                              IntEdgeHandle& cd, 
                              bool ac, 
                              DO_IF_VALID_TAG);
    bool recolorQuadrilateral(IntEdgeHandle& ab, 
                              IntEdgeHandle& cd, 
                              bool ac, 
                              DO_WITHOUT_TEST_TAG);
    //@}
  
#ifdef GRID_EDGE_INDEX
    /**
     * Checks to see if the line segment from x to y crosses any edges
     * that are currently in the coloring.  Two (closed) line segments
     * cross iff their intersection is a single point.  This method
     * requires that both x and y are in the interior.
     *
     * @param xp the origin of the line segment 
     * @param yp the terminus of the line segment
     * @return   true if a line segment from x to y crosses an edge in 
     *           the coloring
     */
    bool compatible(const Geometry::Point &xp, 
                    const Geometry::Point &yp) const;
#endif // #ifdef GRID_EDGE_INDEX

#ifdef TRIANGULATION_EDGE_INDEX
    /**
     * Checks to see if the line segment from x to y crosses any edges
     * that are currently in the coloring.  Two (closed) line segments
     * cross iff their intersection is a single point.  This method
     * requires that both x and y are in the interior.
     *
     * @param xp the origin of the line segment 
     * @param yp the terminus of the line segment
     * @param fh (optional) a handle to a face of the triangulation
     *           that is near xp; supplying a close face handle can
     *           significantly improve the speed of this test (if 
     *           a triangulation index is used)
     * @return   true if a line segment from x to y crosses an edge in 
     *           the coloring
     */
    bool compatible(const Geometry::Point &xp, 
                    const Geometry::Point &yp,
                    TFaceHandle fh = TFaceHandle()) const;
#endif // #ifdef TRIANGULATION_EDGE_INDEX

    /**
     * Traces the segment from xp to yp and finds the first interior
     * edge that intersects the segment (if any).
     *
     * @param  xp   a point in the interior of the coloring
     * @param  yp   a point distinct from xp
     * @param  edge a handle an interior edge that is updated
     *              to the first interior edge intersecting
     *              the segment (if any)
     * @param  ipt  the intersection point (if any)
     * @param  sd   updated to the squared distance between xp and 
     *              the intersection point (if any)
     * @return      true if there is an intersecting edge
     */
    bool trace(const Geometry::Point &xp, 
               const Geometry::Point &yp,
               IntEdgeHandle& edge,
               Geometry::Point &ipt,
               Geometry::Kernel::FT& sd) const;

    /**
     * Returns true iff xp is in the interior of the colored region.
     *
     * @param xp a point
     * @return   true iff xp is in the interior of the colored region
     */
    bool interior(const Geometry::Point& xp) const {
      return bd.has_on_bounded_side(xp);
    }

    /**
     * Returns true iff xp lies on the boundary of the colored region.
     *
     * @param xp a point
     * @return   true iff xp lies on the boundary of the colored region
     */
    bool boundary(const Geometry::Point& xp) const {
      return bd.has_on_boundary(xp);
    }

    /**
     * Returns true iff xp lies outside the closure of the colored region.
     *
     * @param xp a point
     * @return   true iff xp lies outside the closure of the colored region
     */
    bool outside(const Geometry::Point& xp) const {
      return bd.has_on_unbounded_side(xp);
    }

    /**
     * Returns a point sampled uniformly at random from the interior of
     * the colored region.
     *
     * @param random a source of pseudorandomness
     * @return a random point in the region
     */
    Geometry::Point 
    randomPoint(Arak::Util::Random& random = Arak::Util::default_random) const;

    /**
     * Returns the area of the colored region.
   
     * @return the area of the colored region
     */
    double area() const { 
      using namespace Arak::Geometry;
      return CGAL::to_double(bd.area()); 
    }
    
    /**
     * Returns a reference to a vertex with the supplied type selected
     * uniformly at random.
     *
     * @param  type   a type of vertex
     * @param  random a source of pseudorandomness
     * @return a reference to the vertex
     */
    inline VertexHandle randomVertex(Vertex::Type type, 
                                     Arak::Util::Random& random = Arak::Util::default_random) const {
      assert(numVertices(type) > 0);
      return getVertex(type, random.uniform(numVertices(type)));
    }

    /**
     * Computes a point in the interior of the coloring and its color.
     * From this colored point the colors of all other points can be
     * deduced from the discontinuity set.
     */
    void getPointWithColor(Geometry::Point& p, Color& c) const;

    /**
     * Returns the color of the supplied point.  This operation is
     * expensive (requiring ray tracing); for a more efficient
     * computation, the point should be registered as a query point. 
     *
     * @param point a point in the interior or on the boundary of 
     *              the colored region
     * @return      true if the point is colored black, and false 
     *              if it is white
     */
    Color color(const Geometry::Point& point) const;
    
    /**
     * Returns true iff every point in the closure of the supplied
     * convex polygon is the supplied color.
     *
     * @param poly a convex polygon
     * @param color the text color
     * @return true iff every point in poly is the supplied color 
     */
    bool solid(const Geometry::Polygon& poly, Arak::Color color) const;

    /**
     * Adds a new listener to this coloring's list of listeners.
     *
     * @param listener the new listener
     */
    void addListener(Listener& listener) const;

    /**
     * Adds all query points in the supplied index to the internal
     * query point index.
     *
     * @param index an index of query points
     */
    void addQueryPoints(const QueryPointIndex& index) const;

    /**
     * Returns a const reference to the query point index.
     */
    const QueryPointIndex& getQueryPoints() const { return *queryPoints; }

    /**
     * Returns a reference to the query point index.
     */
    QueryPointIndex& getQueryPoints() { return *queryPoints; }

    /**
     * Writes a text representation of this coloring out to the
     * supplied stream.
     *
     * @param out the output stream to write the coloring on
     */
    void write(std::ostream& out) const;

    /**
     * Writes a binary representation of this coloring out to the
     * supplied stream.
     *
     * @param out the output stream to write the coloring on
     */
    void writeBinary(std::ostream& out) const;

    /**
     * Writes an Xfig visualization of this coloring out to the
     * supplied stream.
     *
     * @param out the output stream to write the visualization on
     */
    void writeXfig(std::ostream& out) const;

    /**
     * Initializes this coloring from a text representation read from
     * the supplied stream.
     *
     * @param in the input stream to read the coloring from
     * @return true if the read was successful, false otherwise
     */
    bool read(std::istream& in);

    /**
     * Initializes this coloring from a binary representation read from
     * the supplied stream.
     *
     * @param in the input stream to read the coloring from
     * @return true if the read was successful, false otherwise
     */
    bool readBinary(std::istream& in);

    /**
     * Computes a representation of this coloring as a constrained
     * triangulation.  Each constrained edge represents a color
     * discontinuity and the faces are annotated with their colors.
     * The FaceInfoType supplied as a template parameter must derive
     * from FaceColorInfo.
     *
     * \todo I can't get this to compile when I use the typedef
     * "typename Triangulation<FaceInfoType>::Triangulation" as the
     * parameter type.
     *
     * @param tri the triangulation that is first cleared, and then
     *            updated to represent this coloring
     */
    template <typename TriangulationT>
    void toTriangulation(TriangulationT& tri) const;

    /**
     * Visualizes this coloring on the supplied widget.
     */
    void visualize(CGAL::Qt_widget& widget,
                   bool drawEdges = true,
                   bool drawVertices = true,
                   bool drawRegions = false,
                   bool drawQueries = false) const;

    /**
     * An equality operator.  Two colorings are equal iff they are the
     * same object.
     */
    bool operator==(const Coloring& other) const {
      return (this == &other);
    }

    /**
     * An inequality operator.  Two colorings are equal iff they are the
     * same object.
     */
    bool operator!=(const Coloring& other) const {
      return (this != &other);
    }
  };

  /**
   * Writes a text representation of the coloring to the stream.
   *
   * @param out the output stream
   * @param c   the coloring to write
   * @return    the output stream
   */
  inline std::ostream& operator<<(std::ostream& out, const Coloring& c) {
    c.write(out);
    return out;
  }

  /**
   * Reads a coloring from the stream.
   *
   * @param in  the input stream
   * @param c   the coloring to initialize
   * @return    the input stream
   */
  inline std::istream& operator>>(std::istream& in, Coloring& c) {
    if (!c.read(in))
      in.setstate(std::ios_base::failbit);
    return in;
  }

  template <typename TriangulationT>
  void Coloring::toTriangulation(TriangulationT& tri) const {
    typedef TriangulationT Triangulation;
    typedef typename Triangulation::Vertex_handle TVh;
    typedef typename Triangulation::Face_handle TFh;
    // Clear the current triangulation.
    tri.clear();
    // Build a map from vertex handles of the coloring to vertex
    // handles of the triangulation.
    std::map<VertexHandle, TVh> c2t;
    // Add the vertices of the coloring to the triangulation.
    for (int i = 0; i < numVertices(Coloring::Vertex::CORNER); i++) {
      Coloring::VertexHandle v = 
        getVertex(Coloring::Vertex::CORNER, i);
      c2t[v] = tri.insert(v->point());
    }
    for (int i = 0; i < numVertices(Coloring::Vertex::BOUNDARY); i++) {
      Coloring::VertexHandle v = 
        getVertex(Coloring::Vertex::BOUNDARY, i);
      c2t[v] = tri.insert(v->point());
    }
    for (int i = 0; i < numVertices(Coloring::Vertex::INTERIOR); i++) {
      Coloring::VertexHandle v = 
        getVertex(Coloring::Vertex::INTERIOR, i);
      c2t[v] = tri.insert(v->point());
    }
    // Add all interior edges as contraints.
    for (int i = 0; i < numInteriorEdges(); i++) {
      const Coloring::IntEdgeHandle e = getInteriorEdge(i);
      Coloring::VertexHandle u = e->getPrevVertex();
      Coloring::VertexHandle v = e->getNextVertex();
      tri.insert_constraint(c2t[u], c2t[v]);
    }
    // Now color the faces.  First invalidate all the colors.
    typedef typename Triangulation::Finite_faces_iterator FiniteFacesIterator;
    FiniteFacesIterator it = tri.finite_faces_begin();
    FiniteFacesIterator end = tri.finite_faces_end();
    while (it != end) {
      it->info().color = INVALID_COLOR;
      it++;
    }
    // Then color one of the faces.
    Geometry::Point p;
    Color c;
    getPointWithColor(p, c);
    TFh fh = tri.locate(p);
    assert(!tri.is_infinite(fh));
    fh->info().color = c;
    // Color neighbors via a depth-first search.
    std::stack<TFh> unvisited;
    unvisited.push(fh);
    while (!unvisited.empty()) {
      fh = unvisited.top(); unvisited.pop();
      assert(fh->info().color != INVALID_COLOR);
      // Visit any neighbors that are finite and not yet colored.
      for (int i = 0; i < 3; i++) {
        TFh nbr = fh->neighbor(i);
        if ((nbr->info().color == INVALID_COLOR) && 
            (!tri.is_infinite(nbr))) {
          if (fh->is_constrained(i))
            nbr->info().color = opposite(fh->info().color);
          else
            nbr->info().color = fh->info().color;
          unvisited.push(nbr);
        }
      }
    }
  }

} // End of namespace: Arak

#endif
