// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _PROPERTIES_H
#define _PROPERTIES_H

#include <ios>
#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <map>
#include "global.hpp"

namespace Arak {

  namespace Util {

    /**
     * A property map is a map from property names to values (both of
     * which are represented by strings).
     */
    typedef std::map<std::string, std::string> PropertyMap;

    /**
     * Gets the value of the property
     */
    inline const std::string& getp(const PropertyMap& props,
				   const std::string& name) 
      throw (std::invalid_argument) {
      PropertyMap::const_iterator it = props.find(name);
      if (it == props.end()) {
	std::cerr << "Value not specified for property \'"
		  << name << "\'." << std::endl;
	throw std::invalid_argument(name);
      } else
	return it->second;
    }

    /**
     * Determines if a property has been assigned a value.
     */
    inline bool hasp(const PropertyMap& props,
		     const std::string& name) {
      return (props.find(name) != props.end());
    }

    /**
     * Reads a property map from an input stream.  If the read is not
     * successful then the std::ios_base::failbit is set in the input
     * stream.  The input stream must consist of assignments of the form
     * 'name = value' (whitespace unimportant), possibly with comments
     * that start with '#' and continue to the next newline.  If the same
     * name is specified multiple times then the last value is stored.
     * Properties in the supplied map are overwritten if they are
     * specified in the stream.
     *
     * @param in the input stream
     * @param pm the property map to populate
     * @return   the input stream after the read
     */
    template<typename charT, typename traits>
    std::basic_istream<charT,traits>&
    operator>>(std::basic_istream<charT,traits>& in, PropertyMap& pm) {
      while (in.good()) {
	// Skip comments and leading whitespace.
	in >> skipcomments >> std::ws;
	charT c;
	std::string name;
	bool readEq = false;
	// Check that the stream is good.
	if (!in.good()) break;
	// Read the name.
	while (in.get(c)) {
	  if (c == '=') {
	    readEq = true;
	    break;
	  } else if (std::isspace(c, std::locale("")))
	    break;
	  name += c;
	}
	if (in.bad()) {
	  in.setstate(std::ios_base::failbit);
	  break;
	}
	// If the equal sign has not been read off, read it.
	if (!readEq) {
	  in >> skipcomments >> std::ws >> c;
	  if (in.bad() || (c != '=')) {
	    in.setstate(std::ios_base::failbit);
	    break;
	  }
	}
	// Read the value.
	std::string value;
	in >> skipcomments >> std::ws >> value;
	if (in.bad()) {
	  in.setstate(std::ios_base::failbit);
	  break;
	}
	// Store the pair.
	pm[name] = value;
      }
      return in;
    }

    /**
     * Writes a property map to an output stream.
     *
     * @param out the output stream
     * @param pm  the property map to output
     * @return    the output stream after the write
     */
    template<typename charT, typename traits>
    std::basic_ostream<charT,traits>&
    operator<<(std::basic_ostream<charT,traits>& out, const PropertyMap& pm) {
      // Compute the width of the widest name.
      int width = 0;
      for (PropertyMap::const_iterator it = pm.begin(); it != pm.end(); it++) {
	const PropertyMap::value_type& pair = *it;
	const std::string& name = pair.first;
	width = std::max<int>(width, name.length());
      }
      // Print the pairs.
      for (PropertyMap::const_iterator it = pm.begin(); it != pm.end(); it++) {
	const PropertyMap::value_type& pair = *it;
	out << pair.first;
	for (int i = pair.first.length(); i < width; i++) out.put(' ');
	out << " = " << pair.second << std::endl;
      }
      return out;
    }

    /**
     * Parses a value from a string using its stream input operator.
     *
     * @param str   the string
     * @param value the place the parsed value is stored
     * @return      true if the parsing was successful
     */
    template<typename T>
    bool parse(const std::string& str, T& value) {
      std::istringstream in(str);
      in >> value;
      return (!in.bad());
    }

  } // End of namespace: Arak::Util

} // End of namespace: Arak

#endif
