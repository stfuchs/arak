// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _MCMC_HPP
#define _MCMC_HPP

#include "properties.hpp"
#include "random.hpp"

namespace Arak {

  /** 
   * A Metropolis-Hastings Markov chain.
   */
  template <class MarkovChainTraits>
  class MarkovChain {

  public:

    typedef typename MarkovChainTraits::StateType StateType;
    typedef typename MarkovChainTraits::DistributionType DistributionType;
    typedef typename MarkovChainTraits::ProposalType ProposalType;
    typedef typename MarkovChainTraits::MoveType MoveType;

  protected:

    /**
     * A source of pseudorandomness.
     */
    Arak::Util::Random& random;

    /**
     * The distribution sampled by this Markov chain.
     */
    DistributionType& dist;

    /**
     * The proposal distribution used by this Markov chain.
     */
    ProposalType& proposal;

    /**
     * The current state of the Markov chain.
     */
    StateType& state;

    /**
     * The log likelihood of the current state.
     */
    double logLikelihood;

    /**
     * The number of times #advance() has been called.
     */
    unsigned long int numSteps;
  
    /**
     * The number of changes to the state of the Markov chain.
     */
    unsigned long int numMoves;

    /**
     * The number of initial samples to discard.
     */
    unsigned int burnIn;

    /**
     * The number of steps to discard in between samples.
     */
    unsigned int stride;

  public:

    /**
     * Default constructor.  Given a stationary distribution, a proposal
     * distribution, and an initial state, this builds a new Markov
     * chain.
     *
     * @param dist     the distribution to be sampled
     * @param proposal the proposal distribution of the Markov chain
     * @param state    the initial state of the Markov chain
     * @param props    a property map containing properties relevant 
     *                 to the Markov chain
     * @param random   a source of pseudorandomness 
     */
    MarkovChain(DistributionType& dist, 
                ProposalType& proposal,
                StateType& state,
                const Arak::Util::PropertyMap& props,
                Arak::Util::Random& random = Arak::Util::default_random) 
      : random(random), 
        dist(dist), 
        proposal(proposal),
        state(state), 
        logLikelihood(dist.logLikelihood(state)),
        numSteps(0),
        numMoves(0), 
        burnIn(0), 
        stride(0) { 
      using namespace Arak::Util;
      // Process the estimation parameters.
      if (hasp(props, "arak.mcmc.estimation.burn_in")) 
        assert(parse(getp(props, "arak.mcmc.estimation.burn_in"), burnIn));
      if (hasp(props, "arak.mcmc.estimation.stride")) 
        assert(parse(getp(props, "arak.mcmc.estimation.stride"), stride));
    }

    /**
     * Returns a const reference to the current state of the Markov
     * chain.
     *
     * @return the current state of the Markov chain
     */
    const StateType& getState() const { return state; }

    /**
     * Returns the log likelihood of the current state.
     */
    double getLogLikelihood() const { return logLikelihood; }

    /**
     * Returns a mutable reference to the current state of the Markov
     * chain.
     *
     * @return the current state of the Markov chain
     */
    StateType& getState() { return state; }

    /**
     * Returns a const reference to the chain's stationary measure.
     *
     * @return the Arak process
     */
    const DistributionType& getDistribution() const { 
      return dist; 
    }

    /**
     * Advances the Markov chain using a Metropolis-Hastings step.  This
     * method returns truth if the proposed move was accepted.
     *
     * @return true if the state of the chain changed
     */
    virtual bool advance(){
      numSteps++;
      // Sample the proposal distribution.
      proposal.sample(state, random, true);
      MoveType& move = proposal.move();      
      // Compute the forwards and reverse proposal probabilities.
      double pll = proposal.ll(state);
      double rpll = proposal.rll(state);
      // Calculate the current log likelihood.
      double llh = dist.logLikelihood(state);
      // Perform the move and then check if it must be undone.
      move.execute(state);
      double llh_new = dist.logLikelihood(state);
      // Calculate the Metropolis-Hastings ratio.
      double mh_log_ratio = (llh_new + rpll) - (llh + pll);
      double mh_ratio = exp(mh_log_ratio);
      // Reject the move with some probability.
      if ((mh_ratio < 1.0) && (random.uniform() > mh_ratio)) {
        move.undo(state);
        proposal.result(false);
        return false;
      }
      // The move was successful.  Update the log likelihood.
      numMoves++;
      logLikelihood = dist.logLikelihood(state);
      proposal.result(true);
      return true;
    }

    /**
     * Returns the number of times #advance() has been called.
     */
    unsigned long int getNumSteps() const { return numSteps; }
    
    /**
     * Returns the number of samples generated by this Markov chain.
     * This is the number of steps divided by the stride, minus the
     * burn-in.
     */
    unsigned long int getNumSamples() const { 
      unsigned long int s = (numSteps / (stride + 1));
      return (s > burnIn) ? s - burnIn : 0;
    }
    
    /**
     * Returns true if the current state of the Markov chain is a
     * sample.
     */
    bool isSample() const { 
      return (getNumSamples() > burnIn) && 
        ((numSteps % (stride + 1)) == 0);
    }

    /**
     * Returns the number of times the state of the Markov chain has
     * changed.
     */
    unsigned long int getNumMoves() const { return numMoves; }

    /**
     * Returns the proportion of accepted moves.
     */
    double acceptance() const { return double(numMoves) / double(numSteps);  }

  };
  
  /** 
   * An annealed Metropolis-Hastings Markov chain.
   */
  template <class MarkovChainTraits>
  class AnnealedMarkovChain : public MarkovChain<MarkovChainTraits> {

  public:

    typedef typename MarkovChain<MarkovChainTraits>::StateType StateType;
    typedef typename MarkovChain<MarkovChainTraits>::DistributionType DistributionType;
    typedef typename MarkovChain<MarkovChainTraits>::ProposalType ProposalType;
    typedef typename MarkovChain<MarkovChainTraits>::MoveType MoveType;

  protected:

    // due to template base class, the compiler requires some additional info
    using MarkovChain<MarkovChainTraits>::random;
    using MarkovChain<MarkovChainTraits>::dist;
    using MarkovChain<MarkovChainTraits>::proposal;
    using MarkovChain<MarkovChainTraits>::state;
    using MarkovChain<MarkovChainTraits>::logLikelihood;
    using MarkovChain<MarkovChainTraits>::numSteps;
    using MarkovChain<MarkovChainTraits>::numMoves;
    using MarkovChain<MarkovChainTraits>::burnIn;
    using MarkovChain<MarkovChainTraits>::stride;

    /**
     * The length of the annealing schedule.
     */
    unsigned long int duration;

    /**
     * The minimum (and final) temperature of the annealing schedule.
     */
    double cold;

    /**
     * A parameter of the annealing schedule.  The Arak process
     * potential is scaled by the inverse temperature \f$1/T\f$, where
     * \f$T = \beta_1 + \beta_2/t^\theta\f$ where \f$t\f$ is the
     * iteration number.
     *
     * @see #beta2
     * @see #theta
     */
    double beta1;

    /**
     * A parameter of the annealing schedule.  The Arak process
     * potential is scaled by the inverse temperature \f$1/T\f$, where
     * \f$T = \beta_1 + \beta_2/t^\theta\f$ where \f$t\f$ is the
     * iteration number.
     *
     * @see #beta2
     * @see #theta
     */
    double beta2;

    /**
     * A parameter of the annealing schedule.  The Arak process
     * potential is scaled by the inverse temperature \f$1/T\f$, where
     * \f$T = \beta_1 + \beta_2/t^\theta\f$ where \f$t\f$ is the
     * iteration number.
     *
     * @see #beta2
     * @see #theta
     */
    double theta;

  public:

    /**
     * Default constructor.  Given a stationary distribution, a
     * proposal distribution, and an initial state, this builds a
     * new annealed Markov chain.  If tempMax, tempMin, and duration are
     * supplied, then they define an annealing schedule in which the
     * Arak process potential is scaled by the inverse temperature
     * \f$1/T\f$, which is updated according to the schedule \f$T =
     * \beta_1 + \beta_2/t^\theta\f$ where \f$t\f$ is the iteration
     * number.  The parameters \f$\beta_1\f$, \f$\beta_2\f$, and
     * \f$\theta\f$ are computed using the maximum (initial)
     * temperature, minimum temperature, and the number of steps that
     * it will take to reach the minimum temperature.
     *
     * @param dist     the distribution to be sampled
     * @param proposal the proposal distribution of the Markov chain
     * @param state    the initial state of the Markov chain
     * @param props    a property map containing properties relevant 
     *                 to the Markov chain
     * @param random   a source of pseudorandomness 
     */
    AnnealedMarkovChain(DistributionType& dist, 
                        ProposalType& proposal,
                        StateType& state,
                        const Arak::Util::PropertyMap& props,
                        Arak::Util::Random& random = 
                        Arak::Util::default_random) 
      : MarkovChain<MarkovChainTraits>(dist, proposal, state, props, random) { 
      using namespace Arak::Util;
      // Process the annealing parameters.
      if (hasp(props, "arak.mcmc.annealing.hot")) {
        double hot;
        assert(parse(getp(props, "arak.mcmc.annealing.hot"), hot));
        assert(parse(getp(props, "arak.mcmc.annealing.cold"), cold));
        assert(parse(getp(props, "arak.mcmc.annealing.duration"), duration));
        // Compute the annealing parameters from the supplied parameters.
        if (hot != cold) {
          theta = (ln(hot) - ln(cold)) / ln(duration);
          double power = pow(double(duration), theta);
          beta1 = (cold - hot / power) * (1.0 - 1.0 / power);
          beta2 = hot - beta1;
        } else {
          beta1 = 0.0;
          beta2 = 1.0;
          theta = 0.0;
        }
      } else {
        beta1 = 1.0;
        beta2 = 0.0;
        theta = 0.0;
      }
    }

    /**
     * Advances the annealed Markov chain using a Metropolis-Hastings
     * step.  This method returns truth if the proposed move was
     * accepted.
     *
     * @return true if the state of the chain changed
     */
    virtual bool advance(){
      numSteps++;
      // Sample the proposal distribution.
      proposal.sample(state, random, false);
      MoveType& move = proposal.move();      
      // Compute the temperature.
      double temp;
      if (numSteps > duration)
        temp = cold;
      else
        temp = beta1 + beta2 / pow(double(numSteps), theta);
      // Compute the forwards and reverse proposal probabilities.
      double pll = proposal.ll(state);
      double rpll = proposal.rll(state);
      // Calculate the current (annealed) log likelihood.
      double llh = dist.logLikelihood(state, temp);
      // Perform the move and then check if it must be undone.
      move.execute(state);
      double llh_new = dist.logLikelihood(state, temp);
      // Calculate the Metropolis-Hastings ratio.
      double mh_log_ratio = (llh_new + rpll) - (llh + pll);
      double mh_ratio = exp(mh_log_ratio);
      // Reject the move with some probability.
      if ((mh_ratio < 1.0) && (random.uniform() > mh_ratio)) {
        move.undo(state);
        proposal.result(false);
        return false;
      }
      // The move was successful.  Update the log likelihood.
      numMoves++;
      logLikelihood = dist.logLikelihood(state);
      proposal.result(true);
      return true;
    }

  };

  /** 
   * A stochastic hill climbing Markov chain.
   */
  template <class MarkovChainTraits>
  class StochasticHillClimber : public MarkovChain<MarkovChainTraits> {

  public:

    typedef typename MarkovChain<MarkovChainTraits>::StateType StateType;
    typedef typename MarkovChain<MarkovChainTraits>::DistributionType DistributionType;
    typedef typename MarkovChain<MarkovChainTraits>::ProposalType ProposalType;
    typedef typename MarkovChain<MarkovChainTraits>::MoveType MoveType;

  protected:
    // due to template base class, the compiler requires some additional info
    using MarkovChain<MarkovChainTraits>::random;
    using MarkovChain<MarkovChainTraits>::dist;
    using MarkovChain<MarkovChainTraits>::proposal;
    using MarkovChain<MarkovChainTraits>::state;
    using MarkovChain<MarkovChainTraits>::logLikelihood;
    using MarkovChain<MarkovChainTraits>::numSteps;
    using MarkovChain<MarkovChainTraits>::numMoves;
    using MarkovChain<MarkovChainTraits>::burnIn;
    using MarkovChain<MarkovChainTraits>::stride;

  public:

    /**
     * Default constructor.  Given a stationary distribution, a proposal
     * distribution, and an initial state, this builds a new Markov
     * chain.
     *
     * @param process  the Arak process to be simulated
     * @param proposal the proposal distribution of the Markov chain
     * @param coloring the initial state of the Markov chain
     * @param props    a property map containing properties relevant 
     *                 to the hill climber
     * @param random   a source of pseudorandomness 
     */
    StochasticHillClimber(DistributionType& dist, 
                          ProposalType& proposal,
                          StateType& state,
                          const Arak::Util::PropertyMap& props,
                          Arak::Util::Random& random = Arak::Util::default_random) 
      : MarkovChain<MarkovChainTraits>(dist, proposal, state, props, random) { }

    /**
     * Advances the Markov chain using a stochastic hill climbing
     * step.  This method returns truth if the proposed move was
     * accepted.
     *
     * @return true if the state of the chain changed
     */
    virtual bool advance(){
      numSteps++;
      // Sample the proposal distribution.
      proposal.sample(state, random, false);
      MoveType& move = proposal.move();      
      // Perform the move and then check if it must be undone.
      move.execute(state);
      double llh_new = dist.logLikelihood(state);
      // Reject the move if it reduced the log likelihood.
      if (llh_new <= logLikelihood) {
        move.undo(state);
        proposal.result(false);
        return false;
      }
      // The move was successful.  Update the log likelihood.
      numMoves++;
      logLikelihood = llh_new;
      proposal.result(true);
      return true;
    }

  };

}

#endif
