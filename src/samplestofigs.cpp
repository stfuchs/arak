// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#include <iostream>
#include <fstream>
#include <cstdlib>
#include "parsecl.hpp"
#include "coloring.hpp"

int main(int argc, char** argv) {
  using namespace Arak::Util;
  // Parse the command line.
  CommandLine cl;
  CommandLine::Parameter<std::string> 
    prefix("-p", "--prefix", "the prefix for the fig file names",
	   std::string("sample"));
  cl.add(prefix);
  CommandLine::Parameter<int> 
    stride("-s", "--stride", 
	   "the number of samples skipped between samples that are rendered", 
	   0);
  cl.add(stride);

  if (!cl.parse(argc, argv, std::cerr)) {
    cl.printUsage(std::cerr);
    exit(1);
  }

  Arak::Coloring c;
  int i = 0;
  char *buf = new char[prefix.value().length() + 32];
  while (std::cin.good() && !std::cin.eof()) {
    if (!c.readBinary(std::cin)) {
      std::cerr << "Error reading binary coloring from standard input." 
		<< std::endl;
      return 1;
    }
    std::sprintf(buf, "%s%d.fig", prefix.value().data(), ++i);
    std::ofstream out(buf);
    c.writeXfig(out);
    out.close();
  }
  std::cerr << "Rendered " << i << " colorings." << std::endl;
  delete [] buf;
  return 0;
}
