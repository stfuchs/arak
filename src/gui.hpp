// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _GUI_HPP
#define _GUI_HPP

#include "geometry.hpp"
#include "coloring.hpp"
#include "arak.hpp"
#include "estimation.hpp"

#include <qapplication.h>
#include <CGAL/IO/Qt_widget.h>
#include <CGAL/IO/Qt_widget_layer.h>
#include <qmainwindow.h>
#include <qtimer.h>
#include <qmutex.h>
#include <qlabel.h>

/**
 * Renders a polygon to a Qt widget as a set of triangles.  If the
 * fill color is set, this will render the polygon as solid.  (If the
 * polygon has fewer than three points, it is rendered as a point or
 * line segment.)
 *
 * \todo Implement rendering of non-convex polygons
 */
inline CGAL::Qt_widget& operator<<(CGAL::Qt_widget& widget, 
                                   Arak::Geometry::Polygon& poly) {
  assert(poly.is_convex()); // TODO: if polygon is non-convex, first
  // partition it into convex polygons using
  // CGAL::approx_convex_partition_2.
  int n = poly.size();
  if (n == 1)
    widget << poly[0];
  else if (n == 2)
    widget << *(poly.edges_begin());
  else if (n > 2) {
    const Arak::Geometry::Point p = poly[0];
    const Arak::Geometry::Point q = poly[1];
    for (int i = 2; i < n; i++)
      widget << Arak::Geometry::Triangle(poly[0], poly[i - 1], poly[i]);
  }
  return widget;
}

namespace Arak {


  enum VizType {
    PROC_AND_BD = 0,
    COLORING,
    PT_COL_EST_AND_BD,
    QUERY_PTS_AND_BD
  };

  QApplication* InitGui(int argc, 
                        char** argv, 
                        int width, 
                        int height, 
                        Coloring& c,
                        ArakProcess& p,
                        GridColorEstimator& e,
                        QMutex& mutex,
                        bool toolbar = true,
                        double refreshRateHz = 2.0);

  class ColoringQTLayer : public CGAL::Qt_widget_layer {
    friend class ColoringQTWidget;
  protected:
    Coloring& c;
    ArakProcess& p;
    GridColorEstimator& e;
    QMutex& mutex;
    VizType curType;
  public:
    ColoringQTLayer(Coloring& c, 
                    ArakProcess& p, 
                    GridColorEstimator& e, 
                    QMutex& mutex);
    void draw();
  };

  class ColoringQTWidget : public CGAL::Qt_widget {
    Q_OBJECT
  protected:
    QMainWindow* win;
    ColoringQTLayer* layer;
    QTimer* timer;
  private slots:
    void timeout();
  public:
    ColoringQTWidget(QMainWindow& win, 
                     ColoringQTLayer& layer, 
                     double refreshRateHz = 2.0);
    void mousePressEvent(QMouseEvent *e);
  };
}

#endif
