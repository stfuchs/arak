// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _QUERY_POINT_HPP
#define _QUERY_POINT_HPP

#include <CGAL/Kd_tree.h>
#include <CGAL/Fuzzy_iso_box.h>
#include <CGAL/Search_traits.h>
#include "function_output_iterator.hpp"
#include "global.hpp"
#include "geometry.hpp"

namespace Arak {

  /** 
   * This class is inherited by objects that need to be informed when
   * the color of a query point changes.
   */
  class QueryPointListener {

  public:

    /**
     * Constructor.
     */
    QueryPointListener() { }

    /**
     * Reacts to information that the query point has been recolored.
     * This pure virtual function is implemented by derived classes to
     * define this listener's behavior.
     *
     * @param color the new color of the query point
     */
    virtual void recolor(Color color) = 0;

  }; // end of class: QueryPointListener

  /**
   * A list of listeners associated with the same query point.
   */
  typedef std::list<QueryPointListener*> QueryPointListenerList;

  /** 
   * A query point is a point whose current color is maintained by a
   * Coloring object.  This class is designed so that it may be
   * treated as a lightweight scalar type (i.e., the default
   * assignment operator works) and therefore used in template
   * structures.
   */
  class QueryPoint : public Geometry::Point {

  protected:

    /**
     * A pointer to a list of listeners for this point.
     */
    QueryPointListenerList* listeners;

    /**
     * A pointer to the current color of this query point.
     */
    Color* c;

  public:

    /**
     * Constructor.
     *
     * @param p         the location of the query point
     * @param listeners a pointer to a list of listeners for this point 
     * @param color     a pointer to the color of this query point, 
     *                  which may be modified by this object
     */
    QueryPoint(const Geometry::Point& p,
               QueryPointListenerList* listeners,
               Color* color) 
      : Geometry::Point(p), listeners(listeners), c(color) { }

    /**
     * Default constructor.  This constructor is used to build
     * anonymous query points used in searching the query point index.
     *
     * @param p the location of the query point
     */
    QueryPoint(const Geometry::Point p = CGAL::ORIGIN) 
      : Geometry::Point(p), listeners(NULL), c(NULL){ }

    /**
     * Returns the current color of this query point.
     */
    Color color() const { return *c; }

    /**
     * Sets the color of this query point and informs all listeners
     * for this query point that its color has changed.
     */
    void setColor(Color color) const {
      if (color != *c) {
        *c = opposite(color);
        recolor();
      }
    }

    /**
     * Recolors this point and informs all listeners for this query
     * point that its color has changed.
     */
    void recolor() const {
      *c = opposite(*c);
      typedef QueryPointListenerList::iterator Iterator;
      for (Iterator it = listeners->begin(); it != listeners->end(); it++) {
        QueryPointListener* listener = *it;
        listener->recolor(*c);
      }
    }

    /**
     * Adds an additional listener to this query point.
     *
     * @param listener the new listener
     */
    void addListener(QueryPointListener* listener) const {
      listener->recolor(*c);
      listeners->push_front(listener);
    }

  }; // end of class: QueryPoint

  /**
   * This class is specialized by objects that create listeners for
   * points.
   */
  class QueryPointListenerFactory {

  public:

    /**
     * Creates a new listener for the supplied query point.
     */
    virtual QueryPointListener* create(const QueryPoint& q) = 0;

  };

  /** 
   * A query point index is a collection of query points that are
   * indexed to permit efficient range searching.  This is a simple
   * wrapper for the CGAL KD-tree data structure.
   */
  class QueryPointIndex {

  protected:

    /**
     * A vector of the query points.
     */
    std::vector<QueryPoint> qpoints;

    /**
     * An array of listener lists, one per query point.
     */
    QueryPointListenerList* listenerLists;

    /**
     * An array of colors, one per query point, that indicates their
     * current color.
     */
    Color* color;

    /**
     * The type of kd-tree traits.
     */
    class SearchTraits {
    public:
      typedef Geometry::Kernel K;
      typedef Arak::QueryPoint Point_d;
      typedef K::Iso_rectangle_2 Iso_box_d;
      typedef K::Circle_2 Sphere_d;
      typedef K::Cartesian_const_iterator_2 Cartesian_const_iterator_d;
      typedef K::Construct_cartesian_const_iterator_2 Construct_cartesian_const_iterator_d;
      
      typedef K::Construct_min_vertex_2 Construct_min_vertex_d;
      typedef K::Construct_max_vertex_2 Construct_max_vertex_d;
      typedef K::Construct_center_2 Construct_center_d;
      typedef K::Compute_squared_radius_2 Compute_squared_radius_d;
      
      typedef K::Construct_iso_rectangle_2 Construct_iso_box_d;
      typedef K::FT FT;

      Construct_cartesian_const_iterator_d construct_cartesian_const_iterator_d_object() const
      {
        return Construct_cartesian_const_iterator_d();
      }
    };

    /**
     * A type of a kd-tree of query points.
     */
    typedef CGAL::Kd_tree<SearchTraits> KDTree;

    /**
     * The KD tree of query points.
     */
    KDTree* tree;

    /**
     * This functor is used to recolor all query points inside a
     * triangular region.
     */
    class RecolorTriangleFunction {

    private:

      /**
       * A pointer to the triangle.  We use a pointer here so the
       * default assignment operator works for objects of this type;
       * this is required to use this functor in combination with the
       * boost::function_output_iterator adaptor.
       */
      const Geometry::Triangle* t;

    public:

      /**
       * Constructor.
       *
       * @param t the triangle whose closure is the recolored region
       */
      RecolorTriangleFunction(const Geometry::Triangle& t) : t(&t) { }

      /**
       * Recolors the supplied query point if it lies in the closure
       * of the triangle.
       *
       * @param q the query point
       */
      void operator() (const QueryPoint& q) {
        if (!t->has_on_unbounded_side(q)) q.recolor();
      }
    };

    /**
     * This functor is used to recolor all query points inside a
     * quadrilateral region (that is not necessarily convex).  For
     * efficiency this functor represents such quadrilaterals as the
     * union or difference between two triangles.
     */
    class RecolorQuadrilateralFunction {

    private:

      /**
       * A pointer to the first triangle used to represent the
       * quadrilateral; it must consist of any three of the
       * quadrilateral's vertices.  We use a pointer here so the
       * default assignment operator works for objects of this type;
       * this is required to use this functor in combination with the
       * boost::function_output_iterator adaptor.
       */
      const Geometry::Triangle *t1;

      /**
       * A pointer to the second triangle used to represent the
       * quadrilateral; it must have two adjacent vertices of #t1 and
       * the quadrilateral vertex that is not in #t1.  We use a
       * pointer here so the default assignment operator works for
       * objects of this type; this is required to use this functor in
       * combination with the boost::function_output_iterator adaptor.
       */
      const Geometry::Triangle *t2;

    public:

      /**
       * Constructor.
       *
       * @param t1 the first triangle used to describe the 
       *           quadrilateral; it must consist of any three 
       *           vertices
       * @param t2 the second triangle used to describe the 
       *           quadrilateral; it must consist of any two 
       *           adjacent vertices in t1 and the remaining 
       *           vertex of the quadrilateral
       */
      RecolorQuadrilateralFunction(const Geometry::Triangle& t1,
                                   const Geometry::Triangle& t2) 
        : t1(&t1), t2(&t2) { }

      /**
       * Recolors the supplied query point if it lies in the closure
       * of the quadrilateral.
       *
       * @param q the query point
       */
      void operator() (const QueryPoint& q) {
        /**
         * \todo The technique used to recolor points does not work
         * for points on the boundary of a quadrilateral that is not
         * simple.
         */
        if (!t1->has_on_unbounded_side(q)) q.recolor();
        if (!t2->has_on_unbounded_side(q)) q.recolor();
      }
    };

  public:

    /**
     * Constructor.  This initializes a point index from a vector of
     * Point objects.
     *
     * @param points    a vector of point objects
     * @param listeners a parallel vector of listeners, one per point
     */
    template <class QueryPointListenerType>
    QueryPointIndex(const std::vector<Geometry::Point>& points,
                    const std::vector<QueryPointListenerType*>& listeners) {
      // Allocate the color variables and listener lists
      int n = points.size();
      color = new Color[n];
      listenerLists = new QueryPointListenerList[n];
      // Create the query points.
      qpoints.reserve(n);
      for (int i = 0; i < n; i++) {
        color[i] = INVALID_COLOR;
        QueryPoint q(points[i], &(listenerLists[i]), &(color[i]));
        qpoints.push_back(q);
        q.addListener(listeners[i]);
      }
      // Initialize the KD tree.
      tree = new KDTree(qpoints.begin(), qpoints.end());
    }

    /**
     * Constructs a query point index with a grid of query points in
     * the interior of a rectangular window.
     * 
     * @param r    the rectangular window
     * @param rows the number of rows in the grid
     * @param cols the number of columns in the grid
     */
    QueryPointIndex(const Geometry::Rectangle& r, int rows, int cols);

    /**
     * Constructs a query point containing the query points in both of
     * the supplied indexes.
     * 
     * @param q1 the first query point index
     * @param q2 the second query point index
     */
    QueryPointIndex(const QueryPointIndex& q1, 
                    const QueryPointIndex& q2);

    /**
     * Destructor.
     */
    ~QueryPointIndex();

    /**
     * Adds a query point listener to every query point in this index.
     * The listeners are generated using the supplied factory.
     *
     * @param factory the factory used to generate the listeners
     */
    void addListeners(QueryPointListenerFactory& factory);

    /**
     * Searches for all query points in this index that are in the
     * closure of the supplied rectangle.  These points are written to
     * the supplied output iterator.
     *
     * @param r  the bounding rectangle
     * @param it an output iterator to which the points are written
     */
    template <class OutputIterator>
    void output(const Geometry::Rectangle& r, OutputIterator it) const {
      using namespace Geometry;
      typedef CGAL::Fuzzy_iso_box<SearchTraits> FuzzyBox;
      FuzzyBox box(r.min(), r.max());
      tree->search(it, box);
    }

    /**
     * Searches for all query points in this index that are in the
     * closure of the supplied rectangle.  The supplied unary function
     * is applied to all such points.
     *
     * @param r  the bounding rectangle
     * @param f  a unary function that is applied to all points in r
     */
    template <class UnaryFunction>
    void apply(const Geometry::Rectangle& r, UnaryFunction& f) const {
      output(r, boost::make_function_output_iterator(f));
    }

    /**
     * Applies the supplied unary function to all query points in this
     * index.
     *
     * @param f  a unary function that is applied to all points
     */
    template <class UnaryFunction>
    void applyToAll(UnaryFunction& f) const {
      std::cout << "applyToAll: not implemented" << std::endl;
      //tree->report_all_points(boost::make_function_output_iterator(f));
    }

    /**
     * Returns the number of query points in this set.
     */
    int size() const { return qpoints.size(); }

    /**
     * Returns a const reference to the query point with the supplied index.
     */
    const QueryPoint& point(int i) const { return qpoints[i]; }

    /**
     * Returns the query point with the supplied index.
     */
    QueryPoint& point(int i) { return qpoints[i]; }

    /**
     * Returns the closest query point to the supplied point.
     *
     * @param p a point
     * @return  the query point closest to the supplied point
     */
    const QueryPoint& closest(const Geometry::Point& p) const;

    /**
     * Recolors all query points in the closure of the triangle with
     * the supplied vertices.
     *
     * @param a the first vertex of the triangle
     * @param b the second vertex of the triangle
     * @param c the third vertex of the triangle
     */
    void recolor(const Geometry::Point& a,
                 const Geometry::Point& b,
                 const Geometry::Point& c);

    /**
     * Recolors all query points in the closure of the quadrilateral
     * with the supplied vertices.  The vertices must be supplied in
     * either clockwise or counter-clockwise order.
     *
     * @param a the first vertex of the quadrilateral
     * @param b the second vertex of the quadrilateral
     * @param c the third vertex of the quadrilateral
     * @param d the fourth vertex of the quadrilateral
     */
    void recolor(const Geometry::Point& a,
                 const Geometry::Point& b,
                 const Geometry::Point& c,
                 const Geometry::Point& d);

  }; // end of class: QueryPointIndex
  
}

#endif
