// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#include <string>
#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <stdlib.h>

#include "global.hpp"
#include "parsecl.hpp"

/**
 * A simple program to convert a grid of values to a portable graymap
 * file (PGM) visualization.
 */
int main(int argc, char** argv) {
  using namespace Arak::Util;
  // Parse the command line.
  CommandLine cl;
  CommandLine::Parameter<double> 
    gamma("-g", "--gamma", "the gamma correction value", 1.0 / 0.45);
  cl.add(gamma);
  CommandLine::Parameter<int> 
    depth("-d", "--depth", "the image depth (# gray values)", 256);
  cl.add(depth);
  CommandLine::Parameter<double> 
    max("-min", "--min-intensity", "the surface value mapped to white", 0.0);
  cl.add(max);
  CommandLine::Parameter<double> 
    min("-max", "--max-intensity", "the surface value mapped to black", 1.0);
  cl.add(min);
  CommandLine::Parameter<std::string> 
    inputPath("-i", "--input", 
	      "the input file of point color estimates", std::string());
  cl.add(inputPath);
  CommandLine::Parameter<std::string> 
    outputPath("-o", "--output", "the output PGM file", std::string());
  cl.add(outputPath);
  if (!cl.parse(argc, argv, std::cerr)) {
    cl.printUsage(std::cerr);
    exit(1);
  }

  // Grab the input and output streams.
  std::istream* in = &std::cin;
  if (inputPath.supplied()) {
    in = new std::ifstream(inputPath.value().data());
    if (!in->good()) {
      std::cerr << "Error: cannot open file " << inputPath.value()
		<< " for reading." << std::endl;
      exit(1);
    }
  }
  std::ostream* out = &std::cout;
  if (outputPath.supplied()) {
    out = new std::ofstream(outputPath.value().data());
    if (!out->good()) {
      std::cerr << "Error: cannot open file " << outputPath.value()
		<< " for writing." << std::endl;
      exit(1);
    }
  }

  // Compute the gamma corrected values.
  int* gcv = new int[depth.value()];
  gcv[0] = 0;
  for (int i = 1; i < depth.value(); i++) {
    double raw = double(i) / double(depth.value() - 1);
    double corr = pow(raw, gamma.value());
    gcv[i] = int(round(corr * double(depth.value() - 1)));
  }

  // Skip the number of samples.
  double tmp;
  *in >> tmp;
  // Read the dimensions.
  int rows, cols;
  *in >> rows;
  *in >> cols;
  // Skip the bounding box.
  *in >> tmp >> tmp >> tmp >> tmp;

  // Compute the interpolated values and output the file.
  *out << "P2" << std::endl;                       // magic number
  *out << "# " << outputPath.value() << std::endl; // filename
  *out << cols << " " << rows << std::endl;        // resolution
  *out << (depth.value() - 1) << std::endl;        // max-value
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      double value;
      *in >> value;
      double intensity = 
	(value - min.value()) / (max.value() - min.value());
      intensity = (intensity < 0.0) ? 0.0 : intensity;
      intensity = (intensity > 1.0) ? 1.0 : intensity;
      // Compute the pixel value and then use gamma correction.
      *out << gcv[int(round(intensity * double(depth.value() - 1)))] << " ";
    }
    *out << std::endl;
  }
  out->flush();  

  // Delete any allocated streams.
  if (inputPath.supplied()) delete in;
  if (outputPath.supplied()) delete out;
  
  return 0;
}
