// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _GRID_HPP
#define _GRID_HPP

#include <assert.h>
#include <list>
#include "geometry.hpp"

namespace Arak {

  /** 
   * A grid partitions a rectangular region into rectangular cells of
   * equal height and width.  Each cell is associated with a list of
   * items that can be updated.  This class supports linear-time
   * access to all cells that intersect a given line segment, ray,
   * rectangle, or triangle.
   */
  template <class ItemType>
  class Grid {

  public:

    /**
     * A cell is a rectangular subregion of the window.  The window is
     * partitioned into a grid of cells.  Each cell includes its
     * bottom and left boundaries, and cells against the top and/or
     * right boundaries of the region also include these edges.  In
     * this way each point in the region is assigned to exactly one
     * grid cell.
     */
    class Cell : public Geometry::Rectangle {

      friend class Grid;
      friend class Grid::LineCellIterator;
      friend class Grid::ConstLineCellIterator;
      friend class Grid::RectangleCellIterator;
      friend class Grid::ConstRectangleCellIterator;
      friend class Grid::TriangleCellIterator;
      friend class Grid::ConstTriangleCellIterator;
      friend class Grid::ConvexPolygonCellIterator;
      friend class Grid::ConstConvexPolygonCellIterator;
      
    public:

      /**
       * A list of the items associated with this cell.
       */
      typedef std::list<ItemType> ItemList;

    protected:

      /**
       * An iterator for an interior edge list.
       */
      typedef typename ItemList::iterator ItemIterator;

      /**
       * The row index of this cell.
       */
      int r;

      /**
       * The column index of this cell.
       */
      int c;

      /**
       * A list of items associated with this cell.
       */
      ItemList items;
      
      /**
       * A list of unused items.  This list is used to avoid memory
       * allocation overhead associated with adding items to #items.
       */
      ItemList unused;
      
      /**
       * A pointer to the cell that is immediately above this cell.
       * If there is no such cell, this is NULL.
       */
      Cell* up;

      /**
       * A pointer to the cell that is immediately below this cell.
       * If there is no such cell, this is NULL.
       */
      Cell* down;

      /**
       * A pointer to the cell that is immediately to the right of
       * this cell.  If there is no such cell, this is NULL.
       */
      Cell* right;

      /**
       * A pointer to the cell that is immediately to the left of
       * this cell.  If there is no such cell, this is NULL.
       */
      Cell* left;

    public:

      /**
       * This is a object which permits the efficient removal of an
       * item from a cell's list of items.
       */
      class Entry {

      private:

        /**
         * The cell in which the entry resides.
         */
        Cell* cell;

        /**
         * An iterator that points to the item.
         */
        ItemIterator it;

      public:

        /**
         * Constructor.
         */
        Entry() : cell(NULL), it() { }

        /**
         * Constructor.
         */
        Entry(Cell* cell, ItemIterator it) : cell(cell), it(it) { }

        /**
         * Tests to see if this entry is still valid.
         */
        bool valid() const { return (cell != NULL); }

        /**
         * Removes the item stored in this entry from the internal
         * list of items associated with this cell.  This entry
         * becomes invalid after this operation.
         */
        void remove() {
          assert(valid());
          // Move the item to the unused list.
          cell->unused.splice(cell->unused.begin(), cell->items, it);
          cell = NULL;
        }

        /**
         * Returns the cell in which this entry resides.
         */
        const Cell& getCell() const { return *cell; }

      }; // end of class: Grid::Cell::Entry

      /**
       * Constructor.
       *
       * @param r   the boundary of the cell
       * @param row the row index of the cell
       * @param col the column index of the cell
       */
      Cell(const Geometry::Rectangle& r, int row, int col) : 
        Geometry::Rectangle(r), r(row), c(col), items(),
        up(NULL), down(NULL), right(NULL), left(NULL) { }

      /**
       * Adds an item to this cell's list of items.  The entry in
       * which the item is stored is returned.
       *
       * @param e the item to be inserted
       * @return  the entry in which the item was inserted
       */
      inline Entry add(const ItemType& e) {
        if (unused.empty()) {
          // We have run out of unused items.  Use a simple insertion.
          return Entry(this, items.insert(items.begin(), e));
        } else {
          // The unused list is not empty; reuse its first element.
          ItemIterator it = unused.begin();
          *it = e;
          items.splice(items.begin(), unused, it);
          return Entry(this, items.begin());
        }
      }

      /**
       * Returns a const reference to this cell's list of items.
       */
      inline const ItemList& getItemList() const {
        return items;
      }

      /**
       * Returns a mutable reference to this cell's list of items.
       */
      inline ItemList& getItemList() {
        return items;
      }

      /**
       * Returns the row index of this cell.
       */
      int row() const { return r; }

      /**
       * Returns the column index of this cell.
       */
      int col() const { return c; }

      /**
       * Equality operations.
       */
      bool operator==(const Cell& other) const { return (this == &other); }
      bool operator!=(const Cell& other) const { return (this != &other); }

      /**
       * Returns the point at the center of this cell.
       */
      Geometry::Point center() const {
        return CGAL::midpoint((*this)[0], (*this)[2]);
      }

    }; // end of class: Grid::Cell

    /**
     * A forwards iterator that ranges over the cells whose closures
     * intersect a given ray or line segment.  The current
     * implementation performs an orientation test per iteration.
     *
     * \todo Consider implementing Bresenham's Algorithm
     */
    template <class GridType, class CellType>
    class LineCellIteratorBase {

    protected:
      
      /**
       * The supporting line of the segment or ray that is being traversed.
       */
      Geometry::Line line;

      /**
       * A categorical direction for rays.
       */
      enum CompassDirection {
        NORTH,      /*!< The ray is vertical and directed upward         */
        NORTH_EAST, /*!< The ray is directed into the first quadrant     */
        EAST,       /*!< The ray is horizontal and directed to the right */
        SOUTH_EAST, /*!< The ray is directed into the fourth quadrant    */
        SOUTH,      /*!< The ray is vertical and directed downward       */
        SOUTH_WEST, /*!< The ray is directed into the third quadrant     */
        WEST,       /*!< The ray is horizontal and directed to the left  */
        NORTH_WEST  /*!< The ray is directed into the second quadrant    */
      };

      /**
       * The direction of the traversed ray.
       */
      CompassDirection dir;

      /**
       * The current cell of the iterator.
       */
      CellType* curCell;

      /**
       * The last cell the iterator must visit (or NULL if the
       * iterator should terminate at the boundary of the grid).
       */
      CellType* lastCell;

      /**
       * Computes the direction of the line from p to q.
       *
       * @param p a point
       * @param q a point distinct from p
       * @return  the direction of the line directed from p to q
       */
      inline CompassDirection direction(const Geometry::Point& p,
                                        const Geometry::Point& q) {
        CGAL::Comparison_result xc = CGAL::compare_x(p, q);
        CGAL::Comparison_result yc = CGAL::compare_y(p, q);
        switch (xc) {
        case CGAL::SMALLER:
          switch (yc) {
          case CGAL::SMALLER:
            return NORTH_EAST; 
          case CGAL::LARGER:
            return SOUTH_EAST; 
          case CGAL::EQUAL:
          default:
            return EAST;
          }
        case CGAL::LARGER:
          switch (yc) {
          case CGAL::SMALLER:
            return NORTH_WEST;
          case CGAL::LARGER:
            return SOUTH_WEST;
          case CGAL::EQUAL:
          default:
            return WEST;
          }
        case CGAL::EQUAL:
        default:
          switch (yc) {
          case CGAL::SMALLER:
            return NORTH;
          case CGAL::LARGER:
            return SOUTH;
          case CGAL::EQUAL:
          default:
            assert(false);
          }
        }
      }

    public:

      /**
       * A tag supplied to the constructor to indicate the iterator
       * should traverse the cells intersected by a line segment.
       */
      struct SEGMENT { };

      /**
       * A tag supplied to the constructor to indicate the iterator
       * should traverse the cells intersected by a ray.
       */
      struct RAY { };

      /**
       * Constructor for a cell iterator that travels along a line
       * segment.
       *
       * @param g a grid
       * @param p a point in the closure of the grid's boundary
       * @param q another point in the closure of the grid's 
       *          boundary
       */
      LineCellIteratorBase(GridType& g, 
                           const Geometry::Point &p,
                           const Geometry::Point &q,
                           SEGMENT) : line(p, q) {
        dir = direction(p, q);
        if (g.boundary().has_on_unbounded_side(p) ||
            g.boundary().has_on_unbounded_side(q)) {
          Geometry::Segment s(p, q);
          Geometry::Point r;
          CGAL::Object intersection = CGAL::intersection(s, g.boundary());
          if (CGAL::assign(s, intersection)) {
            curCell = &g.getCell(Geometry::project(s.source(), g.boundary()));
            lastCell = &g.getCell(Geometry::project(s.target(), g.boundary()));
          } else if (CGAL::assign(r, intersection)) {
            curCell = lastCell = &g.getCell(Geometry::project(r, g.boundary()));
          } else {
            curCell = lastCell = NULL;
          }
        } else {
          curCell = &g.getCell(p);
          lastCell = &g.getCell(q);
        }
      }

      /**
       * Constructor for a cell iterator that travels along a ray.
       *
       * @param g a grid
       * @param p a point giving the source of the ray
       * @param q another point, distinct from p, indicating the 
       *          direction of the ray
       */
      LineCellIteratorBase(GridType& g, 
                           const Geometry::Point &p,
                           const Geometry::Point &q,
                           RAY) : line(p, q) {
        dir = direction(p, q);
        if (g.boundary().has_on_unbounded_side(p) ||
            g.boundary().has_on_unbounded_side(q)) {
          Geometry::Segment s(p, q);
          Geometry::Point r;
          CGAL::Object intersection = CGAL::intersection(s, g.boundary());
          if (CGAL::assign(s, intersection)) {
            curCell = &g.getCell(Geometry::project(s.source(), g.boundary()));
            lastCell = &g.getCell(Geometry::project(s.target(), g.boundary()));
          } else if (CGAL::assign(r, intersection)) {
            curCell = lastCell = &g.getCell(Geometry::project(r, g.boundary()));
          } else {
            curCell = lastCell = NULL;
          }
        } else {
          curCell = &g.getCell(p);
          lastCell = NULL;
        }
      }

      /**
       * Constructor for an end iterator outside the grid.
       */
      LineCellIteratorBase() 
        : line(1, 0, 0),
          curCell(NULL), 
          lastCell(NULL) { }

      /**
       * Comparison operators.
       */
      inline bool 
      operator==(const LineCellIteratorBase<GridType,CellType>& other) const
      { return (curCell == other.curCell); }
      inline bool 
      operator!=(const LineCellIteratorBase<GridType,CellType>& other) const 
      { return !(*this == other); }

      /**
       * Increment operators.
       */
      inline LineCellIteratorBase<GridType,CellType>& operator++(int) {
        LineCellIteratorBase<GridType,CellType> tmp(*this);
        ++(*this);
        return tmp;
      }
      inline LineCellIteratorBase<GridType,CellType>& operator++() {
        using namespace Arak::Geometry;
        assert(curCell != NULL);
        if (curCell == lastCell) {
          curCell = NULL;
          return *this;
        }
        switch (dir) {
        case NORTH_EAST:
          switch (CGAL::compare_y_at_x(curCell->vertex(2), line)) {
          case CGAL::SMALLER:
            curCell = curCell->up; break;
          case CGAL::LARGER:
            curCell = curCell->right; break;
          case CGAL::EQUAL:
            curCell = curCell->right;
            if (curCell != NULL)
              curCell = curCell->up; 
            break;
          }
          break;
        case SOUTH_EAST:
          switch (CGAL::compare_y_at_x(curCell->vertex(1), line)) {
          case CGAL::LARGER:
            curCell = curCell->down; break;
          case CGAL::SMALLER:
            curCell = curCell->right; break;
          case CGAL::EQUAL:
            curCell = curCell->right;
            if (curCell != NULL)
              curCell = curCell->down; 
            break;
          }
          break;
        case SOUTH_WEST:
          switch (CGAL::compare_y_at_x(curCell->vertex(0), line)) {
          case CGAL::LARGER:
            curCell = curCell->down; break;
          case CGAL::SMALLER:
            curCell = curCell->left; break;
          case CGAL::EQUAL:
            curCell = curCell->left; 
            if (curCell != NULL)
              curCell = curCell->down; 
            break;
          }
          break;
        case NORTH_WEST:
          switch (CGAL::compare_y_at_x(curCell->vertex(3), line)) {
          case CGAL::SMALLER:
            curCell = curCell->up; break;
          case CGAL::LARGER:
            curCell = curCell->left; break;
          case CGAL::EQUAL:
            curCell = curCell->left; 
            if (curCell != NULL)
              curCell = curCell->up; 
            break;
          }
          break;
        case NORTH:
          curCell = curCell->up; 
          break;
        case SOUTH:
          curCell = curCell->down; 
          break;
        case EAST:
          curCell = curCell->right; 
          break;
        case WEST:
          curCell = curCell->left; 
          break;
        }
        return *this;
      }

      /**
       * Accessors.
       */
      inline CellType* operator->() const {return curCell;}
      inline CellType& operator*() const { return *curCell;}

    }; // end of class: Grid::LineCellIteratorBase

    /**
     * A mutable forwards iterator that ranges over the cells whose
     * closures intersect a given ray or line segment.
     */
    typedef LineCellIteratorBase<Grid, Cell> LineCellIterator;

    /**
     * A const forwards iterator that ranges over the cells whose
     * closures intersect a given ray or line segment.
     */
    typedef LineCellIteratorBase<const Grid, const Cell> ConstLineCellIterator;

    /**
     * A mutable forwards iterator that ranges over the cells whose
     * closures intersect a given axis-aligned rectangle.
     */
    template <class GridType, class CellType>
    class RectangleCellIteratorBase {

    protected:
      
      /**
       * The current i coordinate.
       */
      int i;

      /**
       * The largest i coordinate.
       */
      int imax;

      /**
       * The current j coordinate.
       */
      int j;

      /**
       * The smallest j coordinate.
       */
      int jmin;

      /**
       * The largest j coordinate.
       */
      int jmax;

      /**
       * The current cell of the iterator.
       */
      CellType* curCell;

      /**
       * The grid.
       */
      GridType* grid;

    public:

      /**
       * Constructor for an iterator over all cells whose closures
       * intersect a given rectangle.
       *
       * @param g a grid
       * @param r the rectangle
       */
      RectangleCellIteratorBase(GridType& g, Geometry::Rectangle r)
        : curCell(NULL), grid(&g) { 
        if (CGAL::assign(r, CGAL::intersection(g.boundary(), r))) {
          assert(g.getCellCoords(r.min(), i, jmin));
          j = jmin;
          assert(g.getCellCoords(r.max(), imax, jmax));
          curCell = &g.getCell(i, j);
        } else {
          curCell = NULL;
        }
      }

      /**
       * Constructor for an end iterator.
       */
      RectangleCellIteratorBase() : curCell(NULL), grid(NULL) { }

      /**
       * Comparison operators.
       */
      inline bool operator==(const RectangleCellIteratorBase<GridType,CellType>& other) const
      { return (curCell == other.curCell); }
      inline bool operator!=(const RectangleCellIteratorBase<GridType,CellType>& other) const 
      { return !(*this == other); }

      /**
       * Increment operators.
       */
      inline RectangleCellIteratorBase<GridType,CellType>& operator++(int) {
        RectangleCellIteratorBase<GridType,CellType> tmp(*this);
        ++(*this);
        return tmp;
      }
      inline RectangleCellIteratorBase<GridType,CellType>& operator++() {
        using namespace Arak::Geometry;
        assert(curCell != NULL);
        if ((i == imax) && (j == jmax)) {
          curCell = NULL;
          return *this;
        } else if (j == jmax) {
          i++;
          j = jmin;
          curCell = &(grid->getCell(i, j));
        } else {
          j++;
          curCell = curCell->right;
        }
        return *this;
      }

      /**
       * Accessors.
       */
      inline CellType* operator->() const { return curCell; }
      inline CellType& operator*() const { return *curCell; }

    }; // end of class: Grid::RectangleCellIteratorBase

    /**
     * A mutable forwards iterator that ranges over the cells whose
     * closures intersect a given rectangle.
     */
    typedef RectangleCellIteratorBase<Grid, Cell> 
    RectangleCellIterator;

    /**
     * A const forwards iterator that ranges over the cells whose
     * closures intersect a given rectangle.
     */
    typedef RectangleCellIteratorBase<const Grid, const Cell> 
    ConstRectangleCellIterator;

    /**
     * A forwards iterator that ranges over the cells whose closures
     * intersect a given triangle.
     */
    template <class GridType, class CellType>
    class TriangleCellIteratorBase {

    protected:
      
      /**
       * The current cell of the iterator.
       */
      CellType* curCell;

      /**
       * The grid.
       */
      GridType* grid;

      /**
       * The minimum i coordinate of all cells intersecting the triangle.
       */
      int imin;

      /**
       * The maximum i coordinate of all cells intersecting the triangle.
       */
      int imax;

      /**
       * Stores for each column between #imin and #imax (inclusive)
       * the minimum j coordinate of all cells intersecting the
       * triangle.
       */
      std::vector<int> jmins;

      /**
       * Stores for each column between #imin and #imax (inclusive)
       * the maximum j coordinate of all cells intersecting the
       * triangle.
       */
      std::vector<int> jmaxs;

    public:

      /**
       * Constructor for an iterator over all cells whose closures
       * intersect a given triangle.
       *
       * @param g a grid
       * @param t the triangle
       */
      TriangleCellIteratorBase(GridType& g, Geometry::Triangle t)
        : curCell(NULL), grid(&g) { 
        // Compute the smallest rectangle that covers the intersection
        // of the triangle and the grid's closure.
        Geometry::Rectangle r = Geometry::boundingRectangle(t[0], t[1], t[2]);
        if (!CGAL::assign(r, CGAL::intersection(g.boundary(), r))) {
          // The triangle does not intersect the grid's closure.
          curCell = NULL; 
          return;
        }
        // Compute the columns crossed by the triangle.
        int jmin, jmax;
        assert(g.getCellCoords(r.min(), imin, jmin));
        assert(g.getCellCoords(r.max(), imax, jmax));
        assert(imin <= imax);
        assert(jmin <= jmax);
        // Compute the values of jmins and jmaxs using line searches
        // along the boundary of the triangle.
        jmins.insert(jmins.end(), imax - imin + 1, 
                     std::numeric_limits<int>::max());
        jmaxs.insert(jmaxs.end(), imax - imin + 1, 
                     std::numeric_limits<int>::min());
        for (int v = 0; v < 3; v++) {
          ConstLineCellIterator it(g, t[v], t[v + 1], 
                                   typename ConstLineCellIterator::SEGMENT()), end;
          while (it != end) {
            const Cell& cell = *it;
            int i = cell.row();
            int j = cell.col();
            int k = i - imin;
            jmins[k] = std::min<int>(jmins[k], j);
            jmaxs[k] = std::max<int>(jmaxs[k], j);
            ++it;
          }
        }
        // Replace any unset limits using the bounding rectangle.
        std::replace(jmins.begin(), jmins.end(),
                     std::numeric_limits<int>::max(), jmin);
        std::replace(jmaxs.begin(), jmaxs.end(),
                     std::numeric_limits<int>::min(), jmax);
        // Initialize the iterator at the first cell.
        curCell = &g.getCell(imin, jmins[0]);
      }

      /**
       * Constructor for an end iterator.
       */
      TriangleCellIteratorBase() : curCell(NULL), grid(NULL) { }

      /**
       * Comparison operators.
       */
      inline bool operator==(const TriangleCellIteratorBase<GridType,CellType>& other) const
      { return (curCell == other.curCell); }
      inline bool operator!=(const TriangleCellIteratorBase<GridType,CellType>& other) const 
      { return !(*this == other); }

      /**
       * Increment operators.
       */
      inline TriangleCellIteratorBase<GridType,CellType>& operator++(int) {
        TriangleCellIterator tmp(*this);
        ++(*this);
        return tmp;
      }
      inline TriangleCellIteratorBase<GridType,CellType>& operator++() {
        using namespace Arak::Geometry;
        assert(curCell != NULL);
        int i = curCell->row();
        int j = curCell->col();
        int k = i - imin;
        if ((i == imax) && (j == jmaxs[k])) {
          // We are at the last cell; the iterator is finished.
          curCell = NULL;
          return *this;
        } else if (j == jmaxs[k])
          // We are at the last column but not the last row; wrap
          // around.
          curCell = &(grid->getCell(i + 1, jmins[k + 1]));
        else 
          // We have not reached the end of the current row; move
          // right.
          curCell = curCell->right;
        return *this;
      }

      /**
       * Accessors.
       */
      inline CellType* operator->() const { return curCell; }
      inline CellType& operator*() const { return *curCell; }
      
    }; // end of class: Grid::TriangleCellIteratorBase

    /**
     * A mutable forwards iterator that ranges over the cells whose
     * closures intersect a given triangle.
     */
    typedef TriangleCellIteratorBase<Grid, Cell> 
    TriangleCellIterator;

    /**
     * A const forwards iterator that ranges over the cells whose
     * closures intersect a given triangle.
     */
    typedef TriangleCellIteratorBase<const Grid, const Cell> 
    ConstTriangleCellIterator;

    /**
     * A forwards iterator that ranges over the cells whose closures
     * intersect a given convex polygon.
     */
    template <class GridType, class CellType>
    class ConvexPolygonCellIteratorBase {

    protected:
      
      /**
       * The current cell of the iterator.
       */
      CellType* curCell;

      /**
       * The grid.
       */
      GridType* grid;

      /**
       * The minimum i coordinate of all cells intersecting the
       * polygon.
       */
      int imin;

      /**
       * The maximum i coordinate of all cells intersecting the
       * polygon.
       */
      int imax;

      /**
       * Stores for each column between #imin and #imax (inclusive)
       * the minimum j coordinate of all cells intersecting the
       * polygon.
       */
      std::vector<int> jmins;

      /**
       * Stores for each column between #imin and #imax (inclusive)
       * the maximum j coordinate of all cells intersecting the
       * polygon.
       */
      std::vector<int> jmaxs;

    public:

      /**
       * Constructor for an iterator over all cells whose closures
       * intersect a given convex polygon.
       *
       * @param g     a grid
       * @param p     a convex polygon
       */
      ConvexPolygonCellIteratorBase(GridType& g, const Geometry::Polygon& p)
        : curCell(NULL), grid(&g) { 
        // Compute the smallest rectangle that covers the intersection
        // of the triangle and the grid's closure.
        Geometry::Rectangle r(*(p.left_vertex()),
                              *(p.right_vertex()),
                              *(p.bottom_vertex()),
                              *(p.top_vertex()));
        if (!CGAL::assign(r, CGAL::intersection(g.boundary(), r))) {
          // The polygon does not intersect the grid's closure.
          curCell = NULL; 
          return;
        }
        // Compute the columns crossed by the triangle.
        int jmin, jmax;
        assert(g.getCellCoords(r.min(), imin, jmin));
        assert(g.getCellCoords(r.max(), imax, jmax));
        assert(imin <= imax);
        assert(jmin <= jmax);
        // Compute the values of jmins and jmaxs using line searches
        // along the boundary of the polygon.
        jmins.insert(jmins.end(), imax - imin + 1, 
                     std::numeric_limits<int>::max());
        jmaxs.insert(jmaxs.end(), imax - imin + 1, 
                     std::numeric_limits<int>::min());
        const int size = p.size();
        for (int v = 0; v < size; v++) {
          ConstLineCellIterator it(g, p[v], p[(v + 1) % size], 
                                   typename ConstLineCellIterator::SEGMENT()), end;
          while (it != end) {
            const Cell& cell = *it;
            int i = cell.row();
            int j = cell.col();
            int k = i - imin;
            jmins[k] = std::min<int>(jmins[k], j);
            jmaxs[k] = std::max<int>(jmaxs[k], j);
            ++it;
          }
        }
        // Replace any unset limits using the bounding rectangle.
        std::replace(jmins.begin(), jmins.end(),
                     std::numeric_limits<int>::max(), jmin);
        std::replace(jmaxs.begin(), jmaxs.end(),
                     std::numeric_limits<int>::min(), jmax);
        // Initialize the iterator at the first cell.
        curCell = &g.getCell(imin, jmins[0]);
      }

      /**
       * Constructor for an end iterator.
       */
      ConvexPolygonCellIteratorBase() : curCell(NULL), grid(NULL) { }

      /**
       * Comparison operators.
       */
      inline bool operator==(const ConvexPolygonCellIteratorBase<GridType,CellType>& other) const
      { return (curCell == other.curCell); }
      inline bool operator!=(const ConvexPolygonCellIteratorBase<GridType,CellType>& other) const 
      { return !(*this == other); }

      /**
       * Increment operators.
       */
      inline ConvexPolygonCellIteratorBase<GridType,CellType>& operator++(int) {
        ConvexPolygonCellIterator tmp(*this);
        ++(*this);
        return tmp;
      }
      inline ConvexPolygonCellIteratorBase<GridType,CellType>& operator++() {
        using namespace Arak::Geometry;
        assert(curCell != NULL);
        int i = curCell->row();
        int j = curCell->col();
        int k = i - imin;
        if ((i == imax) && (j == jmaxs[k])) {
          // We are at the last cell; the iterator is finished.
          curCell = NULL;
          return *this;
        } else if (j == jmaxs[k])
          // We are at the last column but not the last row; wrap
          // around.
          curCell = &(grid->getCell(i + 1, jmins[k + 1]));
        else 
          // We have not reached the end of the current row; move
          // right.
          curCell = curCell->right;
        return *this;
      }

      /**
       * Accessors.
       */
      inline CellType* operator->() const { return curCell; }
      inline CellType& operator*() const { return *curCell; }
      
    }; // end of class: Grid::ConvexPolygonCellIteratorBase

    /**
     * A mutable forwards iterator that ranges over the cells whose
     * closures intersect a given convex polygon.
     */
    typedef ConvexPolygonCellIteratorBase<Grid, Cell> 
    ConvexPolygonCellIterator;

    /**
     * A const forwards iterator that ranges over the cells whose
     * closures intersect a given convex polygon.
     */
    typedef ConvexPolygonCellIteratorBase<const Grid, const Cell> 
    ConstConvexPolygonCellIterator;

  protected:

    /**
     * The boundary of the grid.
     */
    const Geometry::Rectangle bd;

    /**
     * A two-dimensional vector of cells.
     */
    std::vector< std::vector<Cell*> > cells;

    /**
     * The number of rows in the grid.
     */
    const int rows;

    /**
     * The number of columns in the grid.
     */
    const int cols;

    /**
     * The (approximate) width of the grid cells.
     */
    Geometry::Kernel::FT cellWidth;
    
    /**
     * The (approximate) height of the grid cells.
     */
    Geometry::Kernel::FT cellHeight;

    /**
     * A vector containing the x values of the (#cols + 1) vertical
     * grid lines.  The first of these is the left boundary of the
     * grid, and the last is the right boundary of the grid.  Each
     * grid cell in column j spans the interval \f$ [
     * \texttt{xvals[j]}, \texttt{xvals[j + 1]} ) \f$ except the last
     * column, which is closed on both sides.
     */
    std::vector<Geometry::Kernel::FT> xvals;

    /**
     * A vector containing the y values of the (#rows + 1) horizontal
     * grid lines.  The first of these is the bottom boundary of the
     * grid, and the last is the top boundary of the grid.  Each grid
     * cell in row i spans the interval \f$ [ \texttt{yvals[i]},
     * \texttt{yvals[i + 1]} ) \f$ except the last row, which is
     * closed on both sides.
     */
    std::vector<Geometry::Kernel::FT> yvals;

  public:
    
    /**
     * Default constructor.
     *
     * @param boundary   an axis-aligned rectangle representing the
     *                   boundary of the grid.
     * @param rows       the number of rows in the cell grid
     * @param cols       the number of columns in the cell grid
     */
    Grid(const Geometry::Rectangle& boundary, int rows, int cols)
      : bd(boundary), rows(rows), cols(cols) {
      using namespace Arak::Geometry;
      // Compute the cell size.
      Kernel::FT xmin = bd.xmin();
      Kernel::FT xmax = bd.xmax();
      Kernel::FT ymin = bd.ymin();
      Kernel::FT ymax = bd.ymax();
      this->cellWidth = (xmax - xmin) / Kernel::FT(cols);
      this->cellHeight = (ymax - ymin) / Kernel::FT(rows);
      // Compute the grid lines.
      xvals.reserve(cols + 1);
      Kernel::FT x = xmin;
      for (int i = 0; i < cols; i++) {
        xvals.push_back(x);
        x += cellWidth;
      }
      xvals.push_back(xmax);
      yvals.reserve(rows + 1);
      Kernel::FT y = ymin;
      for (int i = 0; i < rows; i++) {
        yvals.push_back(y);
        y += cellHeight;
      }
      yvals.push_back(ymax);
      // Create the cells.
      cells.reserve(rows);
      for (int i = 0; i < rows; i++) {
        cells.push_back(std::vector<Cell*>());
        cells[i].reserve(cols);
        for (int j = 0; j < cols; j++) {
          Geometry::Rectangle r(xvals[j], yvals[i], 
                                xvals[j + 1], yvals[i + 1]);
          cells[i][j] = new Cell(r, i, j);
          if (i > 0) {
            cells[i][j]->down = cells[i - 1][j];
            cells[i - 1][j]->up = cells[i][j];
          }
          if (j > 0) {
            cells[i][j]->left = cells[i][j - 1];
            cells[i][j - 1]->right = cells[i][j];
          }
        }
      }
    }
  
    /**
     * Destructor.
     */
    ~Grid() {
      
    }

    /**
     * Returns the number of rows in the grid.
     *
     * @return the number of rows in the grid,
     */
    inline int numRows() const { return rows; }

    /**
     * Returns the number of columns in the grid.
     *
     * @return the number of columns in the grid,
     */
    inline int numCols() const { return cols; }

    /**
     * Returns a const reference to the boundary of the indexed
     * region.
     *
     * @return a const reference to the boundary of the indexed
     *         region
     */
    const Geometry::Rectangle& boundary() const { return bd; }

    /**
     * Updates the supplied parameters to the row and column index of
     * the cell that contains the supplied point.
     *
     * @param p the point
     * @param i updated to the row of the cell containing p
     * @param j updated to the column of the cell containing p
     * @return  true if p is in the window (and i and j are valid)
     */
    bool getCellCoords(const Geometry::Point& p, int& i, int& j) const {
      if (bd.has_on_unbounded_side(p)) return false;
      // First we use a simple computation to estimate the correct
      // cell.  This may be only approximate, though, due to floating
      // point error.  Then we shift the indexes as needed to compute
      // the exact cell.
      if (p.y() == bd.ymax()) 
        i = rows - 1;
      else {
        i = int(floor(CGAL::to_double((p.y() - bd.ymin()) / cellHeight)));
        while (p.y() < yvals[i]) i--;
        while (p.y() >= yvals[i + 1]) i++;
      }
      if (p.x() == bd.xmax()) 
        j = cols - 1;
      else {
        j = int(floor(CGAL::to_double((p.x() - bd.xmin()) / cellWidth)));
        while (p.x() < xvals[j]) j--;
        while (p.x() >= xvals[j + 1]) j++;
      }
      return true;
    }
    
    /**
     * Returns a const reference to the cell with the supplied row and
     * column index.
     *
     * @param i the row of the cell
     * @param j the column of the cell
     */
    inline const Cell& getCell(const int i, const int j) const {
      return *(cells[i][j]);
    }

    /**
     * Returns a mutable reference to the cell with the supplied row
     * and column index.
     *
     * @param i the row of the cell
     * @param j the column of the cell
     */
    inline Cell& getCell(const int i, const int j) {
      return *(cells[i][j]);
    }

    /**
     * Returns a const reference to the cell that contains the
     * supplied point.
     */
    const Cell& getCell(const Geometry::Point& p) const {
      int i = -1, j = -1;
      assert(getCellCoords(p, i, j));
      return getCell(i, j);
    }

    /**
     * Returns a reference to the cell that contains the
     * supplied point.
     */
    Cell& getCell(const Geometry::Point& p) {
      int i = -1, j = -1;
      assert(getCellCoords(p, i, j));
      return *(cells[i][j]);
    }

  };  // end of class: Grid

}

#endif
