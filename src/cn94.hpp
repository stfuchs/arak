// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _CN94_H
#define _CN94_H

#include <iostream>
#include <iomanip>
#include "properties.hpp"
#include "geometry.hpp"
#include "coloring.hpp"
#include "arak.hpp"
#include "coloring_mcmc.hpp"

namespace Arak {

  using namespace Geometry;

  /**
   * An implementation of the proposal distribution described in
   * Clifford & Nicholls (1994).
   */
  class CN94Proposal : public ColoringProposal {

  protected:

    /**
     * The type of move.
     */
    enum MoveType {
      INT_TRIANGLE_BIRTH = 0,     /*!< @see InteriorTriangleBirth  */
      INT_TRIANGLE_DEATH,         /*!< @see InteriorTriangleDeath  */
      RECOLOR,                    /*!< @see Recolor                */
      MOVE_INT_VERTEX,            /*!< @see MoveInteriorVertex     */
      MOVE_BD_VERTEX_ALONG_BD,    /*!< @see MoveVertexAlongBd      */
      MOVE_BD_VERTEX_PAST_CORNER, /*!< @see MoveBdVertexPastCorner */
      INT_VERTEX_BIRTH,           /*!< @see InteriorVertexBirth    */
      INT_VERTEX_DEATH,           /*!< @see InteriorVertexDeath    */
      BD_TRIANGLE_BIRTH,          /*!< @see BoundaryTriangleBirth  */
      BD_TRIANGLE_DEATH,          /*!< @see BoundaryTriangleDeath  */
      CORNER_CUT_BIRTH,           /*!< @see CornerCutBirth         */
      CORNER_CUT_DEATH,           /*!< @see CornerCutDeath         */
      INVALID_MOVE,               /*!< a move that is rejected     */
      UNSPECIFIED_MOVE            /*!< a marker used for debugging */
    };

    /**
     * The number of move types (excluding the unspecified marker).
     */
    static const int numMoveTypes = 13;

    /**
     * The names of the moves.
     */
    static const char* moveNames[numMoveTypes];
      
    /**
     * The number of times a move of each type has been proposed.
     */
    unsigned long int numProposed[numMoveTypes];

    /**
     * The number of times a move of each type has been accepted.
     */
    unsigned long int numAccepted[numMoveTypes];

    /**
     * Adds a triangle to the interior of the colored region; all points
     * inside the triangle are recolored.
     */
    class InteriorTriangleBirth : public ColoringMove {

      friend class CN94Proposal;
    
    protected:

      /**
       * The vertices of the triangle.
       */
      Point u, v, w;

      /**
       * A handle on a vertex of the triangle created by the move.
       * This is invalid unless the move has been executed; it is
       * invalidated if the move is undone.
       */
      Coloring::VertexHandle vh;

    public:

      InteriorTriangleBirth() {}

      /**
       * Resets this move with the new values.
       *
       * @param u the first vertex of the new triangle
       * @param v the second vertex of the new triangle
       * @param w the third vertex of the new triangle
       */
      void reset(const Point& u, const Point& v, const Point& w);

      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed interior triangle birth move.
     */
    InteriorTriangleBirth itb;

    /**
     * Removes a triangle from the interior of the colored region; all points
     * inside the triangle are recolored.
     */
    class InteriorTriangleDeath : public ColoringMove {

      friend class CN94Proposal;
    
    protected:

      /**
       * The proposal that this move is associated with.
       */
      const CN94Proposal& proposal;

      /**
       * A handle on a vertex of the triangle removed by the move.
       * This is invalid after the move has been executed, but is reset
       * to if the move is undone.
       */
      Coloring::VertexHandle vh;

      /**
       * The vertices of the triangle.  This information is used to
       * restore the triangle if the move is undone.
       */
      Point u, v, w;

      /**
       * This is true if the triangle to be removed is "short",
       * meaning that a casting box centered at each vertex contains
       * the other two vertices.
       */
      bool isShort;

    public:

      /**
       * Constructor.
       *
       * @param proposal the proposal this move is associated with
       */
      InteriorTriangleDeath(const CN94Proposal& proposal) 
	: proposal(proposal) {}

      /**
       * Resets this move.
       *
       * @param vertex  a vertex of the triangle to be deleted
       * @param isShort true if the vertex is part of a short triangle; 
       *                false if it is part of a long triangle
       */
      void reset(Coloring::VertexHandle vertex, bool isShort);

      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed interior triangle death move.
     */
    InteriorTriangleDeath itd;

    /**
     * Recolors a quadrilateral region of the coloring by replacing
     * two interior edges with two new interior edges that connect the
     * endpoints of the original edges.
     */
    class Recolor : public ColoringMove {

      friend class CN94Proposal;
      friend class ModifiedCN94Proposal;
    
    protected:

      /**
       * The first of the two edges.
       */
      Coloring::IntEdgeHandle edge1;

      /**
       * The second of the two edges.
       */
      Coloring::IntEdgeHandle edge2;

      /**
       * This flag is true if the source of #edge1 should be joined to
       * the source of #edge2 (and false if it should be joined to the
       * target of #edge2).
       */
      bool joinSourceToSource;

      /**
       * This flag is true if the recolored quadrilateral is convex.
       */
      bool convex;

    public:

      Recolor() {}

      /**
       * Resets this move.
       *
       * @param edge1              the first of the two edges
       * @param edge2              the second of the two edges
       * @param joinSourceToSource true if the source of edge1 
       *                           should be joined to the 
       *                           source of edge2 (and false if
       *                           it should be joined to the 
       *                           target of edge2)
       * @param convex             true if the recolored quadrilateral
       *                           is convex
       */
      void reset(Coloring::IntEdgeHandle edge1, 
		 Coloring::IntEdgeHandle edge2,
		 bool joinSourceToSource,
		 bool convex);

      /**
       * Determines if the recolored region is convex or not.
       *
       * @return true if the recolored region is convex
       */
      bool isConvex() { return convex; }

      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed recolor move.
     */
    Recolor rec;

    /**
     * Changes the location of an interior vertex.
     */
    class MoveInteriorVertex : public ColoringMove {

      friend class CN94Proposal;
    
    protected:

      /**
       * The vertex to be moved.
       */
      Coloring::VertexHandle vertex;

      /**
       * The current location of the vertex to be moved.
       */
      Point oldLoc;

      /**
       * The new location of the vertex to be moved.
       */
      Point newLoc;

    public:

      MoveInteriorVertex() {}

      /**
       * Resets this move.
       *
       * @param vertex the interior vertex to move
       * @param point  the new location
       */
      void reset(Coloring::VertexHandle vertex, const Point& point);

      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed "move interior vertex" move.
     */
    MoveInteriorVertex miv;

    /**
     * Moves a boundary vertex along its current boundary face.
     */
    class MoveVertexAlongBdEdge : public ColoringMove {
    
      friend class CN94Proposal;
    
    protected:
    
      /**
       * The boundary vertex to move.
       */
      Coloring::VertexHandle vertex;
    
      /**
       * The current location of the vertex to be moved.
       */
      Point oldLoc;

      /**
       * The new location of the vertex to be moved.
       */
      Point newLoc;

    public:
    
      MoveVertexAlongBdEdge() { }
      
      /**
       * Resets this move.
       *
       * @param vertex the boundary vertex to move
       * @param point  the new location
       */
      void reset(Coloring::VertexHandle vertex, const Point& point);

      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed "move boundary vertex along boundary
     * edge" move.
     */
    MoveVertexAlongBdEdge mbb;

    /**
     * Moves a boundary vertex past a corner to an adjacent face of
     * the boundary.
     */
    class MoveBdVertexPastCorner : public ColoringMove {

      friend class CN94Proposal;
    
    protected:

      /**
       * An edge that joins a corner vertex to a boundary vertex.
       */
      Coloring::BdEdgeHandle edge;

      /**
       * The old location of the boundary vertex incident to #edge.
       * This information is used to undo the move.
       */
      Point oldLoc;

      /**
       * The new location for the boundary vertex incident to #edge.
       * This point lies on the other boundary edge incident to the
       * corner.
       */
      Point newLoc;

    public:

      MoveBdVertexPastCorner() {}

      /**
       * Resets this move with the supplied parameters.
       *
       * @param edge  an edge that joins a corner vertex to a boundary
       *              vertex (or another corner vertex, in which case 
       *              the move is invalid)
       * @param point the new location for the boundary vertex incident
       *              to the edge.  This point lies on the other 
       *              boundary edge incident to the corner.
       */
      void reset(Coloring::BdEdgeHandle edge, const Point& point);

      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed "move boundary vertex past corner"
     * move.
     */
    MoveBdVertexPastCorner mbc;

    /**
     * Splits an interior edge into two by creating a new interior
     * vertex between its endpoints.
     */
    class InteriorVertexBirth : public ColoringMove {

      friend class CN94Proposal;
    
    protected:
    
      /**
       * The first vertex incident to the edge that is segmented in two.
       */
      Coloring::VertexHandle prevVertex;
    
      /**
       * The second vertex incident to the edge that is segmented in
       * two.
       */
      Coloring::VertexHandle nextVertex;

      /**
       * The location of a new vertex that segments the edge.
       */
      Point point;

    public:

      /**
       * Resets this move with the supplied parameters.
       *
       * @param edge  an interior edge that is segmented in two
       * @param point the location of a new vertex that segments the edge
       */
      void reset(Coloring::IntEdgeHandle edge, const Point& point);

      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed "interior vertex birth" move.
     */
    InteriorVertexBirth ivb;

    /**
     * Removes an interior vertex whose adjacent vertices are not
     * connected; the adjacent vertices are joined by a new edge.
     */
    class InteriorVertexDeath : public ColoringMove {

      friend class CN94Proposal;

    protected:

      /**
       * The proposal that this move is associated with.
       */
      const CN94Proposal& proposal;

      /**
       * The vertex preceeding the vertex to be removed.
       */
      Coloring::VertexHandle prevVertex;

      /**
       * The location of the removed vertex.  This information
       * is used to restore the vertex if the move is undone.
       */
      Point point;

      /**
       * The length of the edge that would be introduced if this
       * vertex death move were executed.
       */
      double newEdgeLength;

    public:

      /**
       * Constructor.
       *
       * @param proposal the proposal this move is associated with
       */
      InteriorVertexDeath(const CN94Proposal& proposal) 
	: proposal(proposal) {}

      /**
       * Resets this move.
       *
       * @param vertex the interior vertex to be removed
       */
      void reset(Coloring::VertexHandle vertex);

      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed interior vertex death move.
     */
    InteriorVertexDeath ivd;

    /**
     * Adds a triangle with one edge along the boundary of the colored
     * region; all points inside the triangle are recolored.
     */
    class BoundaryTriangleBirth : public ColoringMove {

      friend class CN94Proposal;
      friend class ModifiedCN94Proposal;
    
    protected:


      /**
       * The window edge along which the triangle is created.  This is
       * set to NULL when the move is executed, and it is restored if
       * the move is undone.
       */
      Coloring::BdEdgeHandle edge;

      /**
       * The boundary vertices of the boundary triangle; these vertices
       * lie along #edge.
       */
      Point u, v;

      /**
       * The interior vertex of the boundary triangle.
       */
      Point w;

      /**
       * A handle on the root vertex of the triangle created by the
       * move.  This is invalid unless the move has been executed; it
       * is invalidated if the move is undone.
       */
      Coloring::VertexHandle vh;


    public:

      BoundaryTriangleBirth() {}

      /**
       * Resets this move with the new values.
       *
       * @param e  the window edge
       * @param u  the first boundary vertex of the new triangle
       * @param v  the second boundary vertex of the new triangle
       * @param w  the interior vertex of the new triangle
       */
      void reset(Coloring::BdEdgeHandle e,
		 const Point& u, 
		 const Point& v, 
		 const Point& w);
 
      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed "boundary triangle birth" move.
     */
    BoundaryTriangleBirth btb;

    /**
     * Removes a triangle against the boundary of the colored region;
     * all points inside the triangle are recolored.
     */
    class BoundaryTriangleDeath : public ColoringMove {

      friend class CN94Proposal;
      friend class ModifiedCN94Proposal;
    
    protected:

      /**
       * A handle on the root vertex of the triangle removed by the
       * move.  This is invalidated after the move has been executed,
       * but is reset if the move is undone.
       */
      Coloring::VertexHandle vh;

      /**
       * The boundary vertices of the boundary triangle.  This
       * information is used to restore the triangle if the move is
       * undone.
       */
      Point up, vp;

      /**
       * The interior vertex of the boundary triangle.  This
       * information is used to restore the triangle if the move is
       * undone.
       */
      Point wp;

      /**
       * A handle on the boundary vertex that precedes the earlier
       * boundary vertex of the boundary triangle.  This is used to
       * restore the boundary triangle in case the move is undone.
       */
      Coloring::VertexHandle prevBoundaryVertex;

      /**
       * A handle on the boundary vertex that follows the later
       * boundary vertex of the boundary triangle.  This is used to
       * restore the boundary triangle in case the move is undone.
       */
      Coloring::VertexHandle nextBoundaryVertex;

    public:

      BoundaryTriangleDeath() {}

      /**
       * Resets this move.
       *
       * @param vertex the root vertex of the boundary triangle
       */
      void reset(Coloring::VertexHandle vertex);

      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed boundary triangle death move.
     */
    BoundaryTriangleDeath btd;

    /**
     * Recolors a triangle in a corner of the region.
     */
    class CornerCutBirth : public ColoringMove {

      friend class CN94Proposal;
    
    protected:

      /**
       * The corner that will form a vertex of the recolored triangle.
       */
      Coloring::VertexHandle corner;

      /**
       * The point on the previous boundary edge incident to #corner
       * that will be another vertex of the triangle.
       */
      Point u;

      /**
       * The point on the next boundary edge incident to #corner
       * that will be another vertex of the triangle.
       */
      Point v;

      /**
       * A handle to the root vertex of the triangle created by the
       * move.  This is invalid unless the move has been executed; it is
       * invalidated if the move is undone.
       */
      Coloring::VertexHandle vh;

    public:

      CornerCutBirth() {}

      /**
       * Resets this move with the new values.
       *
       * @param corner the corner that will become one vertex of the triangle
       * @param u      the point on the previous boundary edge incident to 
       *               the corner that will become another vertex of the 
       *               triangle
       * @param v      the point on the next boundary edge incident to 
       *               the corner that will become another vertex of the 
       *               triangle
       */
      void reset(Coloring::VertexHandle corner, 
		 const Point& u, const Point& v);
 
      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed "corner cut birth" move.
     */
    CornerCutBirth ccb;

    /**
     * Removes a triangle in a corner of the region; all points inside
     * the triangle are recolored.
     */
    class CornerCutDeath : public ColoringMove {

      friend class CN94Proposal;
    
    protected:

      /**
       * The corner that forms a vertex of the triangle to remove.
       */
      Coloring::VertexHandle corner;

      /**
       * The point on the previous boundary edge incident to #corner
       * that is another vertex of the triangle.  This information is
       * used to restore the triangle if the move is undone.
       */
      Point u;

      /**
       * The point on the next boundary edge incident to #corner that
       * will is another vertex of the triangle.  This information is
       * used to restore the triangle if the move is undone.
       */
      Point v;

      /**
       * A handle on the root vertex of the triangle removed by the
       * move.  This is invalid after the move has been executed.
       */
      Coloring::VertexHandle vh;

    public:

      CornerCutDeath() {}

      /**
       * Resets this move.
       *
       * @param vertex a corner vertex whose adjacent boundary vertices
       *               are connected by an internal edge
       */
      void reset(Coloring::VertexHandle vertex);

      virtual void execute(Coloring& c);

      virtual void undo(Coloring& c);

    };

    /**
     * The most recently proposed boundary triangle death move.
     */
    CornerCutDeath ccd;

    /**
     * Samples an interior triangle birth move and updates this proposal
     * object so the sampled move is the current proposal.  A point
     * \f$u\f$ is sampled uniformly from the colored region.  Two other
     * points \f$v\f$ and \f$w\f$ are sampled uniformly at a casting box
     * centered at \f$u\f$, and a move adding the triangle \f$uvw\f$ is
     * proposed (i.e., a recoloring of the triangle's interior is
     * proposed).
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleInteriorTriangleBirth(const Coloring& state, 
					     Arak::Util::Random& random);

    /**
     * Samples a move from the interior death group and updates this
     * proposal object so the sampled move is the current proposal.  The
     * interior death group consists of moves that remove one or more
     * vertices from the interior of the coloring.  A vertex in the
     * interior of the coloring is selected uniformly at random and then
     * one of the three move types is proposed, depending upon the
     * topology of the vertex's neighborhood:
     *
     *   - Interior triangle death: If the sampled vertex is part of an 
     *     interior triangle, then the triangle is removed from the 
     *     coloring.  (This is the reverse of a triangle birth move.)
     *
     *   - Interior vertex death: If the sampled vertex is part of an 
     *     interior polygon with more than three vertices, then the 
     *     vertex is removed and its neighbors are connected.  (This is 
     *     the reverse of an interior vertex birth move.)
     *
     *   - Boundary triangle death: If the sampled vertex is part of a 
     *     triangle where the other two vertices lie on the boundary, 
     *     then the triangle is removed.  (This is the reverse of a 
     *     boundary triangle birth move, and its result can also be 
     *     achieved by a boundary edge death move.)
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     * @param reversible a flag which indicates whether the proposal
     *                   must be capable of proposing the reverse of
     *                   sampled move.  This is set to false when the
     *                   proposal is used for stochastic optimization.
     *                   (Note that it must be possible to undo any move;
     *                   this flag allows the proposal to sample moves
     *                   whose reverses cannot be sampled.)
     */
    virtual void sampleInteriorDeath(const Coloring& state, 
				     Arak::Util::Random& random,
				     bool reversible = true);

    virtual void proposeBdTriangleDeath(const Coloring& state,
					Coloring::VertexHandle prevVertex,
					Coloring::VertexHandle vertex,
					Coloring::VertexHandle nextVertex,
					bool reversible = true);

    virtual void proposeIntVertexDeath(const Coloring& state,
				       Coloring::VertexHandle prevVertex,
				       Coloring::VertexHandle vertex,
				       Coloring::VertexHandle nextVertex,
				       bool reversible = true);

    virtual void proposeIntTriangleDeath(const Coloring& state,
					 Coloring::VertexHandle prevVertex,
					 Coloring::VertexHandle vertex,
					 Coloring::VertexHandle nextVertex,
					 bool reversible = true);

    /**
     * Samples a recolor move and updates this proposal object so the
     * sampled move is the current proposal.  Two distinct interior
     * edges \f$e_1\f$ and \f$e_2\f$ are sampled uniformly.  The
     * proposed move replaces these edges with two new edges that join
     * one vertex of each original edge.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleRecolor(const Coloring& state, 
			       Arak::Util::Random& random);

    /**
     * Samples a move that moves a vertex along the boundary past an
     * adjacent corner, and updates this proposal object so the sampled
     * move is the current proposal.  A corner vertex \f$u\f$is sampled
     * uniformly and one of its adjacent boundary edges \f$\{u, v\}\f$
     * is sampled uniformly.  A point \f$p\f$ is sampled uniformly on
     * its other incident boundary edge \f$\{u, w\}\f$. The proposed
     * move relocates \f$v\f$ to \f$p\f$.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleMoveBdVertexPastCorner(const Coloring& state, 
					      Arak::Util::Random& random);

    /**
     * Samples an interior vertex birth move and updates this proposal
     * object so the sampled move is the current proposal.  An interior
     * edge \f$e\f$ is sampled uniformly and a point \f$p\f$ is sampled
     * uniformly in a box of side length \f$|e|\f$ that is oriented so
     * its edges are parallel and orthogonal to \f$e\f$ and its center
     * is the center of \f$e\f$.  The proposed move splits \f$e\f$ at
     * \f$p\f$.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleInteriorVertexBirth(const Coloring& state, 
					   Arak::Util::Random& random);

    /**
     * Samples a move interior vertex move and updates this proposal
     * object so the sampled move is the current proposal.  An interior
     * vertex is sampled uniformly, a new location is sampled uniformly
     * in a casting box centered at the vertex, and a move that
     * relocates the vertex to the new location is proposed.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleMoveInteriorVertex(const Coloring& state, 
					  Arak::Util::Random& random);

    /**
     * Samples a move that slides a boundary vertex along its boundary
     * face and updates this proposal object so the sampled move is the
     * current proposal.  A boundary vertex is sampled uniformly, a new
     * location is sampled uniformly in the union of its incident
     * boundary edges.  A move that relocates the vertex to the new
     * location is proposed.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleMoveVertexAlongBd(const Coloring& state, 
					 Arak::Util::Random& random);

    /**
     * Samples a boundary triangle birth move and updates this proposal
     * object so the sampled move is the current proposal.  A window
     * edge \f$e\f$ is sampled uniformly, and two points \f$u\f$ and
     * \f$v\f$ are sampled uniformly along \f$e\f$.  A third point
     * \f$w\f$ is sampled in a square box of side length \f$|e|\f$ that
     * is inside the colored region with \f$e\f$ as one of its sides.  A
     * move adding the boundary triangle \f$uvw\f$ is proposed (i.e., a
     * recoloring of the triangle's interior is proposed).
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleBoundaryTriangleBirth(const Coloring& state, 
					     Arak::Util::Random& random);

    /**
     * Samples a boundary triangle death move and updates this proposal
     * object so the sampled move is the current proposal.  A boundary
     * edge is selected uniformly at random, and if it is part of a
     * boundary triangle (that could be created by a boundary triangle
     * birth), a move that removes the triangle is proposed.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     * @param reversible a flag which indicates whether the proposal
     *                   must be capable of proposing the reverse of
     *                   sampled move.  This is set to false when the
     *                   proposal is used for stochastic optimization.
     *                   (Note that it must be possible to undo any move;
     *                   this flag allows the proposal to sample moves
     *                   whose reverses cannot be sampled.)
     */
    virtual void sampleBoundaryTriangleDeath(const Coloring& state, 
					     Arak::Util::Random& random,
					     bool reversible = true);

    /**
     * Samples a corner cut birth move and updates this proposal object
     * so the sampled move is the current proposal.  A corner vertex
     * \f$c\f$ is sampled uniformly, and two points \f$u\f$ and \f$v\f$
     * are sampled uniformly along its adjacent boundary edges.  A move
     * adding the corner triangle \f$cuv\f$ is proposed (i.e., a
     * recoloring of the triangle's interior is proposed).
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleCornerCutBirth(const Coloring& state, 
				      Arak::Util::Random& random);

    /**
     * Samples a corner cut death move and updates this proposal object
     * so the sampled move is the current proposal.  A corner vertex is
     * selected uniformly at random, and if it is part of a corner
     * triangle (that could be created by a corner cut birth), a move
     * that removes the triangle is proposed.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleCornerCutDeath(const Coloring& state, 
				      Arak::Util::Random& random);

    /**
     * The probability that a move from the birth/death (BD) group is
     * proposed rather than a move from the move/recolor (MR) group.
     * This is referred to as \f$\zeta_1\f$ in (Clifford & Nicholls,
     * 1994).
     */
    double ZETA_BD_NOT_MR;

    /**
     * The probability that a move from the move group is proposed
     * rather than a recolor move.  This is referred to as \f$\zeta_2\f$
     * in (Clifford & Nicholls, 1994).
     */
    double ZETA_MOVE_NOT_RECOLOR;

    /**
     * The probability that a move from the birth group is proposed
     * rather than one from the death group.  This is referred to as
     * \f$\zeta_3\f$ in (Clifford & Nicholls, 1994).
     */
    double ZETA_BIRTH_NOT_DEATH;

    /**
     * The probability that a move is proposed to act in the interior
     * rather than on the boundary.  This is referred to as
     * \f$\zeta_4\f$ in (Clifford & Nicholls, 1994).
     */
    double ZETA_INT_NOT_BOUNDARY;

    /**
     * The probability that a move acting on the boundary is proposed to
     * act on an edge structure rather than a corner structure.  This is
     * referred to as \f$\zeta_5\f$ in (Clifford & Nicholls, 1994).
     */
    double ZETA_BOUND_NOT_CORNER;

    /**
     * The probability that an interior birth move is proposed to create
     * a new vertex rather than a new triangle.  This is referred to as
     * \f$\zeta_6\f$ in (Clifford & Nicholls, 1994).
     */
    double ZETA_IB_VERTEX_NOT_TRIANGLE;

    /**
     * The move type most recently proposed.
     */
    MoveType curMoveType;

    /**
     * Half of the side-length of the casting boxes used by this
     * algorithm.  A casting box is an axis-aligned square that is used
     * to enforce locality in many of the moves used by this proposal
     * distribution.
     */
    double castingBoxRadius;

    /**
     * Samples a point uniformly in a casting box centered at the
     * supplied point.
     *
     * @param p      the center of the casting box
     * @param random a source of pseudorandomness
     * @return       a point sampled uniformly in the casting box
     */
    Point sampleInCastingBox(const Point& p,
			     Arak::Util::Random& random) const;

    /**
     * Determines if q is in a casting box centered at p.
     *
     * @param p      the center of the casting box
     * @param q      another point
     * @return       true iff q is in the casting box centered at p
     */
    bool inCastingBox(const Point& p, const Point& q) const;

    /**
     * Returns the area of a casting box.
     *
     * @return the area of a casting box
     */
    double castingArea() const { 
      return 4.0 * castingBoxRadius * castingBoxRadius; 
    }

  public:
  
    /**
     * Constructor.
     */
    CN94Proposal(const Arak::Util::PropertyMap& props);

    /**
     * Destructor.
     */
    virtual ~CN94Proposal();

    /**
     * Resamples the proposal distribution to obtain a new move.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     * @param reversible a flag which indicates whether the proposal
     *                   must be capable of proposing the reverse of
     *                   sampled move.  This is set to false when the
     *                   proposal is used for stochastic optimization.
     *                   (Note that it must be possible to undo any move;
     *                   this flag allows the proposal to sample moves
     *                   whose reverses cannot be sampled.)
     */
    virtual void sample(const Coloring& state, 
			Arak::Util::Random& random,
			bool reversible = true);

    /**
     * Returns a reference to the move most recently sampled from this
     * proposal distribution.  This move is guaranteed to be stable only
     * until #resample(Arak::Util::Random&) is called again.
     *
     * @return the most recently sampled move
     */
    virtual ColoringMove& move();

    /**
     * Returns the log likelihood of proposing a move that would lead to
     * the same coloring as does the most recently sampled move.
     *
     * @return the log likelihood of the proposed state
     */
    virtual double ll(const Coloring& c);
  
    /**
     * Returns the log likelihood of proposing a return to the current
     * state from the most recently proposed state.
     *
     * @return the log likelihood of returning to the current state from
     *         the proposed state
     */
    virtual double rll(const Coloring& c);

    /**
     * This method is invoked by the Markov chain to inform the
     * proposal of the result of its most recently proposed move.
     */
    virtual void result(bool accepted) {
      if (accepted) numAccepted[int(curMoveType)]++;
    }

    /**
     * Writes statistics associated with the proposal to the supplied
     * stream.
     *
     * @param out the stream on which the statistics should be written
     */
    template<typename charT, typename traits>
    void writeStatistics(std::basic_ostream<charT,traits>& out) const {
      double numProposals = 0.0;
      for (int i = 0; i < numMoveTypes; i++)
	numProposals += double(numProposed[i]);
      out << "Clifford & Nicholls (1994) proposal statistics:" << std::endl;
      out << std::setw(28) << "move type" 
	  << std::setw(12) << "% proposed" 
	  << std::setw(12) << "% accepted" 
	  << std::endl;
      out << std::setw(28) << "----------------------------" 
	  << std::setw(12) << "-----------" 
	  << std::setw(12) << "-----------" 
	  << std::endl;
      out.precision(3);
      for (int i = 0; i < numMoveTypes; i++) {
	out << std::setw(28) << moveNames[i]
	    << std::setw(12) 
	    << 100.0 * (double(numProposed[i]) / numProposals) 
	    << std::setw(12) 
	    << 100.0 * (double(numAccepted[i]) / double(numProposed[i])) 
	    << std::endl;
      }
    }

  }; // End of class: Arak::CN94Proposal

  /**
   * This is a modified version of the Clifford & Nicholls (1994)
   * proposal distribution in which the recolor move samples edges
   * that are close together.
   */
  class ModifiedCN94Proposal : public CN94Proposal {

  protected:

    /**
     * The probability that a local recolor move is proposed rather
     * than a global recolor move.
     */
    double ZETA_LOCAL_NOT_GLOBAL_RECOLOR;

    /**
     * Samples a recolor move and updates this proposal object so the
     * sampled move is the current proposal.  With some probability,
     * the recolor move of the Clifford & Nicholls (1994) proposal
     * distribution is sampled; with the remaining probability a
     * recolor move is sampled as follows.  An interior vertex \f$u\f$
     * is sampled uniformly at random and another interior vertex
     * \f$v\f$ that is not adjacent to \f$u\f$ is sampled uniformly
     * from a casting box centered at \f$u\f$.  If there is no such
     * vertex an invalid move is proposed.  Otherwise, a neighbor
     * \f$x\f$ of \f$u\f$ and a neighbor \f$y\f$ of \f$v\f$ are
     * sampled and a recoloring move is proposed which joins \f$u\f$
     * to \f$v\f$ and \f$x\f$ to \f$y\f$.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleRecolor(const Coloring& state, 
			       Arak::Util::Random& random);

    /**
     * The probability that a local boundary triangle birth move is
     * proposed rather than a global boundary triangle birth move.
     */
    double ZETA_LOCAL_NOT_GLOBAL_BD_TRI_BIRTH;

    /**
     * Samples a boundary triangle birth move and updates this
     * proposal object so the sampled move is the current proposal.
     * With some probability, the recolor move of the Clifford &
     * Nicholls (1994) proposal distribution is sampled; with the
     * remaining probability a recolor move is sampled as follows.  A
     * window edge \f$e\f$ is sampled uniformly, and a point \f$u\f$
     * is sampled uniformly along \f$e\f$.  Let \f$c\f$ be the
     * axis-aligned casting box against \f$e\f$ with \f$u\f$ in the
     * center of one of its sides \f$s\f$.  A second point \f$v\f$ is
     * sampled uniformly from \f$s\f$.  (If \f$v\f$ is not on \f$e\f$,
     * the move is rejected.)  A third point \f$w\f$ is sampled from
     * the interior of \f$c\f$.  A move adding the boundary triangle
     * \f$uvw\f$ is proposed (i.e., a recoloring of the triangle's
     * interior is proposed).
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleBoundaryTriangleBirth(const Coloring& state, 
					     Arak::Util::Random& random);

    /**
     * Changes the location of an interior vertex by sliding it along
     * one of its incident edges.
     */
    class SlideInteriorVertex : public CN94Proposal::MoveInteriorVertex {

      friend class ModifiedCN94Proposal;
    
    protected:

      /**
       * This flag is true iff the vertex slide is occuring along the
       * vertex's next edge (rather than its previous edge).
       */
      bool next;

    public:

      SlideInteriorVertex() {}

      /**
       * Resets this move.
       *
       * @param vertex the interior vertex to move
       * @param next   a flag indicating whether the vertex slide is
       *               occuring along the vertex's next edge (rather 
       *               than its previous edge).
       * @param point  the new location
       */
      void reset(Coloring::VertexHandle vertex, bool next, 
		 const Point& point) {
	CN94Proposal::MoveInteriorVertex::reset(vertex, point);
	this->next = next;
      }

    };

    /**
     * The most recently proposed "slide interior vertex" move.
     */
    SlideInteriorVertex siv;

    /**
     * The probability that a slide interior vertex move is proposed
     * rather than a move interior vertex move.
     */
    double ZETA_SLIDE_NOT_MOVE_INT_VERTEX;

    /**
     * This flag indicates whether #miv represents a slide interior
     * vertex move rather than a move interior vertex move.  This is
     * used to correctly evaluate proposal probabilities. 
     *
     * \todo This is an ugly consequence of the design of the base
     * class--specifically, the fact that the MoveType enum is not
     * easily extensible.
     */
    bool slide_not_move;

    /**
     * Samples a slide interior vertex move and updates this proposal
     * object so the sampled move is the current proposal.  An
     * interior vertex \f$u\f$ is sampled uniformly, and one of its
     * incident edges \f$e\f$ is chosen with probability 1/2.  Let
     * \f$\vec{e}\f$ be the vector from \f$u\f$ to the other vertex
     * incident to \f$e\f$.  A new point \f$p\f$ is sampled uniformly
     * from the line segment between \f$u + \vec{e}\f$ and \f$u -
     * \vec{e} / 2\f$.  A move that relocates \f$u\f$ to \f$p\f$ is
     * proposed.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleSlideInteriorVertex(const Coloring& state, 
					   Arak::Util::Random& random);

    /**
     * Samples a move/slide interior vertex move and updates this
     * proposal object so the sampled move is the current proposal.
     * With some probability, the move interior vertex move of the
     * Clifford & Nicholls (1994) proposal distribution is sampled;
     * with the remaining probability a slide interior vertex move.
     *
     * @param state  the current state of the Markov chain
     * @param random a source of pseudorandomness
     */
    virtual void sampleMoveInteriorVertex(const Coloring& state, 
					  Arak::Util::Random& random);

  public:

    /**
     * Constructor.
     */
    ModifiedCN94Proposal(const Arak::Util::PropertyMap& props);

    /**
     * Destructor.
     */
    virtual ~ModifiedCN94Proposal() { }

    /**
     * Returns a reference to the move most recently sampled from this
     * proposal distribution.  This move is guaranteed to be stable only
     * until #resample(Arak::Util::Random&) is called again.
     *
     * @return the most recently sampled move
     */
    virtual ColoringMove& move() {
      if ((curMoveType == MOVE_INT_VERTEX) && slide_not_move)
	return siv;
      else return CN94Proposal::move();
    }

    /**
     * Returns the log likelihood of proposing a move that would lead to
     * the same coloring as does the most recently sampled move.
     *
     * @return the log likelihood of the proposed state
     */
    virtual double ll(const Coloring& c);
  
    /**
     * Returns the log likelihood of proposing a return to the current
     * state from the most recently proposed state.
     *
     * @return the log likelihood of returning to the current state from
     *         the proposed state
     */
    virtual double rll(const Coloring& c);
  };

} // End of namespace: Arak

#endif
