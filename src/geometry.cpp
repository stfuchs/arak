// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#include "geometry.hpp"

using namespace Arak;
using namespace Arak::Geometry;

void Arak::Geometry::n_gon(int n, double radius, Polygon& poly) {
  assert(n > 1);
  assert(radius > 0);
  // Clear the polygon.
  poly.erase(poly.vertices_begin(), poly.vertices_end());
  // Create a rotation tranformation.
  typedef CGAL::Aff_transformation_2<Geometry::Kernel> Transformation;
  double angle = (2.0 * M_PI) / double(n);
  Transformation rot(CGAL::ROTATION, sin(angle), cos(angle));
  Point p(radius, 0);
  for (int i = 0; i < n; i++) {
    poly.push_back(p);
    p = rot.transform(p);
  }
}
