// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _HANDLE_HPP
#define _HANDLE_HPP

template <typename T>
class PointerHandle {
private:
  T* ptr;
public:
  const T& operator*() const { return *ptr; }
  T& operator*() { return *ptr; }
  const T* operator->() const { return ptr; }
  T* operator->() { return ptr; }
  bool valid() const { return (ptr != NULL); }
  
  PointerHandle() { ptr = NULL; }
  PointerHandle(const PointerHandle<T> &vh) { this->ptr = vh.ptr; }
  PointerHandle(T* ptr) { this->ptr = ptr; }
  PointerHandle(T& item) { this->ptr = &item; }

  bool operator==(const PointerHandle<T>& other) const 
  { return (ptr == other.ptr); }
  bool operator!=(const PointerHandle<T>& other) const 
  { return (ptr != other.ptr); }
  bool operator<(const PointerHandle<T>& other) const 
  { return (ptr < other.ptr); }

  const PointerHandle<T>& operator=(const PointerHandle<T>& other) 
  { this->ptr = other.ptr; return *this; }
};

#endif
