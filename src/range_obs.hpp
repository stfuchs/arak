// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _RANGE_OBS_HPP
#define _RANGE_OBS_HPP

#include <iostream>
#include "properties.hpp"
#include "query_point.hpp"
#include "coloring.hpp"
#include "arak.hpp"
#include "grid.hpp"

namespace Arak {

  /** 
   * An posterior Arak process conditioned on localized range
   * measurements with Gaussian noise.
   *
   * This potential is additively decomposable so the posterior Arak
   * process has the spatial Markov property.
   */
  class ArakPosteriorRangeObs : public ArakProcess, 
				public Coloring::Listener {

  protected:

    class RangeObs;
    typedef PointerHandle<RangeObs> RangeObsHandle;

    /**
     * A grid where each cell stores handles to all observations
     * whose source-to-impact rays intersect the cell's boundary.
     */
    typedef Grid<RangeObsHandle> RangeObsIndex;

    /**
     * A range observation.
     *
     * \todo this should be a laser scan, with laser observations
     * inside.  right now we're doing 180 times the work to check that
     * the source of each ray is colored white.
     */
    class RangeObs : public QueryPointListener {

    public:
      
      /**
       * The location from which the range measurement was taken.
       */
      Geometry::Point source;

      /**
       * The point obtained by back-projecting the range measurement.
       */
      Geometry::Point target;

      /**
       * The point obtained by back-projecting the max range measurement.
       */
      Geometry::Point max;

      /**
       * The range measurement.
       */
      double range;

      /**
       * True if the sensor returned its max-range measurement.
       */
      bool isMaxRange;

      /**
       * The current color of the source.
       */
      Color sourceColor;

      /**
       * The contact point of this observation (or the max range
       * point) in the current coloring.
       */
      Geometry::Point curContact;

      /**
       * The distance from the source to the current contact point.
       */
      double curRange;

      /**
       * The current log probability of this observation.
       */
      double lp;

      /**
       * An entry of an observation in the index.
       */
      typedef RangeObsIndex::Cell::Entry CellEntry;

      /**
       * A list of cell entries.
       */
      typedef std::list<CellEntry> CellEntryList;

      /**
       * A list of cell entries in which this observation is stored.
       */
      CellEntryList entries;

      /**
       * A list of unused cell entries, used to minimize memory
       * allocation overhead.
       */
      CellEntryList unused;

      /**
       * The sequence number of the last update to this observation.
       */
      unsigned long int updateId;

      /**
       * Default constructor.
       */
      RangeObs(const Geometry::Point& source, 
	       const Geometry::Point& target, 
	       const double maxRange);

      /**
       * Destructor.
       */
      virtual ~RangeObs() {};

      /**
       * Reacts to information that the query point has been recolored.
       *
       * @param color the query point's new color
       */
      virtual void recolor(Color color) {
	sourceColor = color;
      }

    }; // End of class: ArakPosteriorRangeObs::RangeObservation

    /**
     * The prior process.
     */
    const ArakProcess& prior;

    /**
     * A vector of the observations.
     */
    std::vector<RangeObs*> obs;

    /**
     * An index of the observations.
     */
    RangeObsIndex* obsIndex;

    /**
     * The update sequence number.
     */
    unsigned long int updateId;

    /**
     * This is the range measurement obtained when the sensor does not
     * impact a surface.
     */
    double maxRange;

    /**
     * The variance of the range measurement, given the beam impacted
     * a surface.
     */
    double rangeVar;
  
    /**
     * The probability of obtaining an outlier range measurement.
     */
    double pOutlier;

    /**
     * The probability of obtaining a false-negative max-range
     * measurement.  This occurs when the correct observation is
     * #maxRange but a smaller range is observed.
     */
    double pMaxFalseNeg;

    /**
     * The probability of obtaining a false-positive max-range
     * measurement.  This occurs when the correct observation is
     * a range smaller than #maxRange but #maxRange is observed.
     */
    double pMaxFalsePos;

    /**
     * The current potential arising from observations.
     */
    double lp;
    
    /**
     * Retraces the supplied observation in the current coloring.
     * Returns true if the contact point of the observation changed.
     */
    bool retraceObs(RangeObs& o, bool init = false) const;

    /**
     * Updates the log probability of the supplied observation under
     * the current coloring and process parameters.  The change in its
     * log probability is returned.
     */
    double updateObsLP(RangeObs& o, bool init = false) const;

  public:

    /**
     * Stream constructor.  The observations are read from a file of
     * numbers with the following format:
     *
     * \verbatim
     * n
     * source-1-x source-1-y target-1-x target-1-y 
     * source-2-x source-2-y target-2-x target-2-y 
     * ...
     * source-n-x source-n-y target-n-x target-n-y 
     * \endverbatim
     *
     * @param c  The coloring whose probability is evaluated.  
     *           This is fixed for each process object so that 
     *           likelihood changes due to coloring updates can 
     *           be processed efficiently.
     * @param props the properties that define the posterior
     */
    ArakPosteriorRangeObs(const ArakProcess& prior,
			  const Arak::Util::PropertyMap& props);

    /**
     * Destructor.
     */
    virtual ~ArakPosteriorRangeObs();

    /**
     * Returns a const reference to a rectangle that encloses the
     * observed data.
     */
    const Geometry::Rectangle& boundary() const { 
      return obsIndex->boundary(); 
    }

    /**
     * Returns the scale of this Arak process.
     */
    virtual double scale() const { return prior.scale(); }

    /**
     * Computes the (log) measure associated with the Arak process for
     * the coloring.  (This is actually multiplied by an elementary
     * Poisson measure over the vertex locations to yield the coloring
     * measure.)
     *
     * @return  the measure of the coloring 
     */
    virtual double logMeasure() const;

    /**
     * Computes the potential associated with this posterior Arak
     * process.
     * 
     * @return  the current potential
     * @see ArakProcess
     */
    virtual double potential() const;

    /**
     * Renders a graphical representation of this Arak posterior using
     * the supplied widget.  The Gaussian observations are rendered as
     * disks of varying gray level.
     *
     * @param widget the widget on which to visualize the process
     */
    virtual void visualize(CGAL::Qt_widget& widget) const;

    /**
     * This method recalculates the likelihood of all observations
     * that are out of date and are sensitive to a given triangle.
     *
     * @param a the first vertex of the triangle
     * @param b the second vertex of the triangle
     * @param c the third vertex of the triangle
     */
    virtual void update(const Geometry::Point& a,
			const Geometry::Point& b,
			const Geometry::Point& c);
      
    /**
     * This method is invoked to inform this process that the
     * triangle with the supplied vertices has been recolored.
     *
     * @param a the first vertex of the triangle
     * @param b the second vertex of the triangle
     * @param c the third vertex of the triangle
     */
    virtual void recolored(const Geometry::Point& a,
			   const Geometry::Point& b,
			   const Geometry::Point& c);
      
    /**
     * This method is invoked to inform this process that the
     * quadrilateral with the supplied vertices has been recolored.
     * The vertices are supplied in either clockwise or
     * counter-clockwise order.  Note that this quadrilateral is
     * simple, but not necessarily convex.
     *
     * @param a the first vertex of the quadrilateral
     * @param b the second vertex of the quadrilateral
     * @param c the third vertex of the quadrilateral
     * @param d the fourth vertex of the quadrilateral
     */
    virtual void recolored(const Geometry::Point& a,
			   const Geometry::Point& b,
			   const Geometry::Point& c,
			   const Geometry::Point& d);
  }; // End of class: ArakPosteriorRangeObs

  /**
   * Renders a graphical representation of an Arak posterior using the
   * supplied widget.  The range measurments are rendered as line segments.
   *
   * @param widget the widget on which to visualize the process
   * @param p      the process to visualize
   * @return       the widget after the rendering is finished
   */
  CGAL::Qt_widget& operator<<(CGAL::Qt_widget& widget, 
			      ArakPosteriorRangeObs& p);

}

#endif
