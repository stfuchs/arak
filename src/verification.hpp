// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _VERIFICATION_HPP
#define _VERIFICATION_HPP

#include <iostream>
#include <assert.h>
#include "geometry.hpp"
#include "random.hpp"
#include "coloring.hpp"

namespace Arak {

  /**
   * An object which estimates several critical statistics whose true
   * values are known for the Arak process.  Comparing these estimates
   * to the true values gives a verification test for the correctness
   * of the implementation: if the estimates do not converge to their
   * true values the implementation is incorrect.  (Of course,
   * convergence to the correct values is necessary but not sufficient
   * for correctness.)
   *
   * The critical statistics that are estimated are:
   *
   * - the average number of vertices in the interior of the coloring
   * - the average number of (interior) edges in the coloring
   * - the average number of vertices on each face of the boundary
   * - the average number of times a test segment in the interior is 
   *   crossed by interior edges
   */
  class CriticalStatsEstimator {

  protected:

    /**
     * The number of samples used to update this estimator.
     */
    unsigned long int n;

    /**
     * The sum of the number of interior vertices of the samples.
     */
    unsigned long int sumIntVertices;

    /**
     * The sum of the number of interior edges of the samples.
     */
    unsigned long int sumIntEdges;

    /**
     * The sum of the number of boundary vertices on each face of the
     * boundary.  These estimates are coindexed with the preceeding
     * corner vertex index: sumBdVertices[i] corresponds to the
     * boundary face linking corners i and i + 1 (mod 4) of the
     * coloring samples.
     */
    unsigned long int sumBdVertices[4];

    /**
     * A set of test segments that lie in the interior of the
     * coloring.
     */
    std::vector<Geometry::Segment> testSegments;

    /**
     * The sum of the number of edge crossings for each test segment.
     * This vector is coindexed with #testSegments.
     */
    std::vector<unsigned long int> sumCrossings;

  public:

    /**
     * Default constructor.  No test segments are used.
     */
    CriticalStatsEstimator() 
      : n(0), sumIntVertices(0), sumIntEdges(0) {
      sumBdVertices[0] = 
	sumBdVertices[1] = 
	sumBdVertices[2] = 
	sumBdVertices[3] = 0;
    }

    /**
     * Constructor.
     *
     * @param k the number of random test segments
     * @param c the coloring for which the estimates are computed
     * @param random a source of pseudo-randomness
     */
    CriticalStatsEstimator(int k, const Coloring& c,
			   Arak::Util::Random& random = 
			   Arak::Util::default_random) 
      : n(0), sumIntVertices(0), sumIntEdges(0), sumCrossings(k, 0) {
      sumBdVertices[0] = 
	sumBdVertices[1] = 
	sumBdVertices[2] = 
	sumBdVertices[3] = 0;
      for (int i = 0; i < k; i++) {
	Geometry::Point p = c.randomPoint(random);
	Geometry::Point q = c.randomPoint(random);
	testSegments.push_back(Geometry::Segment(p, q));
      }
    }

    /**
     * Updates this estimator with a new sample.
     *
     * @param c the sample
     */
    void update(const Coloring& c);

    /**
     * Writes a report to the supplied output stream.
     *
     * @param boundary the boundary of the colored region
     * @param scale    the scale of the Arak process
     * @param out      the stream to which the report is written
     */
    template<typename charT, typename traits>
    void report(const Geometry::Rectangle& boundary,
		double scale,
		std::basic_ostream<charT,traits>& out) const {
      // TODO: I don't know the formula unless the boundary is square
      assert(boundary.xmax() - boundary.xmin() ==
	     boundary.ymax() - boundary.ymin());
      double side = CGAL::to_double(boundary.xmax() - boundary.xmin());
      double expNumIntVertices = 4.0 * M_PI * pow(side * scale, 2);
      double avgNumIntVertices = double(sumIntVertices) / double(n);
      double expNumIntEdges = 4.0 * side * scale + expNumIntVertices;
      double avgNumIntEdges = double(sumIntEdges) / double(n);
      out << std::setw(28) << "statistic" 
	  << std::setw(12) << "average" 
	  << std::setw(12) << "expectation" 
	  << std::endl;
      out << std::setw(28) << "----------------------------" 
	  << std::setw(12) << "-----------" 
	  << std::setw(12) << "-----------" 
	  << std::endl;
      out << std::setw(28) << "# interior vertices"
	  << std::setw(12) << avgNumIntVertices
	  << std::setw(12) << expNumIntVertices
	  << std::endl;
      out << std::setw(28) << "# interior edges"
	  << std::setw(12) << avgNumIntEdges
	  << std::setw(12) << expNumIntEdges
	  << std::endl;
      for (int i = 0; i < 4; i++) {
	double expNumBdVertices = 2.0 * scale * side;
	double avgNumBdVertices = double(sumBdVertices[i]) / double(n);
	out << std::setw(26) << "# boundary vertices (" 
	    << std::setw(0) << i << ")" 
	    << std::setw(12) << avgNumBdVertices
	    << std::setw(12) << expNumBdVertices
	    << std::endl;
      }
      for (unsigned int i = 0; i < testSegments.size(); i++) {
	const Geometry::Segment& s = testSegments[i];
	double length = sqrt(CGAL::to_double(s.squared_length()));
	double expNumCrossings = 2.0 * scale * length;
	double avgNumCrossings = double(sumCrossings[i]) / double(n);
	out << std::setw(26) << "# crossings of test edge (" 
	    << std::setw(0) << i << ")" 
	    << std::setw(12) << avgNumCrossings
	    << std::setw(12) << expNumCrossings
	    << std::endl;
      }
    }

  };

}

#endif
