// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _PLANAR_MAP_H
#define _PLANAR_MAP_H

#include <CGAL/basic.h>
#include <CGAL/Pm_default_dcel.h>
#include <CGAL/Pm_segment_traits_2.h>
#include <CGAL/Planar_map_2.h>
#include <CGAL/Topological_map.h>
#include "geometry.hpp"

namespace Arak {
  namespace Geometry {

    class VisitingFace : public CGAL::Pm_face_base {
      bool visited;
    public:
      VisitingFace() : CGAL::Pm_face_base(), visited(false) {}
      bool wasVisited() { return visited; }
      void visit() { visited = true; }
    };

    typedef CGAL::Pm_segment_traits_2<ExactPredKernel> Traits;
    typedef CGAL::Pm_dcel<CGAL::Pm_vertex_base<Traits::Point>,
			  CGAL::Pm_halfedge_base<Traits::X_curve>,
			  Arak::Geometry::VisitingFace > Dcel;  
    typedef Traits::X_monotone_curve_2           Curve;
    typedef CGAL::Planar_map_2<Dcel,Traits>      PlanarMap;
    typedef PlanarMap::Locate_type               LocateType;
    typedef PlanarMap::Vertex_handle             VertexHandle;
    typedef PlanarMap::Halfedge_handle           HalfedgeHandle;
    typedef PlanarMap::Face_handle               FaceHandle;
    typedef PlanarMap::Halfedge_iterator         HalfedgeIterator;
    typedef PlanarMap::Holes_iterator            HolesIterator;
    typedef PlanarMap::Ccb_halfedge_circulator   CCBHalfedgeCirculator;

    typedef ExactPredKernel::Point_2             ExactPredPoint;
    typedef ExactPredKernel::Segment_2           ExactPredSegment;

    inline ExactPredPoint toExactPredPoint(const Point& p) {
      return ExactPredPoint(p.x(), p.y());
    }

    inline ExactPredSegment toExactPredSegment(const Segment& s) {
      return ExactPredSegment(toExactPredPoint(s.source()),
			      toExactPredPoint(s.target()));
    }
  }
}

#endif
