// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _STORE_HPP
#define _STORE_HPP

#include <stack>

/**
 * A simple template data structure that encapsulates the process of
 * memory management so that deleted objects are returned to a "store"
 * for later "reallocation".  This avoids the overhead of memory
 * allocation for objects that are frequently created and destroyed.
 * The objects managed by this store must have default (zero
 * parameter) constructors.
 */
template <typename T>
class Store {

 protected:

  /**
   * A stack of the items in the store.
   */
  std::stack<T*> items;

 public:

  /**
   * Constructor.
   */
  Store() { }
  
  /**
   * Destructor.
   */
  ~Store();

  /**
   * Retrieves a fresh item from the store.
   */
  T* newItem();

  /**
   * Returns a used item to the store.
   */
  void deleteItem(T* item);

};

template <typename T>
Store<T>::~Store() {
  while (!items.empty()) {
    delete items.top();
    items.pop();
  }
}

template <typename T>
T* Store<T>::newItem() {
  if (items.empty())
    return new T();
  else {
    T* item = items.top();
    items.pop();
    return item;
  }
}

template <typename T>
void Store<T>::deleteItem(T* item) {
  items.push(item);
}

#endif
