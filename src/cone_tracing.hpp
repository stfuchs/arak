// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _CONE_TRACING_HPP
#define _CONE_TRACING_HPP

#include "handle.hpp"
#include "geometry.hpp"
#include "coloring.hpp"
#include "grid.hpp"

namespace Arak {

  /**
   * Provides a triangular outer approximation to this cone. 
   *
   * @return the smallest triangle that contains the cone
   */
  inline Geometry::Triangle outerApprox(const Geometry::Point& vertex,
					const Geometry::Direction& min,
					const Geometry::Direction& max,
					const Geometry::Kernel::FT& radius) {
    // Compute the other two vertices of the largest inner triangular
    // approximation to the cone.
    Geometry::Point p = vertex + min.vector();
    double length = sqrt(CGAL::to_double(CGAL::squared_distance(p, vertex)));
    p = vertex + (p - vertex) * (CGAL::to_double(radius) / length);
    Geometry::Point q = vertex + max.vector();
    length = sqrt(CGAL::to_double(CGAL::squared_distance(q, vertex)));
    q = vertex + (q - vertex) * (CGAL::to_double(radius) / length);
    // Squared hypotenuse length of inner triangle.
    const Geometry::Kernel::FT dsq = CGAL::squared_distance(vertex, p);
    // Squared base length of the inner triangle
    const Geometry::Kernel::FT bsq = CGAL::squared_distance(p, q);
    // Squared height of the inner triangle
    const double hsq = CGAL::to_double(dsq) - CGAL::to_double(bsq) / 4.0;
    // The inner and outer triangles are similar.  So the ratio
    // between d and h (the heights of the outer and inner
    // triangles) is the same as the ratio between the hypotenuse
    // length of the outer triangle and d, the hypotenuse length of
    // the inner triangle.
    double scale = sqrt(CGAL::to_double(dsq / hsq));
    return Geometry::Triangle(vertex, 
			      vertex + (p - vertex) * scale, 
			      vertex + (q - vertex) * scale);
  }

  /**
   * Computes the intersection of a ray and a segment that are known
   * to intersect.
   *
   * @param p the source of the ray
   * @param d the direction of the ray
   * @param s the segment
   * @return the point where the ray and segment intersect
   */
  inline Geometry::Point intersection(const Geometry::Point& p,
				      const Geometry::Direction& d,
				      const Geometry::Segment& s) {
    const Geometry::Line line1 = s.supporting_line();
    const Geometry::Line line2(p, d);
    Geometry::Point contact;
    assert(CGAL::assign(contact, CGAL::intersection(line1, line2)));
    return contact;
  }

  // Forward declaration.
  class ScanEdge;

  // A handle to a scan edge.
  typedef PointerHandle<ScanEdge> ScanEdgeHandle;

  /**
   * An endpoint of a scan edge.
   */
  struct ScanVertex {

    /**
     * A handle to the scan edge to which this vertex is incident.
     */
    ScanEdgeHandle edge;

    /**
     * True if this is the minimum vertex of the scan edge; false if
     * it is the maximum vertex.
     */
    bool isMin;

    /**
     * Constructor.
     */
    ScanVertex(ScanEdgeHandle edge, bool isMin) : edge(edge), isMin(isMin) { }

  };

  /**
   * A scan edge is an edge of the scan adorned with metadata.
   */
  struct ScanEdge {

    /**
     * The handle to the actual edge.
     */
    Coloring::IntEdgeHandle edge;

    /**
     * The direction to the minimum vertex from the scan origin.
     */
    Geometry::Direction dmin;

    /**
     * The location of the minimum vertex.
     */ 
    Geometry::Point pmin;

    /**
     * The direction to the maximum vertex from the scan origin.
     */
    Geometry::Direction dmax;

    /**
     * The location of the maximum vertex.
     */ 
    Geometry::Point pmax;

    /**
     * This flag is set to true if this edge has been pruned from the
     * scan list.
     */
    bool pruned;

    /**
     * If this edge is currently in the scan list, then this iterator
     * points to this edge's location in the scan list.
     */
    std::list<ScanEdgeHandle>::iterator it;

    /**
     * Constructor.
     *
     * @param vertex the vertex of the scan cone
     * @param min    the minimum direction of the scan cone
     * @param max    the maximum direction of the scan cone
     * @param e      a handle to the edge
     */
    void init(const Geometry::Point& vertex,
	      const Geometry::Direction& min,
	      const Geometry::Direction& max,
	      Coloring::IntEdgeHandle e) {
      this->pruned = true;
      this->edge = e;
      // Get the source, target, and midpoint of the edge.
      const Geometry::Point& source = e->source();
      const Geometry::Point& target = e->target();
      Geometry::Point midpoint = CGAL::midpoint(source, target);
      // Compute the directions to these points from the vertex.
      Geometry::Segment toSource(vertex, source);
      Geometry::Segment toTarget(vertex, target);
      Geometry::Segment toMidpoint(vertex, midpoint);
      Geometry::Direction ds = toSource.direction();
      Geometry::Direction dt = toTarget.direction();
      Geometry::Direction dm = toMidpoint.direction();
      // Determine the (counter-clockwise) order of these directions.
      bool sourceIsMin = dm.counterclockwise_in_between(ds, dt);
      if (sourceIsMin) {
	dmin = ds;
	dmax = dt;
      } else {
	dmin = dt;
	dmax = ds;
      }
      // Compute the minimum visible point on the segment.
      if (min.counterclockwise_in_between(dmin, dmax)) {
	dmin = min;
	pmin = intersection(vertex, min, e->segment());
      } else
	pmin = (sourceIsMin ? source : target);
      // Compute the maximum visible point on the segment.
      if (max.counterclockwise_in_between(dmin, dmax)) {
	dmax = max;
	pmax = intersection(vertex, max, e->segment());
      } else
	pmax = (sourceIsMin ? target : source);
    }

    /**
     * Returns the minimum vertex of this scan edge.
     */
    inline ScanVertex min() {
      return ScanVertex(ScanEdgeHandle(this), true);
    };

    /**
     * Returns the maximum vertex of this scan edge.
     */
    inline ScanVertex max() {
      return ScanVertex(ScanEdgeHandle(this), false);
    };
  };

  /**
   * Compares two directions to see which is closer to a third.
   *
   * @param a a direction
   * @param b a direction
   * @param min a direction
   * @return CGAL::EQUAL if a and b are the same direction, 
   *         CGAL::SMALLER if in a counterclockwise sweep from min, 
   *         a precedes b
   */
  inline CGAL::Comparison_result compare(const Geometry::Direction& a,
					 const Geometry::Direction& b,
					 const Geometry::Direction& min) {
    if (a == b) return CGAL::EQUAL;
    if (a == min) return CGAL::SMALLER;
    if (b == min) return CGAL::LARGER;
    if (a.counterclockwise_in_between(min, b)) return CGAL::SMALLER;
    return CGAL::LARGER;
  }

  /**
   * This object is used to place a total ordering on scan vertices.
   * Scan vertices are ordered by direction (angle).
   */
  class ScanVertexComparator {

  private:

    /**
     * A direction that serves as the least direction.
     */
    Geometry::Direction min;

  public:

    /**
     * Constructor.
     *
     * @param min a direction that serves as the least direction
     */
    ScanVertexComparator(const Geometry::Direction& min) : min(min) { }

    /**
     * The comparison operator.
     *
     * @param a a handle to a scan vertex
     * @param b a handle to a scan vertex
     * @return  true if the scan vertex indicated by a precedes the 
     *               scan vertex indicated by b
     */
    inline bool operator() (const ScanVertex& a,
			    const ScanVertex& b) const {
      const Geometry::Direction& adir = 
	(a.isMin ? a.edge->dmin : a.edge->dmax);
      const Geometry::Direction& bdir = 
	(b.isMin ? b.edge->dmin : b.edge->dmax);
      return (compare(adir, bdir, min) == CGAL::SMALLER);
    }
  };

  /**
   * Computes the intersection of a ray and a scan edge that are known
   * to intersect.
   *
   * @param p the source of the ray
   * @param d the direction of the ray
   * @param se a handle to the scan edge
   * @return the point where the ray and scan edge intersect
   */
  inline Geometry::Point intersection(const Geometry::Point& p,
				      const Geometry::Direction& d,
				      const ScanEdgeHandle se) {
    if (d == se->dmin) return se->pmin;
    if (d == se->dmax) return se->pmax;
    return intersection(p, d, se->edge->segment());
  }

  /**
   * Returns true iff two cones are disjoint.
   *
   * @param amin the minimum direction of the first cone
   * @param amax the maximum direction of the first cone
   * @param bmin the minimum direction of the second cone
   * @param bmax the maximum direction of the second cone
   * @return     true iff the first and second cones are disjoint
   */
  inline bool cones_intersect(const Geometry::Direction& amin,
			      const Geometry::Direction& amax,
			      const Geometry::Direction& bmin,
			      const Geometry::Direction& bmax) {
    return ((amin == bmin) ||
	    (amax == bmax) ||
	    bmin.counterclockwise_in_between(amin, amax) ||
	    bmax.counterclockwise_in_between(amin, amax) ||
	    amin.counterclockwise_in_between(bmin, bmax) ||
	    amax.counterclockwise_in_between(bmin, bmax));
  }

  /**
   * Returns true one cone is a subset of another.
   *
   * @param amin the minimum direction of the first cone
   * @param amax the maximum direction of the first cone
   * @param bmin the minimum direction of the second cone
   * @param bmax the maximum direction of the second cone
   * @return     true iff the first cone is a subset of the second
   */
  inline bool cone_subset(const Geometry::Direction& amin,
			  const Geometry::Direction& amax,
			  const Geometry::Direction& bmin,
			  const Geometry::Direction& bmax) {
    return (((amin == bmin) || 
	     amin.counterclockwise_in_between(bmin, bmax))
	    &&
	    ((amax == bmax) || 
	     amax.counterclockwise_in_between(bmin, bmax)));
  }

  /**
   * Returns true if two scan edges, known to overlap a given ray, are
   * ordered by depth.
   *
   * @param p   the source of the ray
   * @param dir the direction of the ray
   * @param a   a handle to the first scan edge 
   * @param b   a handle to the second scan edge 
   * @return    true if edge a is closer to p than b
   */
  inline bool closer(const Geometry::Point& p,
		     const Geometry::Direction& dir,
		     ScanEdgeHandle a,
		     ScanEdgeHandle b) {
    Geometry::Point ap = intersection(p, dir, a);
    Geometry::Point bp = intersection(p, dir, b);
    if (ap == bp) {
      bool a_at_min = (ap == a->pmin);
      bool b_at_min = (bp == b->pmin);
      if (a_at_min) {
	if (b_at_min) {
	  // a_at_min && b_at_min
	  return (CGAL::orientation(b->pmin, b->pmax, a->pmax) ==
		  CGAL::orientation(b->pmin, b->pmax, p));
	} else {
	  // a_at_min && !b_at_min
	  return false;
	}
      } else {
	if (b_at_min) {
	  // !a_at_min && b_at_min
	  return false;
	} else {
	  // !a_at_min && !b_at_min
	  return (CGAL::orientation(b->pmin, b->pmax, a->pmax) ==
		  CGAL::orientation(b->pmin, b->pmax, p));
	}
      }
    } else
      return (CGAL::compare_distance_to_point(p, ap, bp) == CGAL::SMALLER);
  }

  /**
   * A scan event represents a portion of an interior edge that is
   * visible.
   */
  struct ScanEvent {

    /**
     * A handle to the interior edge that is visible.
     */
    Coloring::IntEdgeHandle edge;

    /**
     * The minimum direction at which the edge is visible.  (We
     * compare the angles between the positive x-axis and the
     * directions in counterclockwise order.)
     */
    Geometry::Direction dmin;

    /**
     * The maximum direction at which the edge is visible.  (We
     * compare the angles between the positive x-axis and the
     * directions in counterclockwise order.)
     */
    Geometry::Direction dmax;

    /**
     * The point of contact corresponding to the minimum direction #dmin;
     */
    Geometry::Point pmin;

    /**
     * The point of contact corresponding to the maximum direction #dmax;
     */
    Geometry::Point pmax;
  };

  /**
   * Computes all edges of a coloring that are visible in a cone.
   *
   * @param coloring the coloring
   * @param vertex   the vertex of the cone, i.e., the point that is 
   *                 viewing the edges
   * @param min      the minimum direction that is visible 
   * @param max      the maximum direction that is visible 
   * @param radius   the maximum distance that is visible
   * @param filter   an outer triangular approximation to the cone;
   *                 this is used to probe the coloring's edge index
   * @param out      an output iterator to which ScanEvent objects
   *                 are assigned
   */
  template <class OutputIterator>
  void coneTrace(const Coloring& coloring,
		 const Geometry::Point& vertex,
		 const Geometry::Direction& min,
		 const Geometry::Direction& max,
		 const Geometry::Kernel::FT& radius,
		 const Geometry::Triangle& filter,
		 OutputIterator out) {
    typedef typename std::set<Coloring::IntEdgeHandle> EdgeHandleSet;
    static EdgeHandleSet edges; // Warning: this is not thread-safe
    edges.clear();
    const Coloring::InteriorEdgeIndex& index = 
      coloring.getInteriorEdgeIndex();
#ifdef RECTANGULAR_FILTER
    Geometry::Rectangle rFilter = boundingRectangle(filter[0],
						    filter[1],
						    filter[2]);
    Coloring::InteriorEdgeIndex::ConstRectangleCellIterator 
      it(index, rFilter), end;
#else
    Coloring::InteriorEdgeIndex::ConstTriangleCellIterator 
      it(index, filter), end;
#endif
    while (it != end) {
      const Coloring::InteriorEdgeIndex::Cell::ItemList& obsList = 
	it->getItemList();
      edges.insert(obsList.begin(), obsList.end());
      ++it;
    }
    // If there are no edges, we are done.
    if (edges.empty()) return;
    // Create scan edges and vertices.  The scan edge objects are
    // allocated statically for efficiency.
    const size_t MAX_NUM_SCAN_EDGES = 1024;
    static ScanEdge _se[MAX_NUM_SCAN_EDGES];
    assert(edges.size() <= MAX_NUM_SCAN_EDGES);
    // ScanEdge* _se = new ScanEdge[edges.size()];
    typedef typename std::vector<ScanVertex> ScanVertexSequence;
    static ScanVertexSequence scanVertices; // Warning: not thread-safe
    scanVertices.clear();
    int i = 0;
    for (EdgeHandleSet::iterator it = edges.begin(); it != edges.end(); it++) {
      _se[i].init(vertex, min, max, *it);
      ScanEdgeHandle seh(_se[i]);
      i++;
      // If the scan edge does not intersect the scan cone, ignore it.
      if (!cones_intersect(min, max, seh->dmin, seh->dmax)) continue;
      // Store the associated scan vertices.
      scanVertices.push_back(seh->max());
      scanVertices.push_back(seh->min());
    }
    // If there are no scan vertices, we are done.
    if (scanVertices.empty()) return;
    // Sort the scan vertices by direction.
    ScanVertexComparator comp(min);
    std::sort(scanVertices.begin(), scanVertices.end(), comp);
    /* Scan through the sorted vertices.  When a minimum vertex is
     * encountered, insert its edge into the list in order of its
     * depth along the current angle.  When a maximum vertex is
     * encountered, remove its edge from the list.  Whenever a change
     * occurs, append an event to the list of events.
     */
    // The list of scan edges, sorted by depth.
    typedef std::list<ScanEdgeHandle> ScanEdgeHandleList;
    ScanEdgeHandleList front_to_back;
    // Insert a sentinel in the list which means 'no current edge'.
    ScanEdgeHandle front;
    front_to_back.push_back(front);
    // The current scan event.
    ScanEvent curEvent;
    curEvent.edge = Coloring::IntEdgeHandle();
    // Scan through the vertices.
    for (ScanVertexSequence::iterator svi = scanVertices.begin(); 
	 svi != scanVertices.end(); svi++) {
      // Get a handle to the edge that is currently in front.
      front = front_to_back.front();
      // Get the direction to the current vertex.
      const Geometry::Direction& dir = 
	(svi->isMin ? svi->edge->dmin : svi->edge->dmax);
      // Check to see if the current scan vertex is a minimum vertex
      // or a maximum vertex.
      if (svi->isMin) {
	// The current vertex is a minimum vertex.  Insert its scan
	// edge into sorted position in the list.
	for (ScanEdgeHandleList::iterator sehi = front_to_back.begin();
	     sehi != front_to_back.end(); ++sehi) {
	  ScanEdgeHandle seh = *sehi;
	  if (!seh.valid() || closer(vertex, dir, svi->edge, seh)) {
	    svi->edge->it = front_to_back.insert(sehi, svi->edge);
	    svi->edge->pruned = false;
	    // Prune any deeper edges that are dominated by this one.
	    if (seh.valid()) {
	      ++sehi;
	      while ((seh = *sehi).valid()) {
		if (cone_subset(seh->dmin, seh->dmax,
				svi->edge->dmin, svi->edge->dmax)) {
		  seh->pruned = true;
		  sehi = front_to_back.erase(sehi);
		} else
		  ++sehi;
	      }
	    }
	    break;
	  } else if (cone_subset(svi->edge->dmin, svi->edge->dmax, 
				 seh->dmin, seh->dmax)) {
	    // This edge is dominated by the previous edge.  Do not
	    // insert it in the list.
	    svi->edge->pruned = true;
	    break;
	  }
	}
      } else {
	// The current vertex is a maximum vertex.  Remove its scan
	// edge from the list.
	if (!(svi->edge->pruned))
	  front_to_back.erase(svi->edge->it);
      }
      // If the edge in front has changed, update the current event.
      if (front != front_to_back.front()) {
	// Check to see if there is an event in progress that must be
	// reported.
	if (curEvent.edge.valid() && (curEvent.dmin != dir)) {
	  // Report the current event.
	  curEvent.dmax = dir;
	  curEvent.pmax = intersection(vertex, dir, front);
	  out = curEvent;
	  ++out;
	}
	// Update the current event.
	if (front_to_back.front().valid()) {
	  curEvent.edge = front_to_back.front()->edge;
	  curEvent.dmin = dir;
	  curEvent.pmin = intersection(vertex, dir, front_to_back.front());
	} else
	  curEvent.edge = Coloring::IntEdgeHandle();
      }
    }
  }

  /**
   * Computes all edges of a coloring that are visible in a cone.
   *
   * @param coloring the coloring
   * @param vertex   the vertex of the cone, i.e., the point that is 
   *                 viewing the edges
   * @param min      the minimum direction that is visible 
   * @param max      the maximum direction that is visible 
   * @param radius   the maximum distance that is visible
   * @param out      an output iterator to which ScanEvent objects
   *                 are assigned
   */
  template <class OutputIterator>
  void coneTrace(const Coloring& coloring,
		 const Geometry::Point& vertex,
		 const Geometry::Direction& min,
		 const Geometry::Direction& max,
		 const Geometry::Kernel::FT& radius,
		 OutputIterator out) {
    Geometry::Triangle outer = outerApprox(vertex, min, max, radius);
    coneTrace(coloring, vertex, min, max, radius, outer, out);
  }

} // End of namespace: Arak

#endif
