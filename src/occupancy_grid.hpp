// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#ifndef _OCCUPANCY_GRID_HPP
#define _OCCUPANCY_GRID_HPP

#include <iostream>
#include <iomanip>
#include "properties.hpp"
#include "global.hpp"
#include "grid.hpp"

namespace Arak {

  /**
   * This is a simple class that implements "evidence grid" maps for
   * laser and sonar updates.
   */
  class OccupancyGrid {

  protected:

    // Forward declaration
    class OccupancyEstimator;
    class StateComparator;

    /**
     *
     */
    typedef Grid<OccupancyEstimator*> EstimatorGrid;

    /**
     * Estimates the probability a cell is occupied.  The estimate and
     * updates are performed in log-odds space using inverse
     * observation models.
     */
    class OccupancyEstimator {

      friend class OccupancyGrid;
      friend class OccupancyGrid::StateComparator;

    protected:

      /**
       * This is the prior probability the cell is occupied.
       */
      double prior_log_odds;

      /**
       * This is the log-odds ratio of the probability the cell is
       * occupied.
       */
      double log_odds;

    public:
      
      /**
       * Constructor.
       */
      OccupancyEstimator(double prior = 0.5) {
	this->log_odds = this->prior_log_odds = log(prior / (1 - prior));
      }

      /**
       * Conditions this estimator on new evidence.
       *
       * @param posterior the probability the cell is occupied given
       *                  the evidence
       */
      void condition(double posterior) {
	double obs_log_odds = log(posterior / (1.0 - posterior));
	log_odds += obs_log_odds - prior_log_odds;
      }

      /**
       * Returns an estimate of the probability this cell is occupied.
       */
      double estimate() const {
	return 1.0 - (1.0 / (1.0 + exp(log_odds)));
      }

    }; // End of class: OccupancyEstimator

    /**
     * An index of the grid cells whose occupancies are estimated.
     */
    EstimatorGrid* grid;

  public:

    /**
     * Constructor.  
     *
     * @param props a property map
     */
    OccupancyGrid(const Util::PropertyMap& props);

    /**
     * Destructor.
     */
    ~OccupancyGrid();

    /**
     * Conditions this occupancy grid on a range observation.
     *
     * @param source        the point from which the observation 
     *                      was made
     * @param impact        the measurement, backprojected into the global
     *                      coordinate frame
     * @param maxRange      the maximum valid range measurement
     * @param obstacleWidth the depth of an obstacle
     * @param p_given_obs   the posterior probability a cell is 
     *                      occupied given an obstacle is observed
     *                      crossing the cell
     * @param p_given_free  the posterior probability a cell is 
     *                      free given no obstacle is observed
     *                      crossing the cell
     */
    void laser(const Geometry::Point& source,
	       const Geometry::Point& impact,
	       double maxRange,
	       double obstacleWidth,
	       double p_given_obs,
	       double p_given_free);

    /**
     * Conditions this occupancy grid on a range observation.
     *
     * @param source        the point from which the observation 
     *                      was made
     * @param impacts       a vector of the scan measurements, 
     *                      backprojected into the global
     *                      coordinate frame
     * @param maxRange      the maximum valid range measurement
     * @param obstacleWidth the depth of an obstacle
     * @param p_given_obs   the posterior probability a cell is 
     *                      occupied given an obstacle is observed
     *                      crossing the cell
     * @param p_given_free  the posterior probability a cell is 
     *                      free given no obstacle is observed
     *                      crossing the cell
     */
    void laser_scan(const Geometry::Point& source,
		    const std::vector<Geometry::Point>& impacts,
		    double maxRange,
		    double obstacleWidth,
		    double p_given_obs,
		    double p_given_free);
    /**
     * Conditions this occupancy grid on a sonar observation.
     *
     * @param source        the location of the sensor (meters)
     * @param angle         the orientation of the sensor (radians)
     * @param range         the observed range measurement (meters)
     * @param maxRange      the maximum range measurement
     * @param obstacleWidth the depth of an obstacle
     * @param width         the width of the sonar cone (radians)
     * @param p_given_obs   the posterior probability a cell is 
     *                      occupied given an obstacle is observed
     *                      crossing the cell
     * @param p_given_free  the posterior probability a cell is 
     *                      free given no obstacle is observed
     *                      crossing the cell
     */
    void sonar(const Geometry::Point& source,
	       double angle,
	       double range,
	       double maxRange,
	       double obstacleWidth,
	       double width,
	       double p_given_obs,
	       double p_given_free);

    /**
     * Writes an ASCII representation of the current estimates to the
     * supplied stream.  The first line gives the number of rows and
     * columns of the grid; the third line is the boundary of the
     * grid.  Then the estimates are written out as a matrix (one line
     * per row) in reverse-row order (so the first element corresponds
     * to the upper left corner of the grid).  Each estimate is the
     * probability the cell is occupied.
     *
     * @param out    the stream on which to write the estimates
     */
    template<typename charT, typename traits>
    void write(std::basic_ostream<charT,traits>& out) const {
      // Write out an arbitrary number (so the form is the same as
      // Arak grid estimates).
      out << 0 << std::endl;
      // Write out the number of rows and columns.
      out << grid->numRows() << " " << grid->numCols() << std::endl;
      // Write out the boundary of the grid.
      out << grid->boundary() << std::endl;
      // Now write out each point color estimate.
      out << std::scientific << std::setw(16) << std::setprecision(12);
      for (int i = grid->numRows() - 1; i >= 0; i--) {
	for (int j = 0; j < grid->numCols(); j++) {
	  const EstimatorGrid::Cell& cell = grid->getCell(i, j);
	  const OccupancyEstimator* est = *(cell.getItemList().begin());
	  out << est->estimate() << " ";
	}
	out << std::endl;
      }
    }

    /**
     * Stream constructor.  
     *
     * @param in an input stream from which the occupancy grid is read.
     */
    template<typename charT, typename traits>
    OccupancyGrid(std::basic_istream<charT,traits>& in) {
      in >> skipcomments;
      int unused, num_rows, num_cols;
      in >> unused >> num_rows >> num_cols;
      double xmin, ymin, xmax, ymax, prob;
      in >> xmin >> ymin >> xmax >> ymax;
      Geometry::Rectangle r(xmin, ymin, xmax, ymax);
      grid = new EstimatorGrid(r, num_rows, num_cols);
      for (int i = num_rows - 1; i >= 0; i--)
	for (int j = 0; j < num_cols; j++) {
	  EstimatorGrid::Cell& cell = grid->getCell(i, j);
	  OccupancyEstimator* est = new OccupancyEstimator();
	  in >> prob;
	  est->prior_log_odds = 0.0; // TODO: parameter
	  est->log_odds = log(prob / (1.0 - prob));
	  cell.add(est);
	}
      assert(in.good());
    }

  };

}

#endif
