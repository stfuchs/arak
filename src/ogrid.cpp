// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "parsecl.hpp"
#include "properties.hpp"
#include "occupancy_grid.hpp"

using namespace Arak;

/**
 * This is a wrapper that parses the command line and invokes the
 * program.
 */
int main(int argc, char** argv) {
  using namespace Arak::Util;
  // Parse the command line.
  CommandLine cl;
  CommandLine::MultiParameter<std::string>
    propFiles("-p", "--prop-file", "specifies a property file");
  cl.add(propFiles);
  CommandLine::MultiParameter<std::string>
    propDefs("-D", "--define-prop", "defines a property value");
  cl.add(propDefs);

  if (!cl.parse(argc, argv, std::cerr)) {
    cl.printUsage(std::cerr);
    exit(1);
  }

  // Parse the properties files.
  Arak::Util::PropertyMap properties;
  const std::vector<std::string>& propFilePaths = propFiles.values();
  for (std::vector<std::string>::const_iterator it = propFilePaths.begin();
       it != propFilePaths.end(); it++) {
    std::string path = *it;
    std::ifstream in(path.data());
    in >> properties;
  }
  // Parse the overriding property definitions.
  const std::vector<std::string>& propDefinitions = propDefs.values();
  for (std::vector<std::string>::const_iterator it = propDefinitions.begin();
       it != propDefinitions.end(); it++) {
    std::string def = *it;
    std::istringstream in(def);
    in >> properties;
  }

  std::cerr << "Properties:" << std::endl << properties << std::endl;

  // Build the occupancy grid.
  OccupancyGrid grid(properties);

  // Write the result to standard out.
  grid.write(std::cout);
  
}
