// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#include "geometry.hpp"
#include "arak.hpp"

using namespace Arak;
using namespace Arak::Geometry;

const double NEGATIVE_INFINITY = -1e10;

void ArakPrior::initialize() {
  c.addListener(*this);
  sumLengths = 0.0;
  sumLogLengths = 0.0;
  sumLogSines = 0.0;
  for (int i = 0; i < c.numVertices(Coloring::Vertex::INTERIOR); i++)
    vertexHasBeenAdded(c.getVertex(Coloring::Vertex::INTERIOR, i));
  for (int i = 0; i < c.numVertices(Coloring::Vertex::BOUNDARY); i++)
    vertexHasBeenAdded(c.getVertex(Coloring::Vertex::BOUNDARY, i));
  for (int i = 0; i < c.numInteriorEdges(); i++)
    edgeHasBeenAdded(c.getInteriorEdge(i));
}

void ArakPrior::vertexHasBeenAdded(Coloring::VertexHandle vh) {
  sumLogSines += std::max(NEGATIVE_INFINITY, vh->logSine());
}

void ArakPrior::vertexWillBeRemoved(Coloring::VertexHandle vh) {
  sumLogSines -= std::max(NEGATIVE_INFINITY, vh->logSine());
}

void ArakPrior::edgeHasBeenAdded(Coloring::IntEdgeHandle eh) {
  // TODO: deal with numerical instability
  double length = eh->length();
  sumLengths += length;
  sumLogLengths += std::max(NEGATIVE_INFINITY, ln(length));
}
    
void ArakPrior::edgeWillBeRemoved(Coloring::IntEdgeHandle eh) {
  // std::cerr << sumLogSines << " " << sumLogLengths << std::endl;
  // TODO: deal with numerical instability
  double length = eh->length();
  sumLengths -= length;
  sumLogLengths -= std::max(NEGATIVE_INFINITY, ln(length));
}

double ArakPrior::logMeasure() const {
  return (c.numInteriorEdges() * ln(p) 
	  - sumLogLengths 
	  + sumLogSines);
}




