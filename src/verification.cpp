// Software for Inference and Robotic Mapping with the Arak Process
// Copyright (C) 2005 Mark A. Paskin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact information:
//   Mark A. Paskin
//   http://paskin.org
//   mark@paskin.org

#include <set>
#include "verification.hpp"
#include "coloring.hpp"
#include "grid.hpp"

using namespace Arak;

void CriticalStatsEstimator::update(const Coloring& c) {
  n++;
  sumIntVertices += c.numVertices(Coloring::Vertex::INTERIOR);
  sumIntEdges += c.numInteriorEdges();
  for (int i = 0; i < 4; i++) {
    Coloring::VertexHandle vh = c.getVertex(Coloring::Vertex::CORNER, i);
    vh = vh->getNextBdEdge()->getNextVertex();
    while (vh->type() != Coloring::Vertex::CORNER) {
      sumBdVertices[i]++;
      vh = vh->getNextBdEdge()->getNextVertex();
    }
  }
  typedef const Coloring::InteriorEdgeIndex Index;
  typedef Index::ConstLineCellIterator Iterator; 
  typedef const Index::Cell Cell;
  typedef Cell::ItemList::const_iterator EdgeIterator;
  Index& index = c.getInteriorEdgeIndex();
  std::set<Coloring::IntEdgeHandle> counted;
  for (unsigned int i = 0; i < testSegments.size(); i++) {
    const Geometry::Segment& s = testSegments[i];
    counted.clear();
    Iterator it(index, s.source(), s.target(), Iterator::SEGMENT()), end;
    while (it != end) {
      Cell& cell = *it;
      const Cell::ItemList& edges = cell.getItemList();
      for (EdgeIterator jt = edges.begin(); jt != edges.end(); jt++) {
	const Coloring::IntEdgeHandle e = *jt;
	if ((counted.find(e) == counted.end()) && 
	    e->crosses(s.source(), s.target()))
	  counted.insert(e);
      }
      ++it;
    }
    sumCrossings[i] += counted.size();
  }
}


