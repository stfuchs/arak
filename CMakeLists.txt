# Created by the script cgal_create_cmake_script
# This is the CMake script for compiling a CGAL application.


project( arak )

cmake_minimum_required(VERSION 2.6.2)
if("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}" VERSION_GREATER 2.6)
  if("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}.${CMAKE_PATCH_VERSION}" VERSION_GREATER 2.8.3)
    cmake_policy(VERSION 2.8.4)
  else()
    cmake_policy(VERSION 2.6)
  endif()
endif()

set(CMAKE_BUILD_TYPE Debug)
set(EXECUTABLE_OUTPUT_PATH "${CMAKE_SOURCE_DIR}/bin" CACHE PATH "Build directory" FORCE)
set(LIBRARY_OUTPUT_PATH "${CMAKE_SOURCE_DIR}/lib" CACHE PATH "Build directory" FORCE)


find_package(CGAL REQUIRED COMPONENTS Core Qt3)
include( ${CGAL_USE_FILE} )

#find_package(Qt4 REQUIRED)
#include( ${QT_USE_FILE} )
find_package(Qt3 REQUIRED)
add_definitions(${QT_DEFINITIONS})

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake_modules/")
find_package(GSL REQUIRED)

include_directories(
  ${QT_INCLUDE_DIR}
  "aux/gzstream/")

add_library(arak
  "src/arak.cpp"
  "src/cn94.cpp"
  "src/coloring.cpp"
  "src/coloring_mcmc.cpp"
  "src/estimation.cpp"
  "src/geometry.cpp"
  "src/jet.cpp"
  "src/point_obs.cpp"
  "src/query_point.cpp"
  "src/random.cpp"
  "src/range_obs.cpp"
  "src/sonar_obs.cpp"
  "src/verification.cpp")
#target_link_libraries(arak ${GSL_LIBRARIRES} )

add_library(gzstream "aux/gzstream/gzstream.C")

qt_wrap_cpp(moc1 gui_moc "src/gui.hpp")
add_library(moc1 ${gui_moc})

#qt_wrap_cpp(moc2 sonar_moc "src/sonar_viz.cpp")
#add_library(moc2 ${sonar_moc})

add_executable(sampler "src/sampler.cpp" "src/gui.cpp")
target_link_libraries(sampler arak gzstream ${QT_LIBRARIES} ${GSL_LIBRARIES})

add_executable(ogrid "src/ogrid.cpp" "src/occupancy_grid.cpp")
add_executable(surftopgm "src/surftopgm.cpp")
add_executable(gridtopgm "src/gridtopgm.cpp")
target_link_libraries(gridtopgm arak)
# not working yet
#add_executable(sonar_viz "src/sonar_viz.cpp")
#target_link_libraries(sonar_viz arak)
add_executable(coloringtofig "src/coloringtofig.cpp")
target_link_libraries(coloringtofig arak ${GSL_LIBRARIES})
add_executable(samplestofigs "src/samplestofigs.cpp")
target_link_libraries(samplestofigs arak ${GSL_LIBRARIES})
